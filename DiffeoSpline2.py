#!/usr/bin/python2

# from vectormomenta config module. must be made general package by itself 
from Configs import Config, Compute

# pyca modules
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
import Libraries.CAvmCommon as cavmcommon

# diffeo_splines modules
import Core2 as Core
import Optim2 as Optim
import SplinesConfig
# others
import numpy as np
import matplotlib
#matplotlib.use('GTKAgg')
import matplotlib.pyplot as plt
import os
import errno

import logging
import sys
import copy
import math
import time

# optimization
from scipy.optimize import line_search
from scipy.optimize import fmin_l_bfgs_b

StudySpec = {
    'numImages':
    Config.Param(default=4, required=True,
    comment="Total number of images."),
    'imageIds':
    Config.Param(default=['sid1', 'sid2', 'sid3', 'sid4'], required=True,
    comment="List of image ids. This should be unique for each image."),
    'imagePaths':
    Config.Param(default=['subject1_I.mhd', 'subject2_I.mhd', 'subject3_I.mhd', 'subject4_I.mhd'], required=True,
    comment="List of images files. It is required, even if you only want to regress through deformation fields."),
    'imageTimes':
    Config.Param(default=[0, 0.33, 0.66, 1.0], required=True,
    comment="List of image times in the same order as imagePaths, t (t is in (0,1))."),
    'regressImages':    
    Config.Param(default=True, required=True,
    comment="Whether or not to regress through images."),
    'regressDefFields':    
    Config.Param(default=False, required=True,
    comment="Whether or not to regress through deformation fields."),
    'runSplineExtension':    
    Config.Param(default=True, required=True,
    comment="Whether or not to run extended version of splines."),
    'defPaths':    
    Config.Param(default=['subject1_phiinv.mhd', 'subject2_phiinv.mhd', 'subject3_phiinv.mhd', 'subject4_phiinv.mhd'], required=False,
    comment="List of inverse deformation fields to regress through.")
}

SplinesConfigSpec = {
    'compute': Compute.ComputeConfigSpec,
    'study': StudySpec,
    'splinesconfig': SplinesConfig.ConfigSpec,
    'optim': Optim.OptimConfigSpec,
    'io': {
        'plotEvery':
        Config.Param(default=10,
                     comment="Update plots every N iterations"),
        'plotSlice':
        Config.Param(default=None,
                     comment="Slice to plot.  Defaults to mid axial"),
        'quiverEvery':
        Config.Param(default=1,
                     comment="How much to downsample for quiver plots"),
        'gridEvery':
        Config.Param(default=1,
                     comment="How much to downsample for deformation grid plots"),
        'outputPrefix':
        Config.Param(default="./",
                     comment="Where to put output.  Don't forget trailing "
                     + "slash"),
        'writeAll':
        Config.Param(default=False,
                     comment="Write out all states at all time descretizations?"),
        'saveFrames':
        Config.Param(default=True,
                     comment="Save frames for every timestep for creating video later?")},
    '_resource': 'SplinesConfigSpec'}


class OptimizerResult:
    def __init__(self):
        self.xopt = None
        self.fopt = None
        self.dopt = None


def splines_regression(cf):
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    if cf.compute.useCUDA and cf.compute.gpuID is not None:
        ca.SetCUDADevice(cf.compute.gpuID)

    # prepare output directory
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))

    # Output loaded config
    if cf.io.outputPrefix is not None:
        cfstr = Config.ConfigToYAML(SplinesConfigSpec, cf)
        with open(cf.io.outputPrefix + "parsedconfig.yaml", "w") as f:
            f.write(cfstr)

    mem_type = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST

    # Read input 
    logging.info('Reading program input...')

    def_data = None
    image_data = [common.LoadITKImage(f, mem_type) if isinstance(f, str) else f for f in cf.study.imagePaths]
    if cf.study.regressDefFields:
        def_data = [common.LoadITKField(f, mem_type) if isinstance(f, str) else f for f in cf.study.defPaths]

    image_ids = cf.study.imageIds
    time_data = cf.study.imageTimes
    control_locations = cf.splinesconfig.controlLocations
    logging.info('...done.')

    # start up the memory manager for scratch variables
    logging.info('Initializing memory manager...')    
    grid = image_data[0].grid()

    ca.ThreadMemoryManager.init(grid, mem_type, 0)
    logging.info('...done.')

    logging.info('Setting up time descretization and allocating memory...')
    (time_descretized_array, measurement_indices, control_indices) = Core.setup_timearray(cf.optim.nTimeSteps,
                                                                                          time_data, control_locations,
                                                                                          0.001)
    tdisc_spline = Core.setup_time_discretization(time_descretized_array, image_data, image_ids, measurement_indices,
                                                  control_indices, def_data)
    logging.info('...done.')

    logging.info('Loading initializations...')
    if cf.optim.I0 is not None:
        tdisc_spline[0].I = common.LoadITKImage(cf.optim.I0, tdisc_spline[0].mem_type)
        logging.info('Loaded initial image from %s ', cf.optim.I0)
    if cf.optim.m0 is not None:
        m0 = common.LoadITKField(cf.optim.m0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].m, m0, cf.optim.scaleInits[0]*float(cf.optim.scale))
        logging.info('Loaded initial momenta from %s and scaled it by %f', cf.optim.m0, float(cf.optim.scale))
    if cf.optim.u0 is not None:
        u0 = common.LoadITKField(cf.optim.u0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].u, u0, cf.optim.scaleInits[1]*float(cf.optim.scale)**2.0)
        logging.info('Loaded initial control from %s and scaled it by %f', cf.optim.u0, float(cf.optim.scale)**2)
    if cf.optim.p0 is not None:
        p0 = common.LoadITKField(cf.optim.p0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].p, p0, cf.optim.scaleInits[2]*float(cf.optim.scale)**3.0)
        logging.info('Loaded initial jerk from %s and scaled it by %f', cf.optim.p0, float(cf.optim.scale)**3)
    if cf.optim.pc is not None and len(cf.optim.pc) > 0:
        pc = [common.LoadITKField(f, mem_type) if isinstance(f, str) else f for f in cf.optim.pc]
        for i in range(len(control_indices)):
            ca.MulC(tdisc_spline[control_indices[i]].p, pc[i], cf.optim.scaleInits[3]*float(cf.optim.scale)**3.0)
            logging.info('Loaded initial jerk at controls from %s and scaled it by %f', cf.optim.pc[i], float(cf.optim.scale)**3)

    if not cf.optim.update_I0:
        logging.info('The algorithm will not estimate initial image')
    if not cf.optim.update_m0:
        logging.info('The algorithm will not estimate initial momenta')
    if not cf.optim.update_u0:
        logging.info('The algorithm will not estimate initial force')
    if not cf.optim.update_p0:
        logging.info('The algorithm will not estimate initial jerk')
    if not cf.optim.update_ps:
        logging.info('The algorithm will not estimate jerk at controls')

    logging.info('...done.')

    logging.info('Allocating memory for gradient object...')
    spline_gradient = Core.SplineGradient(grid, mem_type, len(control_indices))
    logging.info('...done.')

    # create differential operator
    logging.info('Creating differential operator...')
    diff_oper = Core.setup_diff_oper(cf.splinesconfig.diffOpParams[0], cf.splinesconfig.diffOpParams[1],
                                     cf.splinesconfig.diffOpParams[2], image_data[0].grid(), mem_type)
    logging.info('...done.')

    # Run spline regression
    logging.info('Running spline regression...')
    optimizer_result, energy_history = run_splines_regression(cf, tdisc_spline, diff_oper, spline_gradient)
    logging.info('...done.')

    # Write output
    logging.info('Saving results...')
    write_output(cf, tdisc_spline, optimizer_result, diff_oper, energy_history)
    logging.info('...done.')


def run_splines_regression(cf, tdisc_spline, diff_oper, spline_gradient):
    energy_history = []
    optimizer_result = None
    grid = spline_gradient.grid
    num_controls = spline_gradient.num_controls

    if cf.optim.method == 'LBFGS':        
        optimizer_result = OptimizerResult()
        # initialize numpy x, grad_x and eps_x arrays
        length_x = 3*3*grid.nVox() + 3*num_controls*grid.nVox()
        x = np.zeros(length_x, dtype='float')
        Core.serialize_into_x(x, tdisc_spline)
        grad_x = np.zeros(length_x, dtype='float')
        #bounds_lst = [(-0.01,0.01),(-0.01,0.01),(-0.05,0.05),(-0.0, 0.0)]
        #bounds_lst = [(None,None),(None,None),(None,None),(None, None)]
        #bounds_lst = [(-0.01,0.01),(-0.01,0.01),(-0.01,0.01),(-0.00, 0.00)]
        bounds_lst =[tuple([None if type(entry) is str else entry for entry in tpl]) for tpl in cf.optim.lbfgs_bounds]
        logging.info('Running LBFGS optimizer...')
        logging.info('Using bounds in LBFGS: %s', bounds_lst)

        bounds_x = Core.create_bounds_x_np(num_controls, grid.nVox(), cf, bounds=bounds_lst)
        factr=1e4; pgtol=1e-05; iprint=2; maxfun=1000; maxiter=500;
        optimizer_result.xopt, optimizer_result.fopt, optimizer_result.dopt = fmin_l_bfgs_b(Core.compute_energy_np,
                                                                                            x,
                                                                                            fprime=Core.compute_gradient_np,
                                                                                            args=(tdisc_spline,
                                                                                                  grad_x, spline_gradient,
                                                                                                  diff_oper, cf),
                                                                                            bounds=bounds_x,
                                                                                            factr=factr,
                                                                                            pgtol=pgtol,
                                                                                            iprint=iprint,
                                                                                            maxfun=maxfun,
                                                                                            maxiter=maxiter)        
        logging.info('Completed LBFGS optimization...')
        if optimizer_result is not None:
            logging.info('Energy at min: %f', optimizer_result.fopt)
            logging.info('Warning flag: %d', optimizer_result.dopt['warnflag'])
            logging.info('Number of function calls: %d', optimizer_result.dopt['funcalls'])
            logging.info('Number of iterations: %d', optimizer_result.dopt['nit'])
            logging.info('Reason for stopping: %s', optimizer_result.dopt['task'])

        if optimizer_result.xopt is not None:
            total_energy_np = Core.compute_energy_np(optimizer_result.xopt, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1)
            logging.info('Energy using numpy datatypes: %0.5f', total_energy_np )
            if  optimizer_result.dopt['warnflag'] == 2:
                logging.info('WARNING!!! Optimizer abnormally terminated! The results may still be meaningful!!!')                
        else:
            raise Exception('Optimizer returned \'None\'!!!')
    elif cf.optim.method == 'LNSRCHGD':
        length_x = 3*3*grid.nVox() + 3*num_controls*grid.nVox()
        x = np.zeros(length_x, dtype='float')
        Core.serialize_into_x(x, tdisc_spline)
        grad_x = np.zeros(length_x, dtype='float')
        eps_x = Core.create_eps_x_np(num_controls, grid.nVox(), cf)
        logging.info('Running gradient descent with line search optimization...')

        # optimize using these numpy datatypes
        stp=None; f_count=None; g_count=None; fval=None; old_fval=None; gval=None;
        skips=0
        for i in range(cf.optim.Niter):
            try:
                Core.unserialize_into_tdisc_spline(tdisc_spline, x)
                total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
                logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1)
                energy_history.append(total_energy)

                # compute search direction
                Core.compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)

                # line search to compute stepsize
                search_dir = -np.multiply(grad_x, eps_x)
                stp, f_count, g_count, fval, old_fval, gval = line_search(Core.compute_energy_np,
                                                                          Core.compute_gradient_np, x,
                                                                          search_dir, gfk=grad_x,
                                                                          old_fval=fval, old_old_fval=old_fval,
                                                                          args=(tdisc_spline, grad_x,
                                                                                spline_gradient, diff_oper,
                                                                                cf), c1=0.0001, c2=0.9,
                                                                          amax=50)
                logging.info('| Stepsize alpha0 = %s | Number of fevals = %s | Number of gradevals = %s |', stp, f_count, g_count)

                if gval is not None and fval is not None and stp is not None and not math.isnan(fval):
                    # take the step
                    x += search_dir*stp
                    logging.info('Effective stepsize with which the gradients for m is scaled by is %0.10f', stp*cf.optim.stepSizem0)
                    logging.info('-----------------------------------------------------------')
                    skips=0
                else:
                    print (stp, f_count, g_count, fval, old_fval, gval)
                    logging.info('WARNING!!! Line search resulting in NaNs or None. Reducing stepsize by 2 and skipping this update.')
                    eps_x *= 0.5
                    cf.optim.stepSizem0 *= 0.5
                    fval = None
                    old_fval = None
                    skips+=1
                    if skips > 20:
                        logging.info('Algorithm has skipped updates for last twenty iterations. It will therefore quit.')
                        break
            except:
                logging.info('Unexpected error occured inside the line search gradient descent loop!')
                logging.info('Unexpected error: %s', sys.exc_info()[0])
                logging.info('Program will still write output...')
                logging.info('-----------------------------------------------------------')
                break
    elif cf.optim.method == 'LBFGSFD':
        optimizer_result = OptimizerResult()
        # initialize numpy x, grad_x and eps_x arrays
        length_x = 3*3*grid.nVox() + 3*num_controls*grid.nVox()
        x = np.zeros(length_x, dtype='float')
        grad_x = np.zeros(length_x, dtype='float')
        #bounds_lst = [(-0.01,0.01),(-0.01,0.01),(-0.05,0.05),(-0.0, 0.0)]
        #bounds_lst = [(None,None),(None,None),(None,None),(None, None)]
        bounds_lst = [(-0.01,0.01),(-0.01,0.01),(-0.00,0.01),(-0.00, 0.00)]
        bounds_x = Core.create_bounds_x_np(num_controls, grid.nVox(), cf, bounds=bounds_lst)
        factr=1e1; pgtol=1e-05; iprint=0; maxfun=1000; maxiter=100;
        
        optimizer_result.xopt, optimizer_result.fopt, optimizer_result.dopt = fmin_l_bfgs_b(Core.compute_energy_np,
                                                                                            x,
                                                                                            fprime=Core.compute_gradient_np_fdapprox,
                                                                                            args=(tdisc_spline,
                                                                                                  grad_x, spline_gradient,
                                                                                                  diff_oper, cf),
                                                                                            bounds=bounds_x,
                                                                                            factr=factr,
                                                                                            pgtol=pgtol,
                                                                                            iprint=iprint,
                                                                                            maxfun=maxfun,
                                                                                            maxiter=maxiter)        
        if optimizer_result is not None:
            logging.info('Energy at min: %f', optimizer_result.fopt)
            logging.info('Warning flag: %d', optimizer_result.dopt['warnflag'])
            logging.info('Number of function calls: %d', optimizer_result.dopt['funcalls'])
            logging.info('Number of iterations: %d', optimizer_result.dopt['nit'])
            logging.info('Reason for stopping: %s', optimizer_result.dopt['task'])

        if optimizer_result.xopt is not None and optimizer_result.dopt['warnflag'] != 2:
            total_energy_np = Core.compute_energy_np(optimizer_result.xopt, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 )
            logging.info('Energy using numpy datatypes: %0.5f', total_energy_np )
        else:
            raise Exception('Optimizer returned \'None\' or it was abnormally terminated!!!')            
    elif cf.optim.method == 'FIXEDGDNP':
        # initialize numpy x, grad_x and eps_x arrays
        length_x = 3*3*grid.nVox() + 3*num_controls*grid.nVox()
        x = np.zeros(length_x, dtype='float')
        grad_x = np.zeros(length_x, dtype='float')
        eps_x = Core.create_eps_x_np(num_controls, grid.nVox(), cf)
        
        # optimize using these numpy datatypes
        for i in range(cf.optim.Niter):
            # print energy
            total_energy_np = Core.compute_energy_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 )
            logging.info('Iter: %d, Energy using numpy datatypes: %0.5f', i, total_energy_np )
            energy_history.append(total_energy_np)
            # compute gradient
            Core.compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            
            # take gradient steps
            x -= np.multiply(grad_x, eps_x)
            
            # error checking
            #grad_error = Core.check_gradient(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            #eps=0.000001
            #Core.compute_gradient_np_fdapprox(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf, eps)
            #logging.info('Iter: %d, Error in gradient compared to finite difference gradient: %0.5f', i, grad_error )
            
        # when optimization complete, copy x and grad_x back to pyca datatypes
        # compute_gradient_np will do this
        # Core.compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    elif cf.optim.method == 'DEBUG':
        # initialize numpy x, grad_x and eps_x arrays
        length_x = 3*3*grid.nVox() + 3*num_controls*grid.nVox()
        x = np.zeros(length_x, dtype='float')
        grad_x = np.zeros(length_x, dtype='float')
        eps_x = Core.create_eps_x_np(num_controls, grid.nVox(), cf)
        #Core.compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)
        #Core.serialize_into_grad_x(grad_x, spline_gradient)
        #Core.unserialize_into_tdisc_spline(tdisc_spline, x)
        #Core.compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
        #Core.unserialize_into_tdisc_spline(tdisc_spline, x)

        # calculate gradient at x since now x is copied to pyca type datatype
        #Core.compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)
    
        # convert the gradient value in spline_gradient to numpy array to return
        #Core.serialize_into_grad_x(grad_x, spline_gradient)

        # optimize using these numpy datatypes
        for i in range(cf.optim.Niter):                    
            total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 )
            energy_history.append(total_energy)

            # comppute automatic differentiation gradient and save
            #logging.info('Running Automatic Differentiation')
            #Core.compute_gradient_np_ad(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)                    
            #save_gradients(cf, spline_gradient, iter_id=i, suffix='_ad')
            
            # compute finite difference gradient and save
            logging.info('Running Finite Difference Approximation')
            eps=0.0000001
            Core.compute_gradient_np_fdapprox(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf, eps)                    
            save_gradients(cf, spline_gradient, iter_id=i, suffix='_fd', vmin=-1000.0, vmax=1000.0)
            
            # compute gradient and save
            logging.info('Running My Own Gradient')
            Core.compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
            save_gradients(cf, spline_gradient, iter_id=i, suffix='', vmin=-1000.0, vmax=1000.0)
                              
            # take gradient steps 
            x -= np.multiply(grad_x, eps_x)

        
    elif cf.optim.method == 'FIXEDGD':
        for i in range(cf.optim.Niter):
            # print energy
            total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1  = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1)
            energy_history.append(total_energy)
            # compute gradient
            Core.compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)

            # take gradient steps in m(0), u(0) and p(0)
            if cf.optim.update_m0:
                ca.Add_MulC_I(tdisc_spline[0].m, spline_gradient.grad_m, -cf.optim.stepSizem0)
            if cf.optim.update_u0:
                ca.Add_MulC_I(tdisc_spline[0].u, spline_gradient.grad_u, -cf.optim.stepSizeu0)
            if cf.optim.update_p0:
                ca.Add_MulC_I(tdisc_spline[0].p, spline_gradient.grad_p, -cf.optim.stepSizep0)
            
            # take gradient steps in controls, p(t_c)
            if not cf.optim.fitGeodesic:
                controls_counter = 0
                if spline_gradient.num_controls > 0:
                    logging.info('Starting to update controls...')
                    for j in range(len(tdisc_spline)):
                        if tdisc_spline[j].is_control:
                            ca.Add_MulC_I(tdisc_spline[j].p, spline_gradient.grad_controls[controls_counter],
                                          -cf.optim.stepSizepc)
                            controls_counter += 1
                            logging.info('Updated p(t_c) for control %d at t=%f at location %d', controls_counter, tdisc_spline[j].t, j)
                    logging.info('...done.')

            # closed form update for initial image
            if cf.optim.update_I0:
                # closed-form for image update
                scratch_I = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                im_ones = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_splat_J = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_jacs = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                ca.SetMem(im_ones, 1.0)
                ca.SetMem(sum_splat_J, 0.0)
                ca.SetMem(sum_jacs, 0.0)
                for j in range(len(tdisc_spline)):
                    if tdisc_spline[j].has_data:
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, tdisc_spline[j].J)
                        ca.Add_I(sum_splat_J, scratch_I)
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, im_ones)
                        ca.Add_I(sum_jacs, scratch_I)
                ca.Div(tdisc_spline[0].I, sum_splat_J, sum_jacs)

    total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = Core.compute_energy(cf, tdisc_spline, diff_oper)
    logging.info('After final iter, energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f+%0.5f', total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1)
    energy_history.append(total_energy)
    return optimizer_result, energy_history


def write_output(cf, tdisc_spline, optimizer_result, diff_oper, energy_history):
    
    #ENERGY
    fig = plt.figure(1)
    plt.clf()
    fig.patch.set_facecolor('white')
    plt.plot(energy_history)
    plt.title('Total Energy')
    plt.draw()
    plt.show()

    plt.savefig(cf.io.outputPrefix+'energy.pdf')
    plt.savefig(cf.io.outputPrefix+'energy.png')

    write_list_to_file(energy_history, cf.io.outputPrefix+'energy.csv')
    '''
    if optimizer_result is not None:
        logging.info('Energy at min: %f', optimizer_result.fopt)
        logging.info('Warning flag: %d', optimizer_result.dopt['warnflag'])
        logging.info('Number of function calls: %d', optimizer_result.dopt['funcalls'])
        logging.info('Number of iterations: %d', optimizer_result.dopt['nit'])
        logging.info('Reason for stopping: %s', optimizer_result.dopt['task'])
        # convert optimizer result to pyca style output to save as images
        #Core.unserialize_into_splinestate_array_debug(tdisc_spline, optimizer_result.xopt, tdisc_spline[0].grid,
        #                                    tdisc_spline[0].mem_type)
    '''

    if cf.optim.integMethod == "EULER":
        Core.shoot_forward(cf, tdisc_spline, diff_oper)
    elif cf.optim.integMethod == "RK4":
        Core.shoot_forward_rk4_direct(cf, tdisc_spline, diff_oper)
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)
    # write out initial conditions
    common.SaveITKField(tdisc_spline[0].m, cf.io.outputPrefix + "m00.mhd")
    common.SaveITKField(tdisc_spline[0].u, cf.io.outputPrefix + "u00.mhd")
    common.SaveITKField(tdisc_spline[0].p, cf.io.outputPrefix + "p00.mhd")
    common.SaveITKImage(tdisc_spline[0].I, cf.io.outputPrefix + "I00.mhd")

    # write out P at controls
    ctrl_id=0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            common.SaveITKField(tdisc_spline[i].p, cf.io.outputPrefix + "p_ctrl_"+str(ctrl_id)+".mhd")
            ctrl_id+=1

    if cf.io.writeAll:
        for i in range(1,len(tdisc_spline)):
            common.SaveITKField(tdisc_spline[i].m, cf.io.outputPrefix + "m"+str(i).zfill(2)+".mhd")
            common.SaveITKField(tdisc_spline[i].u, cf.io.outputPrefix + "u"+str(i).zfill(2)+".mhd")
            common.SaveITKField(tdisc_spline[i].p, cf.io.outputPrefix + "p"+str(i).zfill(2)+".mhd")
            common.SaveITKImage(tdisc_spline[i].I, cf.io.outputPrefix + "I"+str(i).zfill(2)+".mhd")

    # write out frames
    if cf.io.saveFrames:
        save_frames(cf, tdisc_spline)

def save_gradients(cf, spline_gradient, iter_id=0, suffix='', vmin=None, vmax=None):
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/')
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/extras/gradients/')

    fig = plt.figure('grad_m', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(spline_gradient.grad_m, '', vmin, vmax, plot_vector=True) 
    fig.set_size_inches(12, 12)
    outfilename = cf.io.outputPrefix+'/frames/extras/gradients/grad_m_'+str(iter_id).zfill(5)+suffix+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('grad_u', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(spline_gradient.grad_u, '', vmin, vmax, plot_vector=True) 
    fig.set_size_inches(12, 12)
    outfilename = cf.io.outputPrefix+'/frames/extras/gradients/grad_u_'+str(iter_id).zfill(5)+suffix+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('grad_p', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(spline_gradient.grad_p, '', vmin/5, vmax/5, plot_vector=True) 
    fig.set_size_inches(12, 12)
    outfilename = cf.io.outputPrefix+'/frames/extras/gradients/grad_p_'+str(iter_id).zfill(5)+suffix+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    
    for i in range(spline_gradient.num_controls):
        fig = plt.figure('grad_pc', frameon=False) 
        plt.clf()
        Core.plot_vf_2d(spline_gradient.grad_controls[i], '', vmin/5, vmax/5, plot_vector=True)
        fig.set_size_inches(12, 12)
        outfilename = cf.io.outputPrefix+'/frames/extras/gradients/grad_p'+str(i).zfill(2)+'_'+str(iter_id).zfill(5)+suffix+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    

def save_frames(cf, tdisc_spline):
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/')
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/extras/')
    
    image_idx = 0

    for i in range(len(tdisc_spline)):
        fig = plt.figure('images', frameon=False)
        plt.clf()
        display.DispImage(tdisc_spline[i].I, '', newFig=False, cmap='gray', sliceIdx=cf.io.plotSlice)
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/I'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/I'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

        fig = plt.figure('invdef',frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].ginv, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/invdef'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/invdef'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)

        fig = plt.figure('def', frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].g, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/def'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/def'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        # plot extras
        save_extra_frames(cf, tdisc_spline, i, image_idx)

        image_idx += 1

def save_extra_frames(cf, tdisc_spline, i, image_idx): 
    fig = plt.figure('momenta', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].m, '') 
    fig.set_size_inches(12, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/m'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100) 
    outfilename = cf.io.outputPrefix+'/frames/extras/m'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('control', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].u, '') 
    fig.set_size_inches(12, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100) 
    outfilename =  cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('jerk', frameon=False) 
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].p, '') 
    fig.set_size_inches(12, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,  dpi=100) 
    outfilename = cf.io.outputPrefix+'/frames/extras/p'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

def write_list_to_file(listname, filename):
    try:
        os.remove(filename)
    except OSError:
        pass
    theFile = open(filename, 'w')
    for item in listname:
        print>>theFile, item
    theFile.close()


if __name__ == '__main__':
    #common.DebugHere()

    try:
        usercfg = Config.Load(spec=SplinesConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        # Don't freak out, this should have printed a sample config to STDOUT
        sys.exit(1)

    splines_regression(usercfg)

