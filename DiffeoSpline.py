#!/usr/bin/python2

# from vectormomenta config module. must be made general package by itself 
from Configs import Config, Compute

# pyca modules
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
import Libraries.CAvmCommon as cavmcommon

# diffeo_splines modules
import Core
import Optim
import SplinesConfig
# others
import numpy as np
import matplotlib
#matplotlib.use('GTKAgg')
import matplotlib.pyplot as plt
import os
import errno

import logging
import sys
import copy
import math
import time

# optimization
from scipy.optimize import minimize
from scipy.optimize import fmin_l_bfgs_b

StudySpec = {
    'numImages':
    Config.Param(default=4, required=True,
    comment="Total number of images."),
    'imageIds':
    Config.Param(default=['sid1', 'sid2', 'sid3', 'sid4'], required=True,
    comment="List of image ids. This should be unique for each image."),
    'imagePaths':
    Config.Param(default=['subject1_I.mhd', 'subject2_I.mhd', 'subject3_I.mhd', 'subject4_I.mhd'], required=True,
    comment="List of images files. It is required, even if you only want to regress through deformation fields."),
    'imageTimes':
    Config.Param(default=[0, 0.33, 0.66, 1.0], required=True,
    comment="List of image times in the same order as imagePaths, t (t is in (0,1))."),
    'regressImages':    
    Config.Param(default=True, required=True,
    comment="Whether or not to regress through images."),
    'regressDefFields':    
    Config.Param(default=False, required=True,
    comment="Whether or not to regress through deformation fields."),
    'runSplineExtension':    
    Config.Param(default=True, required=True,
    comment="Whether or not to run extended version of splines."),
    'defPaths':    
    Config.Param(default=['subject1_phiinv.mhd', 'subject2_phiinv.mhd', 'subject3_phiinv.mhd', 'subject4_phiinv.mhd'], required=False,
    comment="List of inverse deformation fields to regress through.")
}

SplinesConfigSpec = {
    'compute': Compute.ComputeConfigSpec,
    'study': StudySpec,
    'splinesconfig': SplinesConfig.ConfigSpec,
    'optim': Optim.OptimConfigSpec,
    'io': {
        'plotEvery':
        Config.Param(default=10,
                     comment="Update plots every N iterations"),
        'plotSlice':
        Config.Param(default=None,
                     comment="Slice to plot.  Defaults to mid axial"),
        'quiverEvery':
        Config.Param(default=1,
                     comment="How much to downsample for quiver plots"),
        'gridEvery':
        Config.Param(default=1,
                     comment="How much to downsample for deformation grid plots"),
        'outputPrefix':
        Config.Param(default="./",
                     comment="Where to put output.  Don't forget trailing "
                     + "slash"),
        'writeAll':
        Config.Param(default=False,
                     comment="Write out all states at all time descretizations?"),
        'saveFrames':
        Config.Param(default=True,
                     comment="Save frames for every timestep for creating video later?")},
    '_resource': 'SplinesConfigSpec'}


class OptimizerResult:
    def __init__(self):
        self.xopt = None
        self.fopt = None
        self.dopt = None


def splines_regression(cf):
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    if cf.compute.useCUDA and cf.compute.gpuID is not None:
        ca.SetCUDADevice(cf.compute.gpuID)

    # prepare output directory
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))

    # Output loaded config
    if cf.io.outputPrefix is not None:
        cfstr = Config.ConfigToYAML(SplinesConfigSpec, cf)
        with open(cf.io.outputPrefix + "parsedconfig.yaml", "w") as f:
            f.write(cfstr)

    mem_type = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST

    # Read input 
    logging.info('Reading program input...')

    def_data = None
    image_data = [common.LoadITKImage(f, mem_type) if isinstance(f, str) else f for f in cf.study.imagePaths]
    if cf.study.regressDefFields:
        def_data = [common.LoadITKField(f, mem_type) if isinstance(f, str) else f for f in cf.study.defPaths]

    image_ids = cf.study.imageIds
    time_data = cf.study.imageTimes
    control_locations = cf.splinesconfig.controlLocations
    logging.info('...done.')

    # start up the memory manager for scratch variables
    logging.info('Initializing memory manager...')    
    grid = image_data[0].grid()

    ca.ThreadMemoryManager.init(grid, mem_type, 0)
    logging.info('...done.')

    logging.info('Setting up time descretization and allocating memory...')
    (time_descretized_array, measurement_indices, control_indices) = Core.setup_timearray(cf.optim.nTimeSteps,
                                                                                          time_data, control_locations,
                                                                                          0.001)
    tdisc_spline = Core.setup_time_discretization(time_descretized_array, image_data, image_ids, measurement_indices,
                                                  control_indices, def_data)
    logging.info('...done.')

    logging.info('Loading initializations...')
    if cf.optim.I0 is not None:
        tdisc_spline[0].I = common.LoadITKImage(cf.optim.I0, tdisc_spline[0].mem_type)
        logging.info('Loaded initial image from %s ', cf.optim.I0)
    if cf.optim.m0 is not None:
        m0 = common.LoadITKField(cf.optim.m0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].m, m0, float(cf.optim.scale))
        logging.info('Loaded initial momenta from %s and scaled it by %f', cf.optim.m0, float(cf.optim.scale))
    if cf.optim.u0 is not None:
        u0 = common.LoadITKField(cf.optim.u0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].u, u0, float(cf.optim.scale)**2.0)
        logging.info('Loaded initial control from %s and scaled it by %f', cf.optim.u0, float(cf.optim.scale)**2)
    if cf.optim.P0 is not None:
        P0 = common.LoadITKImage(cf.optim.P0, tdisc_spline[0].mem_type)
        ca.MulC(tdisc_spline[0].P, P0, float(cf.optim.scale)**3.0)
        logging.info('Loaded initial jerk from %s and scaled it by %f', cf.optim.P0, float(cf.optim.scale)**3)
    if not cf.optim.update_I0:
        logging.info('The algorithm will not estimate initial image')
    if not cf.optim.update_m0:
        logging.info('The algorithm will not estimate initial momenta')
    if not cf.optim.update_u0:
        logging.info('The algorithm will not estimate initial control')
    if not cf.optim.update_P0:
        logging.info('The algorithm will not estimate initial jerk')
    logging.info('...done.')

    logging.info('Allocating memory for gradient object...')
    spline_gradient = Core.SplineGradientDebug(grid, mem_type, len(control_indices))
    logging.info('...done.')

    # create differential operator
    logging.info('Creating differential operator...')
    diff_oper = Core.setup_diff_oper(cf.splinesconfig.diffOpParams[0], cf.splinesconfig.diffOpParams[1],
                                     cf.splinesconfig.diffOpParams[2], image_data[0].grid(), mem_type)
    logging.info('...done.')

    # Run spline regression
    logging.info('Running spline regression...')
    optimizer_result, energy_history = run_splines_regression(cf, tdisc_spline, diff_oper, spline_gradient)
    logging.info('...done.')

    # Write output
    logging.info('Saving results...')
    write_output(cf, tdisc_spline, optimizer_result, diff_oper, energy_history)
    logging.info('...done.')


def objective(x, cf, tdisc_spline, diff_oper, spline_gradient):
    total_energy = 0.0

    # Convert x to pyca style vars. Basically, copy it into tdisc_spline
    Core.unserialize_into_splinestate_array_debug(tdisc_spline, x, tdisc_spline[0].grid, tdisc_spline[0].mem_type,
                                                  buffer_I=spline_gradient.buffer_I, buffer_v=spline_gradient.buffer_v)

    # Evaluate energy
    total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match = Core.compute_energy(cf, tdisc_spline, diff_oper)

    # Convert back to scipy type output to return

    return total_energy


def grad_objective(x, cf, tdisc_spline, diff_oper, spline_gradient):
    # Convert x to pyca style vars. Basically, copy it into tdisc_spline
    Core.unserialize_into_splinestate_array_debug(tdisc_spline, x, tdisc_spline[0].grid, tdisc_spline[0].mem_type,
                                                  buffer_I=spline_gradient.buffer_I, buffer_v=spline_gradient.buffer_v)
    # Evaluate gradients
    Core.compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)
    # nikhil: debug
    #ca.SetMem(spline_gradient.grad_I, 0.0)

    # Convert back to scipy type output in same configuration as the input x to return
    spline_gradient.serialize_asnp()

    return spline_gradient.serialized_array


def create_bounds_array(num_controls, num_vox):
    # first allocate memory
    # nikhil: debug
    #length_x = 2*3*num_vox + 2*num_vox + num_controls*num_vox

    length_x = 2*3*num_vox + 1*num_vox + num_controls*num_vox
    out_range = [(0.0, 0.0) for i in range(length_x)]

    counter = 0
    for i in range(counter*num_vox, (counter+7)*num_vox):
        out_range[i] = (-0.1, 0.1)
    counter += 7

    # nikhil: debug
    #for i in range(counter*num_vox, (counter+1)*num_vox):
    #    out_range[i] = (-0.0000001, 1.0000001)
    #counter += 1

    for i in range(num_controls):
        for i in range(counter*num_vox, (counter+1)*num_vox):
            out_range[i] = (-0.1, 0.1)
        counter += 1

    return out_range


def create_bounds_array_debug(num_controls, num_vox):
    # first allocate memory
    length_x = 2*3*num_vox + 1*num_vox + num_controls*num_vox
    out_range = [(0.0, 0.0) for i in range(length_x)]

    counter = 0
    for i in range(counter*num_vox, (counter+2*3+1+num_controls)*num_vox):
        out_range[i] = (None, None)

    return out_range


def run_splines_regression(cf, tdisc_spline, diff_oper, spline_gradient):
    energy_history = []
    spline_gradient.serialize_asnp()
    x0 = np.zeros_like(spline_gradient.serialized_array)
    x_bounds = create_bounds_array_debug(spline_gradient.num_controls, tdisc_spline[0].grid.nVox())
    if cf.optim.fitGeodesic:
        ca.SetMem(tdisc_spline[0].u, 0.0)
        ca.SetMem(tdisc_spline[0].P, 0.0)

    # use optimization toolbox and bfgs to optimize
    '''
    # below does not work and gives "Memory error"
    optimizer_result = minimize(objective, x0, method='BFGS', jac=grad_objective, args=(cf, tdisc_spline, diff_oper,
                                                                                       spline_gradient),
                                options={'disp': True})
    '''
    optimizer_result = None

    if cf.optim.method == 'LBFGS':
        optimizer_result = OptimizerResult()
        for i in range(cf.optim.Niter):
            # optimize over m, u, P and controls
            total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, regulaizer_P0, regularizer_m0, regularizer_u0, image_match, def_match )
            energy_history.append(total_energy)
            optimizer_result.xopt, optimizer_result.fopt, optimizer_result.dopt = \
            fmin_l_bfgs_b(objective, x0, fprime=grad_objective, args=(cf, tdisc_spline, diff_oper, spline_gradient))
            #fmin_l_bfgs_b(objective, x0, fprime=grad_objective, args=(cf, tdisc_spline, diff_oper, spline_gradient),
            #              bounds=x_bounds, approx_grad=0)
            # update initial image
            # closed-form for image update
            if cf.optim.update_I0:
                scratch_I = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                im_ones = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_splat_J = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_jacs = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                ca.SetMem(im_ones, 1.0)
                ca.SetMem(sum_splat_J, 0.0)
                ca.SetMem(sum_jacs, 0.0)
                for j in range(len(tdisc_spline)):
                    if tdisc_spline[j].has_data:
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, tdisc_spline[j].J)
                        ca.Add_I(sum_splat_J, scratch_I)
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, im_ones)
                        ca.Add_I(sum_jacs, scratch_I)
                ca.Div(tdisc_spline[0].I, sum_splat_J, sum_jacs)

    elif cf.optim.method == 'FIXEDGD':
        for i in range(cf.optim.Niter):
            # print energy
            total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match = Core.compute_energy(cf, tdisc_spline, diff_oper)
            logging.info('Iter: %d, Energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f', i, total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match )
            energy_history.append(total_energy)
            # compute gradient
            Core.compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)

            # take gradient steps in m(0), u(0) and P(0)
            if cf.optim.update_m0:
                ca.Add_MulC_I(tdisc_spline[0].m, spline_gradient.grad_m, -cf.optim.stepSize)
            if not cf.optim.fitGeodesic:
                if cf.optim.update_u0:
                    ca.Add_MulC_I(tdisc_spline[0].u, spline_gradient.grad_u, -cf.optim.stepSizeu0)
                if cf.optim.update_P0:
                    ca.Add_MulC_I(tdisc_spline[0].P, spline_gradient.grad_P, -cf.optim.stepSizeP0)

            # take gradient steps in controls, P(t_c)
            if not cf.optim.fitGeodesic:
                controls_counter = 0
                if spline_gradient.num_controls > 0:
                    logging.info('Starting to update controls...')

                    for j in range(len(tdisc_spline)):
                        if tdisc_spline[j].is_control:
                            ca.Add_MulC_I(tdisc_spline[j].P, spline_gradient.grad_controls[controls_counter],
                                          -cf.optim.stepSizePc)
                            controls_counter += 1
                            logging.info('Updated P(t_c) for control %d at t=%f at location %d', controls_counter, tdisc_spline[j].t, j)
                    logging.info('...done.')

            # closed form update for initial image
            if cf.optim.update_I0:
                # ca.Add_MulC_I(tdisc_spline[0].I, spline_gradient.grad_I, -cf.optim.stepSize)
                # closed-form for image update
                scratch_I = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                im_ones = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_splat_J = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                sum_jacs = ca.ManagedImage3D(spline_gradient.grid, spline_gradient.mem_type)
                ca.SetMem(im_ones, 1.0)
                ca.SetMem(sum_splat_J, 0.0)
                ca.SetMem(sum_jacs, 0.0)
                for j in range(len(tdisc_spline)):
                    if tdisc_spline[j].has_data:
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, tdisc_spline[j].J)
                        ca.Add_I(sum_splat_J, scratch_I)
                        cavmcommon.SplatSafe(scratch_I, tdisc_spline[j].ginv, im_ones)
                        ca.Add_I(sum_jacs, scratch_I)
                ca.Div(tdisc_spline[0].I, sum_splat_J, sum_jacs)

    total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match = Core.compute_energy(cf, tdisc_spline, diff_oper)
    logging.info('After final iter, energy: %0.5f = %0.5f+%0.5f+%0.5f+%0.5f+%0.5f', total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match)
    energy_history.append(total_energy)
    return optimizer_result, energy_history


def write_output(cf, tdisc_spline, optimizer_result, diff_oper, energy_history):
    logging.info('in write_output...')

    #ENERGY
    fig = plt.figure(1)
    logging.info('in write_output...2')
    plt.clf()
    fig.patch.set_facecolor('white')

    logging.info('in write_output...3')
    plt.plot(energy_history)

    logging.info('in write_output...4')
    plt.title('Total Energy')
    plt.draw()
    plt.show()

    plt.savefig(cf.io.outputPrefix+'energy.pdf')
    plt.savefig(cf.io.outputPrefix+'energy.png')

    write_list_to_file(energy_history, cf.io.outputPrefix+'energy.csv')

    if optimizer_result is not None:
        logging.info('Energy at min: %f', optimizer_result.fopt)
        logging.info('Warning flag: %d', optimizer_result.dopt['warnflag'])
        logging.info('Number of function calls: %d', optimizer_result.dopt['funcalls'])
        logging.info('Number of iterations: %d', optimizer_result.dopt['nit'])

        # convert optimizer result to pyca style output to save as images
        Core.unserialize_into_splinestate_array_debug(tdisc_spline, optimizer_result.xopt, tdisc_spline[0].grid,
                                            tdisc_spline[0].mem_type)

    if cf.optim.integMethod == "EULER":
        Core.shoot_forward(cf, tdisc_spline, diff_oper)
    elif cf.optim.integMethod == "RK4":
        Core.shoot_forward_rk4_direct(cf, tdisc_spline, diff_oper)
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)
    # write out initial conditions
    common.SaveITKField(tdisc_spline[0].m, cf.io.outputPrefix + "m00.mhd")
    common.SaveITKField(tdisc_spline[0].u, cf.io.outputPrefix + "u00.mhd")
    common.SaveITKImage(tdisc_spline[0].P, cf.io.outputPrefix + "P00.mhd")
    common.SaveITKImage(tdisc_spline[0].I, cf.io.outputPrefix + "I00.mhd")

    # write out P at controls
    ctrl_id=0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            common.SaveITKImage(tdisc_spline[i].P, cf.io.outputPrefix + "P_ctrl_"+str(ctrl_id)+".mhd")
            ctrl_id+=1

    if cf.io.writeAll:
        for i in range(1,len(tdisc_spline)):
            common.SaveITKField(tdisc_spline[i].m, cf.io.outputPrefix + "m"+str(i).zfill(2)+".mhd")
            common.SaveITKField(tdisc_spline[i].u, cf.io.outputPrefix + "u"+str(i).zfill(2)+".mhd")
            common.SaveITKImage(tdisc_spline[i].P, cf.io.outputPrefix + "P"+str(i).zfill(2)+".mhd")
            common.SaveITKImage(tdisc_spline[i].I, cf.io.outputPrefix + "I"+str(i).zfill(2)+".mhd")

    logging.info('in write_output...3')
    # write out frames
    if cf.io.saveFrames:
        save_frames(cf, tdisc_spline)


def save_frames(cf, tdisc_spline):
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/')
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/extras/')

    image_idx = 0

    for i in range(len(tdisc_spline)):
        fig = plt.figure('images', frameon=False)
        plt.clf()
        display.DispImage(tdisc_spline[i].I, '', newFig=False, cmap='gray', sliceIdx=cf.io.plotSlice)
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/I'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/I'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

        fig = plt.figure('invdef',frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].ginv, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/invdef'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/invdef'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)

        fig = plt.figure('def', frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].g, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        fig.set_size_inches(4, 4)
        #outfilename = cf.io.outputPrefix+'/frames/def'+str(image_idx).zfill(5)+'.pdf'
        #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        outfilename = cf.io.outputPrefix+'/frames/def'+str(image_idx).zfill(5)+'.png'
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)
        # plot extras
        save_extra_frames(cf, tdisc_spline, i, image_idx)

        image_idx += 1


def save_extra_frames(cf, tdisc_spline, i, image_idx):
    fig = plt.figure('momenta', frameon=False)
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].m, '')
    fig.set_size_inches(12, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/m'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    outfilename = cf.io.outputPrefix+'/frames/extras/m'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('control', frameon=False)
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].u, '')
    fig.set_size_inches(12, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    outfilename = cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('jerk', frameon=False)
    plt.clf()
    Core.plot_im_2d(tdisc_spline[i].P, '')
    fig.set_size_inches(4, 4)
    #outfilename = cf.io.outputPrefix+'/frames/extras/P'+str(image_idx).zfill(5)+'.pdf'
    #plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    outfilename = cf.io.outputPrefix+'/frames/extras/P'+str(image_idx).zfill(5)+'.png'
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

def write_list_to_file(listname, filename):
    try:
        os.remove(filename)
    except OSError:
        pass
    theFile = open(filename, 'w')
    for item in listname:
        print>>theFile, item
    theFile.close()


if __name__ == '__main__':
    #common.DebugHere()

    try:
        usercfg = Config.Load(spec=SplinesConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        # Don't freak out, this should have printed a sample config to STDOUT
        sys.exit(1)

    splines_regression(usercfg)

