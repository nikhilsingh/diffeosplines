"""Core components of splines config"""
from Configs import Config
import PyCA.Core as ca

ConfigSpec = {
    'stdNoise':
    Config.Param(default=0.1,
                 required=True,
                 comment="Standard deviation for noise in measurements (sigma)"),
    'stdNoiseDef':
    Config.Param(default=0.1,
                 required=True,
                 comment="Standard deviation for noise in deformations (sigma_def)"),
    'controlLocations':
    Config.Param(default=[0.5], 
                 required=False,
                 comment="List of data-independent control locations, t_c (t_c is in (0,1))."),
    'diffOpParams':
    Config.Param(default=[0.5, 0.5, 0.001],
                 required=True,
                 comment="Differential operator parameters: alpha, beta and gamma")
    }
