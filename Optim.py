"""Generalized optimization configs and routines"""

from Configs import Config

def _valspec(_):
    """Hook to do extra validation"""
    pass

OptimConfigSpec = {
    'Niter':
    Config.Param(default=100,
                 comment="Number gradient descent iterations to run"),
    
    'stepSize':
    Config.Param(default=0.01,
                 comment="Gradient descent step size used if FIXEDGD is selected as method of optimization"),
    'stepSizeu0':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for initial force. It is used if FIXEDGD is selected as method of optimization"),
    'stepSizeP0':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for initial jerk. It is used if FIXEDGD is selected as method of optimization"),
    'stepSizePc':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for jerks at controls. It is used if FIXEDGD is selected as method of optimization"),
    'method':
    Config.Param(default="FIXEDGD",
                 comment="Optimization scheme.  FIXEDGD or LBFGS"),
    'NIterForInverse':
    Config.Param(default=5,
                 comment="Iterations for computing fixed point iterative inverse of a diffeomorphism."),
    'integMethod':
    Config.Param(default="EULER",
                 comment="Integration scheme.  EULER or RK4"),
    'nTimeSteps':
    Config.Param(default=10,
                 comment="Number of time discretization steps"),
    'update_I0':
    Config.Param(default=False,
                 comment="Fixes initial image if set to False to the initiliazation in I0 otherwise estimates it"),
    'I0':
    Config.Param(default=None,
                 comment="Fixed initial image"),
    'update_m0':
    Config.Param(default=True,
                 comment="Fixes initial momenta if set to False to the initiliazation in m0, otherwise estimates it"),
    'm0':
    Config.Param(default=None,
                 comment="Fixed initial momenta"),
    'update_u0':
    Config.Param(default=True,
                 comment="Fixes initial control if set to False to the initiliazation in u0, otherwise estimates it"),
    'regularize_P0':
    Config.Param(default=False,
                 comment="Regularize the initial jerk?"),
    'r_P0':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for jerk. It is used when regularize_P0 = True"),
    'regularize_m0':
    Config.Param(default=False,
                 comment="Regularize the initial momenta?"),
    'r_m0':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for momenta. It is used when regularize_m0 = True"),
    'regularize_u0':
    Config.Param(default=False,
                 comment="Regularize the initial acceleration?"),
    'r_u0':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for acceleration. It is used when regularize_u0 = True"),
    'u0':
    Config.Param(default=None,
                 comment="Fixed initial control"),
    'update_P0':
    Config.Param(default=True,
                 comment="Fixes initial jerk if set to False to the initiliazation in P0, otherwise estimates it"),
    'P0':
    Config.Param(default=None,
                 comment="Fixed initial jerk"),
    'regularize_Ps':
    Config.Param(default=False,
                 comment="Regularize the jerk at controls?"),
    'r_Ps':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for the jerks at all controls. It is used when regularize_Ps = True"),
    'fitGeodesic':
    Config.Param(default=False,
                 comment="Whether to fit only a geodesic!"),
    'scale':
    Config.Param(default=1.0,
    comment="Scale all the initial conditions by this amount for initialization."),
    '_resource': "Optim",
    '_validation_hook': _valspec}
