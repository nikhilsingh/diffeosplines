"""Generalized optimization configs and routines"""

from Configs import Config

def _valspec(_):
    """Hook to do extra validation"""
    pass

OptimConfigSpec = {
    'Niter':
    Config.Param(default=100,
                 comment="Number gradient descent iterations to run"),
    
    'stepSizem0':
    Config.Param(default=0.01,
                 comment="Gradient descent step size used if FIXEDGD is selected as method of optimization"),
    'stepSizeu0':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for initial force. It is used if FIXEDGD is selected as method of optimization"),
    'stepSizep0':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for initial jerk. It is used if FIXEDGD is selected as method of optimization"),
    'stepSizepc':
    Config.Param(default=0.01,
                 comment="Gradient descent step size for jerks at controls. It is used if FIXEDGD is selected as method of optimization"),
    'method':
    Config.Param(default="FIXEDGD",
                 comment="Optimization scheme.  FIXEDGD or LBFGS"),
    'NIterForInverse':
    Config.Param(default=5,
                 comment="Iterations for computing fixed point iterative inverse of a diffeomorphism."),
    'integMethod':
    Config.Param(default="EULER",
                 comment="Integration scheme.  EULER or RK4"),
    'lbfgs_bounds':
    Config.Param(default=[[-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1], [-0.1, 0.1]],
                 required=True,
                 comment="List of four [min,max] pairs for bounds on m0, u0, p0 and p at controls, respectively. A [None, None] means no bounds and [0,0] means that this variable will not be optimized for. It is currently only used with LBFGS optimization."),
    'nTimeSteps':
    Config.Param(default=10,
                 comment="Number of time discretization steps"),
    'update_I0':
    Config.Param(default=False,
                 comment="Fixes initial image if set to False to the initiliazation in I0 otherwise estimates it"),
    'I0':
    Config.Param(default=None,
                 comment="Fixed initial image"),
    'update_m0':
    Config.Param(default=True,
                 comment="Fixes initial momenta if set to False to the initiliazation in m0, otherwise estimates it"),
    'm0':
    Config.Param(default=None,
                 comment="Fixed initial momenta"),
    'update_u0':
    Config.Param(default=True,
                 comment="Fixes initial control if set to False to the initiliazation in u0, otherwise estimates it"),
    'regularize_h1':
    Config.Param(default=False,
                 comment="Regularize the final displacement? It uses the function f(h)=(|v|^2-a^2)^3 if v>a and 0 if v<=a"),
    'a_h1':
    Config.Param(default=1.0,
                 comment="Parameter for the regularizer term for final displacement. Higher value allows bigger displacement while smaller values penalizes large displacements. It is used when regularize_h1 = True"),
    'r_h1':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for final displacement. It is used when regularize_h1 = True"),    
    'regularize_p0':
    Config.Param(default=False,
                 comment="Regularize the initial jerk?"),
    'r_p0':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for jerk. It is used when regularize_p0 = True"),
    'regularize_m0':
    Config.Param(default=False,
                 comment="Regularize the initial momenta?"),    
    'r_m0':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for momenta. It is used when regularize_m0 = True"),
    'regularize_u':
    Config.Param(default=False,
                 comment="Regularize the acceleration along the curve?"),
    'r_u':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer integral term for acceleration. It is used when regularize_u = True"),
    'u0':
    Config.Param(default=None,
                 comment="Fixed initial control"),
    'update_p0':
    Config.Param(default=True,
                 comment="Fixes initial jerk if set to False to the initiliazation in p0, otherwise estimates it"),
    'p0':
    Config.Param(default=None,
                 comment="Fixed initial jerk"),
    'pc':
    Config.Param(default=[], required=False,
    comment="List of paths to initialize jerks at control locations in the same order as controlLocations."),
    'update_ps':
    Config.Param(default=False,
                 comment="Whether to update jumps at controls."),
    'regularize_ps':
    Config.Param(default=False,
                 comment="Regularize the jerk at controls?"),
    'r_ps':
    Config.Param(default=1.0,
                 comment="Weight for the regularizer term for the jerks at all controls. It is used when regularize_Ps = True"),
    'fitGeodesic':
    Config.Param(default=False,
                 comment="Whether to fit only a geodesic!"),
    'scale':
    Config.Param(default=1.0,
                 comment="Scale the initial conditions by m0*scale, u0*scale^2 and p0*scale^3. It is equivalent to time reparametrization."),
    'scaleInits':
    Config.Param(default=[1.0,1.0,1.0],
                 comment="Scale the initial conditions by m0*scaleInits[0], u0*scaleInits[1] and p0*scaleInits[2] this amount for initialization."),
    '_resource': "Optim",
    '_validation_hook': _valspec}
