#!/usr/bin/python2
# pyca modules
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
import Libraries.CAvmCommon as cavmcommon
import logging
import numpy as np
import matplotlib.pyplot as plt
import os
import math


class SplineState:
    def __init__(self, grid, mem_type, t, is_control=False, has_data=False, J=None, Jid=None, def_data=None):
        self.grid = grid
        self.mem_type = mem_type
        self.t = t
        self.is_control = is_control
        self.J = None
        self.Jid = None
        self.has_data = False
        self.def_data = None

        # states at time t
        self.g = ca.ManagedField3D(grid, mem_type)
        self.m = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.m, 0.0)
        self.u = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.u, 0.0)
        self.P = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.P, 0.0)
        self.I = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.I, 0.0)

        # adjoint states at time t
        self.adj_h = ca.ManagedField3D(grid, mem_type) # only used for spline extension version
        ca.SetMem(self.adj_h, 0.0)
        self.adj_m = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_m, 0.0)
        self.adj_u = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_u, 0.0)
        self.adj_P = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.adj_P, 0.0)
        self.adj_I = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.adj_I, 0.0)

        # if we have measurement data here
        if J is not None:
            #self.J = ca.Image3D(grid, mem_type)
            #ca.SetMem(self.J, 0.0)
            self.J = J
            self.Jid = Jid
            self.has_data = True
            self.def_data = def_data
            # unique id (name) for writing output
            if Jid is not None: # this should always evaluate to true if Jt is not None
                 self.Jid = Jid
        else:
            self.J = None
            self.Jid = None
            self.has_data = False
            self.def_data = None

        # other state vars for convenience
        self.ginv = ca.ManagedField3D(grid, mem_type)

        self.v = ca.ManagedField3D(grid, mem_type)  # dual of m, v\in \mathfrak{g}
        ca.SetMem(self.v, 0.0)
        self.f = ca.ManagedField3D(grid, mem_type)  # dual of u, f\in \mathfrak{g}
        ca.SetMem(self.f, 0.0)
        self.m_part = ca.ManagedField3D(grid, mem_type)  # part of m that is integrated
        ca.SetMem(self.m_part, 0.0)
        self.f_part = ca.ManagedField3D(grid, mem_type)  # part of f that is integrated
        ca.SetMem(self.f_part, 0.0)

        self.adj_f = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_f, 0.0)
        self.adj_f_part = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_f_part, 0.0)
        self.adj_v = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_v, 0.0)
        self.adj_P_part = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.adj_P_part, 0.0)
        self.adj_I_part = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.adj_I_part, 0.0)


class SplineGradient:
    def __init__(self, grid, mem_type, num_controls=0):
        self.grid = grid
        self.mem_type = mem_type
        self.grad_m = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_m, 0.0)
        self.grad_u = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_u, 0.0)
        self.grad_P = ca.Image3D(grid, mem_type)
        ca.SetMem(self.grad_P, 0.0)
        self.grad_I = ca.Image3D(grid, mem_type)
        ca.SetMem(self.grad_I, 0.0)
        self.num_controls = num_controls
        self.grad_controls = [ca.Image3D(grid, mem_type) for i in range(num_controls)]
        for i in range(num_controls):
            ca.SetMem(self.grad_controls[i], 0.0)

        self.serialized_array = None

    def serialize_asnp(self):
        # allocate big host memory before hand
        if self.serialized_array is None:
            # nikhil: debug
            #length_serialized_object = 2*3*self.grid.nVox() + 2*self.grid.nVox() + self.num_controls*self.grid.nVox()
            length_serialized_object = 2*3*self.grid.nVox() + self.grid.nVox() + self.num_controls*self.grid.nVox()

            self.serialized_array = np.zeros(length_serialized_object, dtype='float')

        if self.mem_type == ca.MEM_HOST:
            counter = 0
            logging.debug('Serialization begin with start counter: %d/%d', counter, len(self.serialized_array))
            np_grad_m_x, np_grad_m_y, np_grad_m_z = self.grad_m.asnp()
            np_grad_u_x, np_grad_u_y, np_grad_u_z = self.grad_u.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_z)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_z)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(self.grad_P.asnp())
            counter += 1

            # nikhil: debug
            #self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(self.grad_I.asnp())
            #counter += 1

            for i in range(self.num_controls):
                self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = \
                    np.ravel(self.grad_controls[i].asnp())
                counter += 1
            logging.debug('Serialization done with end counter: %d/%d', counter*self.grid.nVox(), len(self.serialized_array))
        else:
            raise Exception('Serialization of the gradient object of type MEM_DEVICE not yet implemented!')

    def unserialize_fromnp(self):
        if self.mem_type == ca.MEM_HOST:
            raise Exception('UnSerialization of the gradient object of type MEM_HOST not yet implemented!')
        else:
            raise Exception('UnSerialization of the gradient object of type MEM_DEVICE not yet implemented!')


def unserialize_into_splinestate_array(out_tdisc_spline, serialized_array, grid, mem_type):

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(serialized_array))
        # get references to numpy arrays for pyca types
        np_m_x, np_m_y, np_m_z = out_tdisc_spline[0].m.asnp()
        np_u_x, np_u_y, np_u_z = out_tdisc_spline[0].u.asnp()
        np_P = out_tdisc_spline[0].P.asnp()
        np_I = out_tdisc_spline[0].I.asnp()
        np_m_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_P[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                             [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        # nikhil: debug
        #np_I[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
        #                     [grid.size().x, grid.size().y, grid.size().z])
        #counter += 1
        controls_counter = 0
        for i in range(len(out_tdisc_spline)):
            if out_tdisc_spline[i].is_control:
                np_Pt = out_tdisc_spline[i].P.asnp()
                np_Pt[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                      [grid.size().x, grid.size().y, grid.size().z])
                counter += 1
                controls_counter += 1

        logging.debug('UnSerialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(),
                      len(serialized_array), controls_counter)
    else:
        raise Exception('UnSerialization of the object of type MEM_DEVICE not yet implemented!')


def CheckNaN(l, iter=None):
    if type(l) is list:
        x = sum(map(ca.Sum, l))
    else:
        x = ca.Sum(l)
    if math.isinf(x):
        if iter is not None:
            print "Exception in iter: ", iter
        raise Exception("\aInf detected")
    if x != x:
        if iter is not None:
            print "Exception in iter: ", iter
        raise Exception("\aNaN detected")

def setup_time_discretization(time_descretized_array, image_data, image_ids, image_indices, control_indices, def_data=None):
    """
    Set up an list of states at time descritizations for the spline curve (list of SplineStates)
    """
    grid = image_data[0].grid()
    mem_type = image_data[0].memType()
    td = []
    for i in xrange(len(time_descretized_array)):
        if (i in image_indices) and (i in control_indices):          
            if def_data is not None:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], True, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)], def_data[image_indices.index(i)]))
            else :
                td.append(SplineState(grid, mem_type, time_descretized_array[i], True, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)]))
            
        elif i in image_indices:
            if def_data is not None:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], False, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)], def_data[image_indices.index(i)]))
            else:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], False, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)]))

        elif i in control_indices:
            td.append(SplineState(grid, mem_type, time_descretized_array[i], True, False, None, None, None))
        else:
            td.append(SplineState(grid, mem_type, time_descretized_array[i], False, False, None, None, None))
    return td


def setup_timearray(num_timesteps, measurement_times_array, control_times_array, epsilon):
    num_measurements = len(measurement_times_array)
    num_controls = len(control_times_array)
    temp_t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    measurement_indices = [0]*num_measurements
    control_indices = [0]*num_controls

    # first create the time array and add timepoints for measurements
    for i in range(num_measurements):
        found_sub = False
        for j in range(len(temp_t)):
            if (measurement_times_array[i] < (temp_t[j]+epsilon)) and (measurement_times_array[i] > (temp_t[j]-epsilon)):
                found_sub = True
                break

        if not found_sub:  # need to insert a timepoint for this subject
            t.append(measurement_times_array[i])
            t.sort()

    temp_t = t
    # next add timepoints for controls
    for i in range(num_controls):
        found_control = False
        for j in range(len(temp_t)):
            if (control_times_array[i] < (temp_t[j]+epsilon)) and (control_times_array[i] > (temp_t[j]-epsilon)):
                found_control = True
                break

        if not found_control:  # need to insert a timepoint for this control
            t.append(control_times_array[i])
            t.sort()

    # now create the subject index array
    for i in range(num_measurements):
        for j in range(len(t)):
            if (measurement_times_array[i] < (t[j]+epsilon)) and (measurement_times_array[i] > (t[j]-epsilon)):
                measurement_indices[i] = j
                break

    # now create the control index array
    for i in range(num_controls):
        for j in range(len(t)):
            if (control_times_array[i] < (t[j]+epsilon)) and (control_times_array[i] > (t[j]-epsilon)):
                control_indices[i] = j
                break

    return t, measurement_indices, control_indices


def setup_diff_oper(alpha, beta, gamma, grid, mem_type):
    # differential operator (size same as a Field3D)
    diff_oper = None
    if mem_type == ca.MEM_HOST:
        diff_oper = ca.FluidKernelFFTCPU()
    else:
        diff_oper = ca.FluidKernelFFTGPU()
    diff_oper.setAlpha(alpha)
    diff_oper.setBeta(beta)
    diff_oper.setGamma(gamma)
    diff_oper.setGrid(grid)

    return diff_oper


def eval_rhs_g(out_v, v, g):
    """
    Evaluate RHS for forward integration of \dot{g}=v\circ g
    """
    ca.ApplyH(out_v, v, g, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)

    return out_v


def eval_rhs_m_part(out_u, g, u):
    ca.CoAd(out_u, g, u)

    # nikhil: debug
    # ca.SetMem(out_u, 0.0)

    return out_u


def eval_rhs_m(out_m, scratch_v, v, m, u):
    ca.Copy(out_m, u)
    ca.CoAdInf(scratch_v, v, m)
    ca.Sub_I(out_m, scratch_v)
    return out_m


def eval_rhs_f_part(out_f, scratch_v, P, I, f, m, ginv, diff_op):
    ca.Gradient(scratch_v, I)
    ca.MulMulC_I(scratch_v, P, 1.0)
    ca.CoAdInf(out_f, f, m)
    ca.Sub_I(scratch_v, out_f)
    diff_op.applyInverseOperator(scratch_v)
    ca.Ad(out_f, ginv, scratch_v)
    return out_f


def eval_rhs_f(out_f, scratch_v, P, I, f, m, v, diff_op, iter=None, cf=None):
    ca.Gradient(scratch_v, I)
    ca.MulMulC_I(scratch_v, P, 1.0)
    if iter is not None:
        common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/debug/')
        # write it out
        fig = plt.figure('1', frameon=False)
        plt.clf()
        plot_vf_2d(scratch_v, '')
        outfilename = cf.io.outputPrefix+'/frames/debug/pgradI'+str(iter).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    ca.CoAdInf(out_f, f, m)
    if iter is not None:
        # write it out
        fig = plt.figure('2', frameon=False)
        plt.clf()
        plot_vf_2d(out_f, '')
        outfilename = cf.io.outputPrefix+'/frames/debug/coadinffm'+str(iter).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    ca.Sub_I(scratch_v, out_f)
    diff_op.applyInverseOperator(scratch_v)
    if iter is not None:
        # write it out
        fig = plt.figure('3', frameon=False)
        plt.clf()
        plot_vf_2d(scratch_v, '')
        outfilename = cf.io.outputPrefix+'/frames/debug/term12'+str(iter).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)
    ca.AdInf(out_f, v, f)
    if iter is not None:
        # write it out
        fig = plt.figure('3', frameon=False)
        plt.clf()
        plot_vf_2d(out_f, '')
        outfilename = cf.io.outputPrefix+'/frames/debug/advf'+str(iter).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    ca.Add_I(out_f, scratch_v)
    if iter is not None:
        # write it out
        fig = plt.figure('4', frameon=False)
        plt.clf()
        plot_vf_2d(out_f, '')
        outfilename = cf.io.outputPrefix+'/frames/debug/rhs_f'+str(iter).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    return out_f


def eval_rhs_adj_m(out_v, scratch_v, adj_m, v, m, f, adj_f, adj_u, adj_I, I, adj_P, P, diff_op):
    # nikhil: debug
    # ca.SetMem(out_v, 0.0)
    ca.CoAdInf(out_v, f, adj_f)
    ca.Gradient(scratch_v, I)
    ca.MulMulC_I(scratch_v, adj_I, 1.0)
    ca.Add_I(out_v, scratch_v)
    ca.Gradient(scratch_v, adj_P)
    ca.MulMulC_I(scratch_v, P, 1.0)
    ca.Sub_I(out_v, scratch_v)
    
    ca.CoAdInf(scratch_v, adj_m, m)
    ca.Sub_I(out_v, scratch_v)
    diff_op.applyInverseOperator(out_v)
    ca.AdInf(scratch_v, adj_m, v)
    ca.Sub_I(out_v, scratch_v)
    ca.AdInf(scratch_v, f, adj_u)
    ca.Add_I(out_v, scratch_v)

    return out_v


def eval_rhs_adj_f(out_m, scratch_v, adj_v, adj_u, m, v, adj_f):
    ca.CoAdInf(out_m, adj_u, m)
    ca.Add_I(out_m, adj_v)
    ca.CoAdInf(scratch_v, v, adj_f)
    ca.Add_I(out_m, scratch_v)
    ca.MulC_I(out_m, -1.0)
    return out_m


def eval_rhs_adj_f_part(out_m, scratch_v, adj_v, adj_u, m, g):
    ca.CoAdInf(scratch_v, adj_u, m)
    ca.Add_I(scratch_v, adj_v)
    ca.CoAd(out_m, g, scratch_v)
    return out_m


def eval_rhs_adj_P(out_I, scratch_I, scratch_v, I, adj_u, adj_P, v):
    ca.Gradient(scratch_v, I)
    ca.ComponentDotProd(out_I, scratch_v, adj_u)
    ca.Gradient(scratch_v, adj_P)
    ca.ComponentDotProd(scratch_I, scratch_v, v)
    ca.Add_I(out_I, scratch_I)
    return out_I


def eval_rhs_adj_P_part(out_I, scratch_I, scratch_v, I, adj_u, g):
    ca.Gradient(scratch_v, I)
    ca.ComponentDotProd(scratch_I, scratch_v, adj_u)
    ca.ApplyH(out_I, scratch_I, g)
    return out_I


def eval_rhs_adj_I(out_I, scratch_I, scratch_v, P, adj_u, adj_I, v):
    ca.Copy(scratch_v, adj_u)
    ca.MulMulC_I(scratch_v, P, 1.0)
    ca.Divergence(out_I, scratch_v)
    ca.Copy(scratch_v, v)
    ca.MulMulC_I(scratch_v, adj_I, 1.0)
    ca.Divergence(scratch_I, scratch_v)
    ca.Sub_I(out_I, scratch_I)

    return out_I


def eval_rhs_adj_I_part(out_I, scratch_I, scratch_v, P, adj_u, ginv):
    ca.Copy(scratch_v, adj_u)
    ca.MulMulC_I(scratch_v, P, 1.0)
    ca.Divergence(scratch_I, scratch_v)
    cavmcommon.SplatSafe(out_I, ginv, scratch_I)
    # nikhil: debug
    # ca.SetMem(out_I, 0.0)
    return out_I


def shoot_forward(cf, tdisc_spline, diff_op,  start_index=0, end_index=0):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)

    # initial conditions
    if end_index == 0:
        end_index = len(tdisc_spline)-1

    if start_index == 0:
        # initializations
        ca.SetToIdentity(tdisc_spline[0].g)
        ca.SetToIdentity(tdisc_spline[0].ginv)
        ca.Copy(tdisc_spline[0].v, tdisc_spline[0].m)
        diff_op.applyInverseOperator(tdisc_spline[0].v)
        ca.Copy(tdisc_spline[0].f, tdisc_spline[0].u)
        diff_op.applyInverseOperator(tdisc_spline[0].f)
        ca.SetMem(tdisc_spline[0].m_part, 0.0)
        ca.SetMem(tdisc_spline[0].f_part, 0.0)
        index_last_control = 0
    #else:
        # different initializations
    #    ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
    #    diff_op.ApplyInverseOperator(tdisc_spline[i].v)
    #    ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
    #    diff_op.ApplyOperator(tdisc_spline[i].u)

    # do integration

    for i in range(start_index+1, end_index+1, 1):
        dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step
        if cf.optim.integMethod == "EULER":
            logging.debug('Fwd Euler step: %d', i)

            # input previous v, g, ginv
            eval_rhs_g(scratch_v1, tdisc_spline[i-1].v, tdisc_spline[i-1].g)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].g, tdisc_spline[i-1].g, scratch_v1)  # take fwd step
            ca.UpdateInverse(tdisc_spline[i].ginv, scratch_v2, tdisc_spline[i-1].ginv, scratch_v1,
                             cf.optim.NIterForInverse)  # update ginv corresponding to above fwd step
            # updated this g, ginv

            # input previous g, u, m_part, m[0], this ginv
            eval_rhs_m_part(scratch_v1, tdisc_spline[i-1].g, tdisc_spline[i-1].u)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].m_part, tdisc_spline[i-1].m_part, scratch_v1)  # take fwd step
            ca.Add(scratch_v1, tdisc_spline[0].m, tdisc_spline[i].m_part)
            ca.CoAd(tdisc_spline[i].m, tdisc_spline[i].ginv, scratch_v1)  # update m
            # updated this m_part, m

            # input previous P, I, f, m, ginv, f_part, f[0], this g
            eval_rhs_f_part(scratch_v1, scratch_v2, tdisc_spline[i-1].P, tdisc_spline[i-1].I, tdisc_spline[i-1].f,
                            tdisc_spline[i-1].m, tdisc_spline[i-1].ginv, diff_op)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].f_part, tdisc_spline[i-1].f_part, scratch_v1)  # take fwd step
            ca.Add(scratch_v1, tdisc_spline[0].f, tdisc_spline[i].f_part)
            ca.Ad(tdisc_spline[i].f, tdisc_spline[i].g, scratch_v1)  # update f
            # updated this f_part, f

            # input previous I[0], this ginv
            ca.ApplyH(tdisc_spline[i].I, tdisc_spline[0].I, tdisc_spline[i].ginv)  # update I
            # updated this I

            if not tdisc_spline[i].is_control:
                # input previous P[0], this g
                # get g_{last_control, this}
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
                cavmcommon.SplatSafe(tdisc_spline[i].P, scratch_v1, tdisc_spline[index_last_control].P)  # update P
                # updated this P
            else:
                index_last_control = i  # for subsequent iterations

        elif cf.optim.integMethod == "RK4":
            raise Exception('RK4 not yet implemented for shoot forward of splines.')
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, v and u, required for the next integration step
        ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
        diff_op.applyInverseOperator(tdisc_spline[i].v)
        ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
        diff_op.applyOperator(tdisc_spline[i].u)


def integrate_backward(out_gradient, cf, tdisc_spline, diff_op):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_P, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_P_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I_part, 0.0)

    if tdisc_spline[-1].has_data:
        ca.Sub(scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J)
        ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
        ca.MulC_I(scratch_I1, -1.0)  # negative gradient
        cavmcommon.SplatSafe(tdisc_spline[-1].adj_I_part, tdisc_spline[-1].ginv, scratch_I1)
        ca.Copy(tdisc_spline[-1].adj_I, scratch_I1)

    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1

    controls_counter = out_gradient.num_controls-1
    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        if cf.optim.integMethod == "EULER":
            logging.debug('Bwd Euler step: %d', i)
            # input previous adj_m, v, m, f, adj_f, adj_u, adj_I, I
            eval_rhs_adj_m(scratch_v1, scratch_v2,
                           tdisc_spline[i+1].adj_m,
                           tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                           tdisc_spline[i+1].f,
                           tdisc_spline[i+1].adj_f,
                           tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_I,
                           tdisc_spline[i+1].I,
                           tdisc_spline[i+1].adj_P,
                           tdisc_spline[i+1].P, diff_op)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, scratch_v1)  # take fwd step
            #ca.Add_MulC(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, scratch_v1, dt)  # take fwd step
            # updated this adj_m

            # input previous adj_v, adj_u, m, adj_f[end], g[end], this ginv
            eval_rhs_adj_f_part(scratch_v1, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].m, tdisc_spline[i+1].g)
            ca.MulC_I(scratch_v1, dt)
            ca.Sub(tdisc_spline[i].adj_f_part, tdisc_spline[i+1].adj_f_part, scratch_v1)  # take fwd step
            #ca.CoAd(scratch_v1, tdisc_spline[-1].g, tdisc_spline[-1].adj_f)
            #ca.Sub_I(scratch_v1, tdisc_spline[i].adj_f_part)
            #ca.CoAd(tdisc_spline[i].adj_f, tdisc_spline[i].ginv, scratch_v1)  # update adj_f
            ca.CoAd(tdisc_spline[i].adj_f, tdisc_spline[i].ginv, tdisc_spline[i].adj_f_part)  # update adj_f
            # updated this adj_f_part, adj_f

            # input previous I, adj_u, g, adj_P[end], g[end], this ginv
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i+1].g)
            eval_rhs_adj_P_part(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                                scratch_v1)
            #eval_rhs_adj_P_part(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                                #tdisc_spline[i+1].g)
            ca.MulC_I(scratch_I1, dt)
            ca.Sub(tdisc_spline[i].adj_P_part, tdisc_spline[i+1].adj_P_part, scratch_I1)  # take fwd step
            #ca.ApplyH(scratch_I1, tdisc_spline[-1].adj_P, tdisc_spline[-1].g)
            #ca.Sub_I(scratch_I1, tdisc_spline[i].adj_P_part)
            #ca.ApplyH(tdisc_spline[i].adj_P, scratch_I1, tdisc_spline[i].ginv)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[all_control_indices[controls_counter]].g, tdisc_spline[i].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].ginv)
            ca.ApplyH(tdisc_spline[i].adj_P, tdisc_spline[i].adj_P_part, scratch_v1)  # update adj_P
            #ca.ApplyH(tdisc_spline[i].adj_P, tdisc_spline[i].adj_P_part, tdisc_spline[i].ginv)  # update adj_P
            # updated this adj_P_part, adj_P

            # input previous P, adj_u, ginv, adj_I[end], ginv[end], this g
            eval_rhs_adj_I_part(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].ginv)
            ca.MulC_I(scratch_I1, dt)
            ca.Add(tdisc_spline[i].adj_I_part, tdisc_spline[i+1].adj_I_part, scratch_I1)  # take fwd step
            #cavmcommon.SplatSafe(scratch_I1, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_I)
            #ca.Add_I(scratch_I1, tdisc_spline[i].adj_I_part)
            #cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, scratch_I1)
            cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)  # update adj_I
            # updated this adj_I_part, adj_I

            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
                cavmcommon.SplatSafe(scratch_I2, tdisc_spline[i].ginv, scratch_I1)
                ca.Sub_I(tdisc_spline[i].adj_I_part, scratch_I2)
                #cavmcommon.SplatSafe(scratch_I1, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_I)
                #ca.Add_I(scratch_I1, tdisc_spline[i].adj_I_part)
                #cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, scratch_I1)
                cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)

            if tdisc_spline[i].is_control:
                if cf.optim.regularize_Ps:        
                    ca.Gradient(scratch_v1, tdisc_spline[i].I)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, 1.0)                
                    diff_op.applyInverseOperator(scratch_v1)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, -cf.optim.r_Ps)                
                    ca.Divergence(out_gradient.grad_controls[controls_counter], scratch_v1)
                    ca.Sub_I(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P)  # copy Ps
                else:    
                    ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P, -1.0)  # copy -ve into out_gradient
                ca.SetMem(tdisc_spline[i].adj_P, 0.0)  # reset adj_P
                ca.SetMem(tdisc_spline[i].adj_P_part, 0.0)  # reset adj_P_part
                controls_counter -= 1

        elif cf.optim.integMethod == "RK4":
            raise Exception('RK4 not yet implemented for backward integration of adjoint system of splines.')
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0
    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    if cf.optim.regularize_u0:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].f, cf.optim.r_u0)  # copy m0
        ca.Sub_I(out_gradient.grad_u, tdisc_spline[0].adj_u)  # copy m0
    else:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient
    if cf.optim.regularize_P0:        
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)                
        diff_op.applyInverseOperator(scratch_v1)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, -cf.optim.r_P0)                
        ca.Divergence(out_gradient.grad_P, scratch_v1)
        ca.Sub_I(out_gradient.grad_P, tdisc_spline[0].adj_P)  # copy m0
    else:    
        ca.MulC(out_gradient.grad_P, tdisc_spline[0].adj_P, -1.0)  # copy -ve into out_gradient
    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    # nikhil: debug
    #ca.SetMem(out_gradient.grad_u, 0.0)
    #ca.SetMem(out_gradient.grad_P, 0.0)

    return out_gradient

def integrate_backward_direct(out_gradient, cf, tdisc_spline, diff_op):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_P, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)

    if tdisc_spline[-1].has_data:
        ca.Sub(scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J)
        ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
        ca.MulC_I(scratch_I1, -1.0)  # negative gradient
        ca.Copy(tdisc_spline[-1].adj_I, scratch_I1)

    ctrl_index = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        if cf.optim.integMethod == "EULER":
            logging.debug('Bwd Euler step: %d', i)
            # input previous adj_m, v, m, f, adj_f, adj_u, adj_I, I
            eval_rhs_adj_m(scratch_v1, scratch_v2,
                           tdisc_spline[i+1].adj_m,
                           tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                           tdisc_spline[i+1].f,
                           tdisc_spline[i+1].adj_f,
                           tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_I,
                           tdisc_spline[i+1].I,
                           tdisc_spline[i+1].adj_P,
                           tdisc_spline[i+1].P, diff_op)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, scratch_v1)  # take fwd step
            #ca.Add_MulC(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, scratch_v1, dt)  # take fwd step
            # updated this adj_m

            # input previous adj_v, adj_u, m, v, adj_f
            eval_rhs_adj_f(scratch_v1, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].m, tdisc_spline[i+1].v, tdisc_spline[i+1].adj_f)
            ca.MulC_I(scratch_v1, dt)
            ca.Add(tdisc_spline[i].adj_f, tdisc_spline[i+1].adj_f, scratch_v1)  # take fwd step
            # updated this adj_f

            # input previous I, adj_u, adj_P, v
            eval_rhs_adj_P(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_P, tdisc_spline[i+1].v)
            ca.MulC_I(scratch_I1, dt)
            ca.Sub(tdisc_spline[i].adj_P, tdisc_spline[i+1].adj_P, scratch_I1)  # take fwd step
            # updated this adj_P

            # input previous P, adj_u, ginv, adj_I[end], ginv[end], this g
            eval_rhs_adj_I_part(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].ginv)
            eval_rhs_adj_I(scratch_I1, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_I, tdisc_spline[i+1].v)
            ca.MulC_I(scratch_I1, dt)
            ca.Add(tdisc_spline[i].adj_I, tdisc_spline[i+1].adj_I, scratch_I1)  # take fwd step
            # updated this adj_I

            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
                ca.Sub_I(tdisc_spline[i].adj_I, scratch_I1)

            if tdisc_spline[i].is_control:
                if cf.optim.regularize_Ps:        
                    ca.Gradient(scratch_v1, tdisc_spline[i].I)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, 1.0)                
                    diff_op.applyInverseOperator(scratch_v1)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, -cf.optim.r_Ps)                
                    ca.Divergence(out_gradient.grad_controls[ctrl_index], scratch_v1)
                    ca.Sub_I(out_gradient.grad_controls[ctrl_index], tdisc_spline[i].adj_P)  # copy Ps
                else:    
                    ca.MulC(out_gradient.grad_controls[ctrl_index], tdisc_spline[i].adj_P, -1.0)  # copy -ve into out_gradient

                ca.SetMem(tdisc_spline[i].adj_P, 0.0)  # reset adj_P
                ctrl_index -= 1

        elif cf.optim.integMethod == "RK4":
            raise Exception('RK4 not yet implemented for backward integration of adjoint system of splines.')
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0
    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    if cf.optim.regularize_u0:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].f, cf.optim.r_u0)  # copy m0
        ca.Sub_I(out_gradient.grad_u, tdisc_spline[0].adj_u)  # copy m0
    else:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient
    if cf.optim.regularize_P0:        
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)                
        diff_op.applyInverseOperator(scratch_v1)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, -cf.optim.r_P0)                
        ca.Divergence(out_gradient.grad_P, scratch_v1)
        ca.Sub_I(out_gradient.grad_P, tdisc_spline[0].adj_P)  # copy m0
    else:    
        ca.MulC(out_gradient.grad_P, tdisc_spline[0].adj_P, -1.0)  # copy -ve into out_gradient
    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    # nikhil: debug
    #ca.SetMem(out_gradient.grad_u, 0.0)
    #ca.SetMem(out_gradient.grad_P, 0.0)

    return out_gradient


def compute_gradient(out_gradient, cf, tdisc_spline, diff_op):
    if cf.optim.integMethod == "EULER":
        # shoot forward
        shoot_forward(cf, tdisc_spline, diff_op)
        # integrate backward
        integrate_backward(out_gradient, cf, tdisc_spline, diff_op)
    elif cf.optim.integMethod == "RK4":
        # shoot forward
        shoot_forward_rk4_direct(cf, tdisc_spline, diff_op)
        #CheckNaN(tdisc_spline[-1].g)
        # integrate backward
        if cf.study.regressImages and not cf.study.runSplineExtension:
            integrate_backward_rk4_direct(out_gradient, cf, tdisc_spline, diff_op)
        elif cf.study.runSplineExtension:
            if cf.optim.fitGeodesic:
                integrate_backward_rk4_direct_ext_geomatch(out_gradient, cf, tdisc_spline, diff_op)
            else:                
                integrate_backward_rk4_direct_ext(out_gradient, cf, tdisc_spline, diff_op)
        else:
            raise Exception('No regression requested!')

    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)

    return out_gradient


def compute_energy(cf, tdisc_spline, diff_op):
    total_energy = 0.0
    regularizer_P0 = 0.0
    regularizer_m0 = 0.0
    regularizer_u0 = 0.0
    image_match = 0.0
    def_match = 0.0
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    if cf.optim.integMethod == "EULER":
        shoot_forward(cf, tdisc_spline, diff_op)
    elif cf.optim.integMethod == "RK4":
        shoot_forward_rk4_direct(cf, tdisc_spline, diff_op)
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)

    # scratch variables
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    
    if cf.study.regressImages:   
        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                image_match += 0.5*ca.Sum2(scratch_I1)/(cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)

    if cf.study.regressDefFields:        
        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].has_data:
                ca.Sub(scratch_v1, tdisc_spline[i].ginv, tdisc_spline[i].def_data)
                def_match += 0.5*ca.Sum2(scratch_v1)/(cf.splinesconfig.stdNoiseDef*cf.splinesconfig.stdNoiseDef)

    # regularizers
    if cf.optim.regularize_P0:
        scratch_v2 = ca.ManagedField3D(grid, mem_type)
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)        
        ca.Copy(scratch_v2, scratch_v1)
        diff_op.applyInverseOperator(scratch_v1)
        regularizer_P0 = 0.5*cf.optim.r_P0*ca.Dot(scratch_v2, scratch_v1)

    if cf.optim.regularize_m0:
        regularizer_m0 = 0.5*cf.optim.r_m0*ca.Dot(tdisc_spline[0].m, tdisc_spline[0].v)

    if cf.optim.regularize_u0:
        regularizer_u0 = 0.5*cf.optim.r_u0*ca.Dot(tdisc_spline[0].u, tdisc_spline[0].f)
    total_energy = regularizer_P0+regularizer_m0+regularizer_u0+image_match+def_match
    return total_energy, regularizer_P0, regularizer_m0, regularizer_u0, image_match, def_match


class SplineGradientDebug:
    def __init__(self, grid, mem_type, num_controls=0):
        self.grid = grid
        self.mem_type = mem_type
        self.grad_m = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_m, 0.0)
        self.grad_u = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_u, 0.0)
        self.grad_P = ca.Image3D(grid, mem_type)
        ca.SetMem(self.grad_P, 0.0)
        self.grad_I = ca.Image3D(grid, mem_type)
        ca.SetMem(self.grad_I, 0.0)
        self.num_controls = num_controls
        self.grad_controls = [ca.Image3D(grid, mem_type) for i in range(num_controls)]
        for i in range(num_controls):
            ca.SetMem(self.grad_controls[i], 0.0)

        self.serialized_array = None

        # buffers are used for GPUs
        self.buffer_I = None
        self.buffer_v = None

    def serialize_asnp(self):
        # allocate big host memory beforehand
        if self.serialized_array is None:
            length_serialized_object = 2*3*self.grid.nVox() + 1*self.grid.nVox() + self.num_controls*self.grid.nVox()

            self.serialized_array = np.zeros(length_serialized_object, dtype='float')

        if self.mem_type == ca.MEM_HOST:
            counter = 0
            logging.debug('Serialization begin with start counter: %d/%d', counter, len(self.serialized_array))
            np_grad_m_x, np_grad_m_y, np_grad_m_z = self.grad_m.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_z)
            counter += 1

            np_grad_u_x, np_grad_u_y, np_grad_u_z = self.grad_u.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_z)
            counter += 1

            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(self.grad_P.asnp())
            counter += 1

            for i in range(self.num_controls):
                self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = \
                    np.ravel(self.grad_controls[i].asnp())
                counter += 1

            logging.debug('Serialization done with end counter: %d/%d', counter*self.grid.nVox(), len(self.serialized_array))
        else:
            if self.buffer_I is None:
                self.buffer_I = ca.Image3D(self.grid, ca.MEM_HOST)
                logging.info('Buffer initialized during serialization!')
            if self.buffer_v is None:
                self.buffer_v = ca.Field3D(self.grid, ca.MEM_HOST)

            counter = 0
            logging.debug('Serialization begin with start counter: %d/%d', counter, len(self.serialized_array))
            ca.Copy(self.buffer_v, self.grad_m)
            np_grad_m_x, np_grad_m_y, np_grad_m_z = self.buffer_v.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_m_z)
            counter += 1

            ca.Copy(self.buffer_v, self.grad_u)
            np_grad_u_x, np_grad_u_y, np_grad_u_z = self.buffer_v.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_x)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_y)
            counter += 1
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_u_z)
            counter += 1

            ca.Copy(self.buffer_I, self.grad_P)
            np_grad_P = self.buffer_I.asnp()
            self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = np.ravel(np_grad_P)
            counter += 1

            for i in range(self.num_controls):
                ca.Copy(self.buffer_I, self.grad_controls[i])
                self.serialized_array[counter*self.grid.nVox():(counter+1)*self.grid.nVox()] = \
                    np.ravel(self.buffer_I.asnp())
                counter += 1


            logging.debug('Serialization done with end counter: %d/%d', counter*self.grid.nVox(), len(self.serialized_array))

    def unserialize_fromnp(self):
        if self.mem_type == ca.MEM_HOST:
            raise Exception('UnSerialization of the gradient object of type MEM_HOST not yet implemented!')
        else:
            raise Exception('UnSerialization of the gradient object of type MEM_DEVICE not yet implemented!')

def unserialize_into_splinestate_array_debug(out_tdisc_spline, serialized_array, grid, mem_type, buffer_I=None, buffer_v=None):

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(serialized_array))
        # get references to numpy arrays for pyca types
        np_m_x, np_m_y, np_m_z = out_tdisc_spline[0].m.asnp()
        np_m_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1

        np_u_x, np_u_y, np_u_z = out_tdisc_spline[0].u.asnp()
        np_u_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1

        np_P = out_tdisc_spline[0].P.asnp()
        np_P[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                            [grid.size().x, grid.size().y, grid.size().z])
        counter += 1

        controls_counter = 0
        for i in range(len(out_tdisc_spline)):
            if out_tdisc_spline[i].is_control:
                np_Pt = out_tdisc_spline[i].P.asnp()
                np_Pt[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                      [grid.size().x, grid.size().y, grid.size().z])
                counter += 1
                controls_counter += 1

        logging.debug('UnSerialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(),
                      len(serialized_array), controls_counter)
    else:
        if buffer_I is None:
            buffer_I = ca.Image3D(grid, ca.MEM_HOST)
            logging.info('Buffer initialized during unserialization!')
        if buffer_v is None:
            buffer_v = ca.Field3D(grid, ca.MEM_HOST)

        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(serialized_array))
        # get references to numpy arrays for pyca types

        np_m_x, np_m_y, np_m_z = buffer_v.asnp()
        np_m_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_m_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        ca.Copy(out_tdisc_spline[0].m, buffer_v)

        np_u_x, np_u_y, np_u_z = buffer_v.asnp()
        np_u_x[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_y[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        np_u_z[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        ca.Copy(out_tdisc_spline[0].u, buffer_v)

        np_P = buffer_I.asnp()
        np_P[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                            [grid.size().x, grid.size().y, grid.size().z])
        counter += 1
        ca.Copy(out_tdisc_spline[0].P, buffer_I)

        controls_counter = 0
        for i in range(len(out_tdisc_spline)):
            if out_tdisc_spline[i].is_control:
                np_Pt = buffer_I.asnp()
                #np_Pt = out_tdisc_spline[i].P.asnp()
                np_Pt[:] = np.reshape(serialized_array[counter*grid.nVox():(counter+1)*grid.nVox()],
                                      [grid.size().x, grid.size().y, grid.size().z])
                counter += 1
                ca.Copy(out_tdisc_spline[i].P, buffer_I)
                controls_counter += 1

        logging.debug('UnSerialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(),
                      len(serialized_array), controls_counter)

def shoot_forward_rk4_direct(cf, tdisc_spline, diff_op, rk4_scratch=None, start_index=0, end_index=0):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    rk4_g = ca.ManagedField3D(grid, mem_type)
    rk4_m = ca.ManagedField3D(grid, mem_type)
    rk4_f = ca.ManagedField3D(grid, mem_type)
    k_g = ca.ManagedField3D(grid, mem_type)
    k_m = ca.ManagedField3D(grid, mem_type)
    k_f = ca.ManagedField3D(grid, mem_type)

    # initial conditions
    if end_index == 0:
        end_index = len(tdisc_spline)-1

    if start_index == 0:
        # initializations
        ca.SetToIdentity(tdisc_spline[0].g)
        ca.SetToIdentity(tdisc_spline[0].ginv)
        ca.Copy(tdisc_spline[0].v, tdisc_spline[0].m)
        diff_op.applyInverseOperator(tdisc_spline[0].v)
        ca.Copy(tdisc_spline[0].f, tdisc_spline[0].u)
        diff_op.applyInverseOperator(tdisc_spline[0].f)
        ca.SetMem(tdisc_spline[0].m_part, 0.0)
        ca.SetMem(tdisc_spline[0].f_part, 0.0)
        index_last_control = 0
    #else:
        # different initializations
    #    ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
    #    diff_op.ApplyInverseOperator(tdisc_spline[i].v)
    #    ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
    #    diff_op.ApplyOperator(tdisc_spline[i].u)

    # do integration

    for i in range(start_index+1, end_index+1, 1):
        dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            eval_rhs_g(k_g, tdisc_spline[i-1].v, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, dt)  # K1_g*dt computed
            ca.Copy(rk4_g, k_g)
            eval_rhs_m(k_m, scratch_v1, tdisc_spline[i-1].v, tdisc_spline[i-1].m, tdisc_spline[i-1].u)
            #eval_rhs_m_part(k_m_part, tdisc_spline[i-1].g, tdisc_spline[i-1].u)
            ca.MulC_I(k_m, dt)  # K1_m_part*dt computed
            ca.Copy(rk4_m, k_m)

            eval_rhs_f(k_f, scratch_v1, tdisc_spline[i-1].P, tdisc_spline[i-1].I, tdisc_spline[i-1].f,
                       tdisc_spline[i-1].m, tdisc_spline[i-1].v, diff_op)
            #eval_rhs_f_part(k_f_part, scratch_v1, tdisc_spline[i-1].P, tdisc_spline[i-1].I, tdisc_spline[i-1].f,
            #                tdisc_spline[i-1].m, tdisc_spline[i-1].ginv, diff_op)
            ca.MulC_I(k_f, dt)  # K1_f_part*dt computed
            ca.Copy(rk4_f, k_f)

            # for K2
            # get g and ginv at t+1/2#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, 0.5)  # K1_g*dt/2
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1/2#
            ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
            ca.MulC_I(k_m, 0.5)  # K1_m_part*dt/2
            ca.Add_I(rk4_scratch.m, k_m)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1/2#
            ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
            ca.MulC_I(k_f, 0.5)  # K1_f_part*dt/2
            ca.Add_I(rk4_scratch.f, k_f)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1/2#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K2's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k2_g*dt computed
            ca.Add_MulC_I(rk4_g, k_g, 2.0)
            eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
            #eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
            ca.MulC_I(k_m, dt)  # K2_m_part*dt computed
            ca.Add_MulC_I(rk4_m, k_m, 2.0)
            eval_rhs_f(k_f, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                       rk4_scratch.m, rk4_scratch.v, diff_op)
            #eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
            #                rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f, dt)  # K2_f_part*dt computed
            ca.Add_MulC_I(rk4_f, k_f, 2.0)

            # for K3
            # get g and ginv at t+1/2#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, 0.5)  # K2_g*dt/2
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1/2#
            ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
            ca.MulC_I(k_m, 0.5)  # K2_m_part*dt/2
            ca.Add_I(rk4_scratch.m, k_m)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1/2#
            ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
            ca.MulC_I(k_f, 0.5)  # K2_f_part*dt/2
            ca.Add_I(rk4_scratch.f, k_f)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1/2#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K3's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k3_g*dt computed
            ca.Add_MulC_I(rk4_g, k_g, 2.0)
            eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
            ca.MulC_I(k_m, dt)  # K3_m_part*dt computed
            ca.Add_MulC_I(rk4_m, k_m, 2.0)
            eval_rhs_f(k_f, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                       rk4_scratch.m, rk4_scratch.v, diff_op)
            #eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
            #                rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f, dt)  # K3_f_part*dt computed
            ca.Add_MulC_I(rk4_f, k_f, 2.0)

            # for K4
            # get g and ginv at t+1#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1#
            ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
            ca.Add_I(rk4_scratch.m, k_m)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1#
            ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
            ca.Add_I(rk4_scratch.f, k_f)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K4's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k4_g*dt computed
            ca.Add_I(rk4_g, k_g)
            eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
            ca.MulC_I(k_m, dt)  # K4_m_part*dt computed
            ca.Add_I(rk4_m, k_m)
            eval_rhs_f(k_f, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                       rk4_scratch.m, rk4_scratch.v, diff_op)
            #eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
            #                rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f, dt)  # K4_f_part*dt computed
            ca.Add_I(rk4_f, k_f)

            # for final update
            ca.MulC_I(rk4_g, 1.0/float(6.0))
            ca.MulC_I(rk4_m, 1.0/float(6.0))
            ca.MulC_I(rk4_f, 1.0/float(6.0))

            # now take the final update step
            ca.Add(tdisc_spline[i].g, tdisc_spline[i-1].g, rk4_g)  # updated g
            ca.UpdateInverse(tdisc_spline[i].ginv, scratch_v1, tdisc_spline[i-1].ginv, rk4_g, cf.optim.NIterForInverse)  # updated ginv
            ca.Add(tdisc_spline[i].m, tdisc_spline[i-1].m, rk4_m)  # final updated m
            ca.Add(tdisc_spline[i].f, tdisc_spline[i-1].f, rk4_f)  # final updated f
            ca.ApplyH(tdisc_spline[i].I, tdisc_spline[0].I, tdisc_spline[i].ginv)  # final updated I

            if not tdisc_spline[i].is_control:
                # input previous P[0], this g
                # get g_{last_control, this}
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
                cavmcommon.SplatSafe(tdisc_spline[i].P, scratch_v1, tdisc_spline[index_last_control].P)  # final update P
            else:
                index_last_control = i  # for subsequent iterations
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)

        # now update other duals, v and u, required for the next integration step
        ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
        diff_op.applyInverseOperator(tdisc_spline[i].v)
        ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
        diff_op.applyOperator(tdisc_spline[i].u)

        # nikhil: debug
        '''
        if i == 50:
            index_last_control = 50
            eval_rhs_f(k_f, scratch_v1, tdisc_spline[i].P, tdisc_spline[i].I, tdisc_spline[i].f,
                       tdisc_spline[i].m, tdisc_spline[i].v, diff_op, iter=i, cf=cf)
            ca.MulC_I(tdisc_spline[i].P, -1500.0)
            eval_rhs_f(k_f, scratch_v1, tdisc_spline[i].P, tdisc_spline[i].I, tdisc_spline[i].f,
                       tdisc_spline[i].m, tdisc_spline[i].v, diff_op, iter=i+1, cf=cf)
        '''

def shoot_forward_rk4(cf, tdisc_spline, diff_op, rk4_scratch=None, start_index=0, end_index=0):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    rk4_g = ca.ManagedField3D(grid, mem_type)
    rk4_m_part = ca.ManagedField3D(grid, mem_type)
    rk4_f_part = ca.ManagedField3D(grid, mem_type)
    k_g = ca.ManagedField3D(grid, mem_type)
    k_m_part = ca.ManagedField3D(grid, mem_type)
    k_f_part = ca.ManagedField3D(grid, mem_type)

    # initial conditions
    if end_index == 0:
        end_index = len(tdisc_spline)-1

    if start_index == 0:
        # initializations
        ca.SetToIdentity(tdisc_spline[0].g)
        ca.SetToIdentity(tdisc_spline[0].ginv)
        ca.Copy(tdisc_spline[0].v, tdisc_spline[0].m)
        diff_op.applyInverseOperator(tdisc_spline[0].v)
        ca.Copy(tdisc_spline[0].f, tdisc_spline[0].u)
        diff_op.applyInverseOperator(tdisc_spline[0].f)
        ca.SetMem(tdisc_spline[0].m_part, 0.0)
        ca.SetMem(tdisc_spline[0].f_part, 0.0)
        index_last_control = 0
    #else:
        # different initializations
    #    ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
    #    diff_op.ApplyInverseOperator(tdisc_spline[i].v)
    #    ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
    #    diff_op.ApplyOperator(tdisc_spline[i].u)

    # do integration

    for i in range(start_index+1, end_index+1, 1):
        dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            eval_rhs_g(k_g, tdisc_spline[i-1].v, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, dt)  # K1_g*dt computed
            ca.Copy(rk4_g, k_g)

            eval_rhs_m_part(k_m_part, tdisc_spline[i-1].g, tdisc_spline[i-1].u)
            ca.MulC_I(k_m_part, dt)  # K1_m_part*dt computed
            ca.Copy(rk4_m_part, k_m_part)

            eval_rhs_f_part(k_f_part, scratch_v1, tdisc_spline[i-1].P, tdisc_spline[i-1].I, tdisc_spline[i-1].f,
                            tdisc_spline[i-1].m, tdisc_spline[i-1].ginv, diff_op)
            ca.MulC_I(k_f_part, dt)  # K1_f_part*dt computed
            ca.Copy(rk4_f_part, k_f_part)

            # for K2
            # get g and ginv at t+1/2#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, 0.5)  # K1_g*dt/2
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1/2#
            ca.Copy(rk4_scratch.m_part, tdisc_spline[i-1].m_part)
            ca.MulC_I(k_m_part, 0.5)  # K1_m_part*dt/2
            ca.Add_I(rk4_scratch.m_part, k_m_part)
            ca.Add_I(rk4_scratch.m_part, tdisc_spline[0].m)
            ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, rk4_scratch.m_part)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1/2#
            ca.Copy(rk4_scratch.f_part, tdisc_spline[i-1].f_part)
            ca.MulC_I(k_f_part, 0.5)  # K1_f_part*dt/2
            ca.Add_I(rk4_scratch.f_part, k_f_part)
            ca.Add_I(rk4_scratch.f_part, tdisc_spline[0].f)
            ca.Ad(rk4_scratch.f, rk4_scratch.g, rk4_scratch.f_part)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1/2#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K2's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k2_g*dt computed
            ca.Add_MulC_I(rk4_g, k_g, 2.0)
            eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
            ca.MulC_I(k_m_part, dt)  # K2_m_part*dt computed
            ca.Add_MulC_I(rk4_m_part, k_m_part, 2.0)
            eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                            rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f_part, dt)  # K2_f_part*dt computed
            ca.Add_MulC_I(rk4_f_part, k_f_part, 2.0)

            # for K3
            # get g and ginv at t+1/2#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.MulC_I(k_g, 0.5)  # K2_g*dt/2
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1/2#
            ca.Copy(rk4_scratch.m_part, tdisc_spline[i-1].m_part)
            ca.MulC_I(k_m_part, 0.5)  # K2_m_part*dt/2
            ca.Add_I(rk4_scratch.m_part, k_m_part)
            ca.Add_I(rk4_scratch.m_part, tdisc_spline[0].m)
            ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, rk4_scratch.m_part)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1/2#
            ca.Copy(rk4_scratch.f_part, tdisc_spline[i-1].f_part)
            ca.MulC_I(k_f_part, 0.5)  # K2_f_part*dt/2
            ca.Add_I(rk4_scratch.f_part, k_f_part)
            ca.Add_I(rk4_scratch.f_part, tdisc_spline[0].f)
            ca.Ad(rk4_scratch.f, rk4_scratch.g, rk4_scratch.f_part)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1/2#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K3's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k3_g*dt computed
            ca.Add_MulC_I(rk4_g, k_g, 2.0)
            eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
            ca.MulC_I(k_m_part, dt)  # K3_m_part*dt computed
            ca.Add_MulC_I(rk4_m_part, k_m_part, 2.0)
            eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                            rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f_part, dt)  # K3_f_part*dt computed
            ca.Add_MulC_I(rk4_f_part, k_f_part, 2.0)

            # for K4
            # get g and ginv at t+1#
            ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
            ca.Add_I(rk4_scratch.g, k_g)
            ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
            # get m and v at t+1#
            ca.Copy(rk4_scratch.m_part, tdisc_spline[i-1].m_part)
            ca.Add_I(rk4_scratch.m_part, k_m_part)
            ca.Add_I(rk4_scratch.m_part, tdisc_spline[0].m)
            ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, rk4_scratch.m_part)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            # get u and f at t+1#
            ca.Copy(rk4_scratch.f_part, tdisc_spline[i-1].f_part)
            ca.Add_I(rk4_scratch.f_part, k_f_part)
            ca.Add_I(rk4_scratch.f_part, tdisc_spline[0].f)
            ca.Ad(rk4_scratch.f, rk4_scratch.g, rk4_scratch.f_part)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            # get P and I at t+1#
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[index_last_control].P)

            # now compute all K4's
            eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
            ca.MulC_I(k_g, dt)  # k4_g*dt computed
            ca.Add_I(rk4_g, k_g)
            eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
            ca.MulC_I(k_m_part, dt)  # K4_m_part*dt computed
            ca.Add_I(rk4_m_part, k_m_part)
            eval_rhs_f_part(k_f_part, scratch_v1, rk4_scratch.P, rk4_scratch.I, rk4_scratch.f,
                            rk4_scratch.m, rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_f_part, dt)  # K4_f_part*dt computed
            ca.Add_I(rk4_f_part, k_f_part)

            # for final update
            ca.MulC_I(rk4_g, 1.0/float(6.0))
            ca.MulC_I(rk4_m_part, 1.0/float(6.0))
            ca.MulC_I(rk4_f_part, 1.0/float(6.0))

            # now take the final update step
            ca.Add(tdisc_spline[i].g, tdisc_spline[i-1].g, rk4_g)  # updated g
            ca.UpdateInverse(tdisc_spline[i].ginv, scratch_v1, tdisc_spline[i-1].ginv, rk4_g, cf.optim.NIterForInverse)  # updated ginv
            ca.Add(tdisc_spline[i].m_part, tdisc_spline[i-1].m_part, rk4_m_part)  # final updated m_part
            ca.Add(scratch_v1, tdisc_spline[0].m, tdisc_spline[i].m_part)
            ca.CoAd(tdisc_spline[i].m, tdisc_spline[i].ginv, scratch_v1)  # final updated m
            ca.Add(tdisc_spline[i].f_part, tdisc_spline[i-1].f_part, rk4_f_part)  # final updated f_part
            ca.Add(scratch_v1, tdisc_spline[0].f, tdisc_spline[i].f_part)
            ca.Ad(tdisc_spline[i].f, tdisc_spline[i].g, scratch_v1)  # final updated f
            ca.ApplyH(tdisc_spline[i].I, tdisc_spline[0].I, tdisc_spline[i].ginv)  # final updated I

            if not tdisc_spline[i].is_control:
                # input previous P[0], this g
                # get g_{last_control, this}
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
                cavmcommon.SplatSafe(tdisc_spline[i].P, scratch_v1, tdisc_spline[index_last_control].P)  # final update P
            else:
                index_last_control = i  # for subsequent iterations

        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)

        # now update other duals, v and u, required for the next integration step
        ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
        diff_op.applyInverseOperator(tdisc_spline[i].v)
        ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
        diff_op.applyOperator(tdisc_spline[i].u)
        '''
        # nikhil: debug
        if i == 5:
            index_last_control = 5
            ca.MulC_I(tdisc_spline[i].P, -2000.0)

        '''

# WARNINING: this version may not work, it at least has a missing term
# with P term in the integration of m_adj which was found as a bug in
# derivation. Please use integrate_backward_rk4_direct and
# integrate_backward_rk4_direct_ext#
def integrate_backward_rk4(out_gradient, cf, tdisc_spline, diff_op, rk4_scratch=None):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)
    # scratch variables
    rk4_adj_m = ca.ManagedField3D(grid, mem_type)
    rk4_adj_f_part = ca.ManagedField3D(grid, mem_type)
    rk4_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    rk4_adj_I_part = ca.ManagedImage3D(grid, mem_type)
    k_adj_m = ca.ManagedField3D(grid, mem_type)
    k_adj_f_part = ca.ManagedField3D(grid, mem_type)
    k_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    k_adj_I_part = ca.ManagedImage3D(grid, mem_type)

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_P, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_P_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I_part, 0.0)

    if tdisc_spline[-1].has_data:
        ca.Sub(scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J)
        ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
        ca.MulC_I(scratch_I1, -1.0)  # negative gradient
        cavmcommon.SplatSafe(tdisc_spline[-1].adj_I_part, tdisc_spline[-1].ginv, scratch_I1)
        ca.Copy(tdisc_spline[-1].adj_I, scratch_I1)

    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1

    controls_counter = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            eval_rhs_adj_m(k_adj_m, scratch_v2,
                           tdisc_spline[i+1].adj_m,
                           tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                           tdisc_spline[i+1].f,
                           tdisc_spline[i+1].adj_f,
                           tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_I,
                           tdisc_spline[i+1].I,
                           tdisc_spline[i+1].adj_P,
                           tdisc_spline[i+1].P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # K1_adj_m*dt computed
            ca.Copy(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].m, tdisc_spline[i+1].g)
            ca.MulC_I(k_adj_f_part, dt)  # K1_adj_f_part*dt computed
            ca.Copy(rk4_adj_f_part, k_adj_f_part)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i+1].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                                scratch_v1)
            ca.MulC_I(k_adj_P_part, dt)  # K1_adj_P_part*dt computed
            ca.Copy(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K1_adj_I_part*dt computed
            ca.Copy(rk4_adj_I_part, k_adj_I_part)

            # compute and store primal g, ginv, v, m, f, I at t+1/2 for computing k2 and k3
            if i > 0:
                ca.SubMulC(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[i].g, 0.5)
                ca.Add(rk4_scratch.g, tdisc_spline[i].g, scratch_v1)
                ca.UpdateInverse(rk4_scratch.ginv, scratch_v2, tdisc_spline[i].ginv, scratch_v1, cf.optim.NIterForInverse)
            else:  # add 0.5 times identity
                ca.HtoV(rk4_scratch.g, tdisc_spline[i+1].g)
                ca.MulC_I(rk4_scratch.g, 0.5)
                # iteratively update inverse as, g_{1,0} = Id - w\circ g_{1,0}
                ca.SetToIdentity(rk4_scratch.ginv)
                for k in range(cf.optim.NIterForInverse):
                    ca.ApplyH(scratch_v2, rk4_scratch.g, rk4_scratch.ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                    ca.HtoV(rk4_scratch.ginv, scratch_v2)
                    ca.MulC_I(rk4_scratch.ginv, -1.0)
                ca.VtoH_I(rk4_scratch.g)
            ca.AddMulC(scratch_v1, tdisc_spline[i+1].m_part, tdisc_spline[i].m_part, 0.5)
            ca.Add_I(scratch_v1, tdisc_spline[0].m)
            ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, scratch_v1)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            ca.AddMulC(scratch_v1, tdisc_spline[i+1].f_part, tdisc_spline[i].f_part, 0.5)
            ca.Add_I(scratch_v1, tdisc_spline[0].f)
            ca.Ad(rk4_scratch.f, rk4_scratch.g, scratch_v1)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[all_control_indices[controls_counter]].P)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[0].P)

            # for K2
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K1_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f_part, tdisc_spline[i+1].adj_f_part)
            ca.MulC_I(k_adj_f_part, 0.5)  # K1_adj_f_part*dt/2
            ca.Sub_I(rk4_scratch.adj_f_part, k_adj_f_part)
            ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K1_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)
            # get adj_I at t+1/2#

            # now compute all K2's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           rk4_scratch.I, rk4_scratch.adj_P,
                           rk4_scratch.P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k2_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                                rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f_part, dt)  # K2_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f_part, k_adj_f_part, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            ca.MulC_I(k_adj_P_part, dt)  # K2_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K2_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K3
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K2_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f_part, tdisc_spline[i+1].adj_f_part)
            ca.MulC_I(k_adj_f_part, 0.5)  # K2_adj_f_part*dt/2
            ca.Sub_I(rk4_scratch.adj_f_part, k_adj_f_part)
            ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K2_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)

            # now compute all K3's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           rk4_scratch.I, rk4_scratch.adj_P,
                           rk4_scratch.P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k3_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                                rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f_part, dt)  # K3_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f_part, k_adj_f_part, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            ca.MulC_I(k_adj_P_part, dt)  # K3_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K3_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K4
            # get adj_m and adj_v at t+1#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1#
            ca.Copy(rk4_scratch.adj_f_part, tdisc_spline[i+1].adj_f_part)
            ca.Sub_I(rk4_scratch.adj_f_part, k_adj_f_part)
            ca.CoAd(rk4_scratch.adj_f, tdisc_spline[i].ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, tdisc_spline[i].g, rk4_scratch.adj_I_part)

            # now compute all K4's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           tdisc_spline[i].v, tdisc_spline[i].m,
                           tdisc_spline[i].f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           tdisc_spline[i].I, rk4_scratch.adj_P,
                           tdisc_spline[i].P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k4_adj_m*dt computed
            ca.Add_I(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                                tdisc_spline[i].m, tdisc_spline[i].g)
            ca.MulC_I(k_adj_f_part, dt)  # K4_adj_f_part*dt computed
            ca.Add_I(rk4_adj_f_part, k_adj_f_part)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i].I, rk4_scratch.adj_u,
                                scratch_v1)
            ca.MulC_I(k_adj_P_part, dt)  # K4_adj_P_part*dt computed
            ca.Add_I(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i].P, rk4_scratch.adj_u,
                                tdisc_spline[i].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K4_adj_I_part*dt computed
            ca.Add_I(rk4_adj_I_part, k_adj_I_part)

            # for final update
            ca.MulC_I(rk4_adj_m, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_f_part, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_P_part, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_I_part, 1.0/float(6.0))

            # now take the final update step
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, rk4_adj_m)  # updated adj_m
            ca.Sub(tdisc_spline[i].adj_f_part, tdisc_spline[i+1].adj_f_part, rk4_adj_f_part)  # updated adj_f_part
            ca.CoAd(tdisc_spline[i].adj_f, tdisc_spline[i].ginv, tdisc_spline[i].adj_f_part)  # updated adj_f
            ca.Sub(tdisc_spline[i].adj_P_part, tdisc_spline[i+1].adj_P_part, rk4_adj_P_part)  # updated adj_P_part
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[all_control_indices[controls_counter]].g, tdisc_spline[i].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].ginv)
            ca.ApplyH(tdisc_spline[i].adj_P, tdisc_spline[i].adj_P_part, scratch_v1)  # updated adj_P
            ca.Add(tdisc_spline[i].adj_I_part, tdisc_spline[i+1].adj_I_part, rk4_adj_I_part)  # upated adj_I_part
            cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)  # updated adj_I

            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
                cavmcommon.SplatSafe(scratch_I2, tdisc_spline[i].ginv, scratch_I1)
                ca.Sub_I(tdisc_spline[i].adj_I_part, scratch_I2)
                cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)

            if tdisc_spline[i].is_control:
                if cf.optim.regularize_Ps:        
                    ca.Gradient(scratch_v1, tdisc_spline[i].I)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, 1.0)                
                    diff_op.applyInverseOperator(scratch_v1)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, -cf.optim.r_Ps)                
                    ca.Divergence(out_gradient.grad_controls[controls_counter], scratch_v1)
                    ca.Sub_I(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P)  # copy Ps
                else:    
                    ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P, -1.0)  # copy -ve into out_gradient
                ca.SetMem(tdisc_spline[i].adj_P, 0.0)  # reset adj_P
                ca.SetMem(tdisc_spline[i].adj_P_part, 0.0)  # reset adj_P_part
                controls_counter -= 1
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0
    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    if cf.optim.regularize_u0:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].f, cf.optim.r_u0)  # copy m0
        ca.Sub_I(out_gradient.grad_u, tdisc_spline[0].adj_u)  # copy m0
    else:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient
        
    if cf.optim.regularize_P0:        
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)                
        diff_op.applyInverseOperator(scratch_v1)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, -cf.optim.r_P0)                
        ca.Divergence(out_gradient.grad_P, scratch_v1)
        ca.Sub_I(out_gradient.grad_P, tdisc_spline[0].adj_P)  # copy m0
    else:    
        ca.MulC(out_gradient.grad_P, tdisc_spline[0].adj_P, -1.0)  # copy -ve into out_gradient
    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    return out_gradient

def integrate_backward_rk4_direct(out_gradient, cf, tdisc_spline, diff_op, rk4_scratch=None):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)
    # scratch variables
    rk4_adj_m = ca.ManagedField3D(grid, mem_type)
    rk4_adj_f = ca.ManagedField3D(grid, mem_type)
    rk4_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    rk4_adj_I_part = ca.ManagedImage3D(grid, mem_type)
    k_adj_m = ca.ManagedField3D(grid, mem_type)
    k_adj_f = ca.ManagedField3D(grid, mem_type)
    k_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    k_adj_I_part = ca.ManagedImage3D(grid, mem_type)

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_P, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_P_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I_part, 0.0)

    if tdisc_spline[-1].has_data:
        ca.Sub(scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J)
        ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
        ca.MulC_I(scratch_I1, -1.0)  # negative gradient
        cavmcommon.SplatSafe(tdisc_spline[-1].adj_I_part, tdisc_spline[-1].ginv, scratch_I1)
        ca.Copy(tdisc_spline[-1].adj_I, scratch_I1)

    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1

    controls_counter = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            eval_rhs_adj_m(k_adj_m, scratch_v2,
                           tdisc_spline[i+1].adj_m,
                           tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                           tdisc_spline[i+1].f,
                           tdisc_spline[i+1].adj_f,
                           tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_I,
                           tdisc_spline[i+1].I,
                           tdisc_spline[i+1].adj_P,
                           tdisc_spline[i+1].P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # K1_adj_m*dt computed
            ca.Copy(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].m, tdisc_spline[i+1].v, tdisc_spline[i+1].adj_f)
            #CheckNaN(k_adj_f, i)
            #CheckNaN(tdisc_spline[i+1].adj_v)
            #CheckNaN(tdisc_spline[i+1].adj_u, i)
            #CheckNaN(tdisc_spline[i+1].adj_m)
            #CheckNaN(tdisc_spline[i+1].adj_f)
            #CheckNaN(k_adj_f)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
            #                     tdisc_spline[i+1].m, tdisc_spline[i+1].g)
            ca.MulC_I(k_adj_f, dt)  # K1_adj_f_part*dt computed
            ca.Copy(rk4_adj_f, k_adj_f)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i+1].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K1_adj_P_part*dt computed
            ca.Copy(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K1_adj_I_part*dt computed
            ca.Copy(rk4_adj_I_part, k_adj_I_part)

            # compute and store primal g, ginv, v, m, f, I at t+1/2 for computing k2 and k3
            if i > 0:
                ca.SubMulC(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[i].g, 0.5)
                ca.Add(rk4_scratch.g, tdisc_spline[i].g, scratch_v1)
                ca.UpdateInverse(rk4_scratch.ginv, scratch_v2, tdisc_spline[i].ginv, scratch_v1, cf.optim.NIterForInverse)
            else:  # add 0.5 times identity
                ca.HtoV(rk4_scratch.g, tdisc_spline[i+1].g)
                ca.MulC_I(rk4_scratch.g, 0.5)
                # iteratively update inverse as, g_{1,0} = Id - w\circ g_{1,0}
                ca.SetToIdentity(rk4_scratch.ginv)
                for k in range(cf.optim.NIterForInverse):
                    ca.ApplyH(scratch_v2, rk4_scratch.g, rk4_scratch.ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                    ca.HtoV(rk4_scratch.ginv, scratch_v2)
                    ca.MulC_I(rk4_scratch.ginv, -1.0)
                ca.VtoH_I(rk4_scratch.g)
            ca.AddMulC(rk4_scratch.m, tdisc_spline[i+1].m, tdisc_spline[i].m, 0.5)
            #ca.Add_I(scratch_v1, tdisc_spline[0].m)
            #ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, scratch_v1)
            #CheckNaN(rk4_scratch.m)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            ca.AddMulC(rk4_scratch.f, tdisc_spline[i+1].f, tdisc_spline[i].f, 0.5)
            #ca.Add_I(scratch_v1, tdisc_spline[0].f)
            #ca.Ad(rk4_scratch.f, rk4_scratch.g, scratch_v1)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[all_control_indices[controls_counter]].P)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[0].P)

            # for K2
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K1_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K1_adj_f_part*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K1_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)
            # get adj_P at t+1/2#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.MulC_I(k_adj_P_part, 0.5)  # K1_adj_P_part*dt/2
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, rk4_scratch.ginv)

            # now compute all K2's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           rk4_scratch.I, rk4_scratch.adj_P,
                           rk4_scratch.P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k2_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            #CheckNaN(rk4_scratch.adj_f)
            #CheckNaN(rk4_scratch.v)
            #CheckNaN(rk4_scratch.adj_u)
            #CheckNaN(rk4_scratch.adj_v)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f, dt)  # K2_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K2_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K2_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K3
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K2_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K2_adj_f_part*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)

            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K2_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)
            # get adj_P at t+1/2#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.MulC_I(k_adj_P_part, 0.5)  # K1_adj_P_part*dt/2
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, rk4_scratch.ginv)

            # now compute all K3's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           rk4_scratch.I, rk4_scratch.adj_P,
                           rk4_scratch.P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k3_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f, i)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f, dt)  # K3_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            #CheckNaN(rk4_scratch.adj_u)
            #CheckNaN(rk4_scratch.I)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K3_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K3_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K4
            # get adj_m and adj_v at t+1#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, tdisc_spline[i].ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, tdisc_spline[i].g, rk4_scratch.adj_I_part)
            # get adj_P at t+1#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, tdisc_spline[i].ginv)

            # now compute all K4's
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           tdisc_spline[i].v, tdisc_spline[i].m,
                           tdisc_spline[i].f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           tdisc_spline[i].I, rk4_scratch.adj_P,
                           tdisc_spline[i].P, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k4_adj_m*dt computed
            ca.Add_I(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           tdisc_spline[i].m, tdisc_spline[i].v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f, i)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    tdisc_spline[i].m, tdisc_spline[i].g)
            ca.MulC_I(k_adj_f, dt)  # K4_adj_f_part*dt computed
            ca.Add_I(rk4_adj_f, k_adj_f)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i].I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K4_adj_P_part*dt computed
            ca.Add_I(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i].P, rk4_scratch.adj_u,
                                tdisc_spline[i].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K4_adj_I_part*dt computed
            ca.Add_I(rk4_adj_I_part, k_adj_I_part)

            # for final update
            ca.MulC_I(rk4_adj_m, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_f, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_P_part, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_I_part, 1.0/float(6.0))
            #CheckNaN(rk4_adj_P_part)
            #CheckNaN(rk4_adj_f, i)
            # now take the final update step
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, rk4_adj_m)  # updated adj_m
            ca.Add(tdisc_spline[i].adj_f, tdisc_spline[i+1].adj_f, rk4_adj_f)  # updated adj_f_part
            #ca.CoAd(tdisc_spline[i].adj_f, tdisc_spline[i].ginv, tdisc_spline[i].adj_f_part)  # updated adj_f
            ca.Sub(tdisc_spline[i].adj_P_part, tdisc_spline[i+1].adj_P_part, rk4_adj_P_part)  # updated adj_P_part
            #CheckNaN(tdisc_spline[i].adj_P_part)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[all_control_indices[controls_counter]].g, tdisc_spline[i].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].ginv)
            #CheckNaN(tdisc_spline[all_control_indices[controls_counter]].g)
            #CheckNaN(tdisc_spline[i].ginv)
            #CheckNaN(scratch_v1)
            ca.ApplyH(tdisc_spline[i].adj_P, tdisc_spline[i].adj_P_part, scratch_v1)  # updated adj_P
            ca.Add(tdisc_spline[i].adj_I_part, tdisc_spline[i+1].adj_I_part, rk4_adj_I_part)  # upated adj_I_part
            cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)  # updated adj_I
            #CheckNaN(tdisc_spline[i].adj_P)
            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
                cavmcommon.SplatSafe(scratch_I2, tdisc_spline[i].ginv, scratch_I1)
                ca.Sub_I(tdisc_spline[i].adj_I_part, scratch_I2)
                cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)

            if tdisc_spline[i].is_control:
                #CheckNaN(tdisc_spline[i].adj_P)
                if cf.optim.regularize_Ps:        
                    ca.Gradient(scratch_v1, tdisc_spline[i].I)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, 1.0)                
                    diff_op.applyInverseOperator(scratch_v1)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, -cf.optim.r_Ps)                
                    ca.Divergence(out_gradient.grad_controls[controls_counter], scratch_v1)
                    ca.Sub_I(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P)  # copy Ps
                else:    
                    ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P, -1.0)  # copy -ve into out_gradient
                ca.SetMem(tdisc_spline[i].adj_P, 0.0)  # reset adj_P
                ca.SetMem(tdisc_spline[i].adj_P_part, 0.0)  # reset adj_P_part
                controls_counter -= 1
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0

    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    if cf.optim.regularize_u0:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].f, cf.optim.r_u0)  # copy m0
        ca.Sub_I(out_gradient.grad_u, tdisc_spline[0].adj_u)  # copy m0
    else:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient
    if cf.optim.regularize_P0:        
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)                
        diff_op.applyInverseOperator(scratch_v1)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, -cf.optim.r_P0)                
        ca.Divergence(out_gradient.grad_P, scratch_v1)
        ca.Sub_I(out_gradient.grad_P, tdisc_spline[0].adj_P)  # copy m0
    else:    
        ca.MulC(out_gradient.grad_P, tdisc_spline[0].adj_P, -1.0)  # copy -ve into out_gradient
    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    return out_gradient


def plot_vf_2d(v, name):
    if v.memType() == ca.MEM_DEVICE:
        vh = v.copy()
        vh.toType(ca.MEM_HOST)
        vArr_x, vArr_y, vArr_z = vh.asnp()
    else:
        vArr_x, vArr_y, vArr_z = v.asnp()
    vArr_x = np.squeeze(vArr_x)
    vArr_y = np.squeeze(vArr_y)

    #title = name
    #plt.figure(title)
    plt.subplot(1, 2, 1)
    plt.imshow(vArr_x)
    plt.colorbar()
    plt.title('component x')
    plt.draw()
    plt.subplot(1, 2, 2)
    plt.imshow(vArr_y)
    plt.colorbar()
    plt.title('component y')
    plt.draw()
    plt.show(block=False)
    return plt
    #plt.show(block=False)
    #plt.show(block="False")

def plot_im_2d(I, name):
    if I.memType() == ca.MEM_DEVICE:
        Ih = I.copy()
        Ih.toType(ca.MEM_HOST)
        IArr = Ih.asnp()
    else:
        IArr = I.asnp()
    IArr = np.squeeze(IArr)
    #title = name
    #plt.figure(title)
    plt.imshow(IArr)
    plt.colorbar()
    plt.draw()
    plt.show(block=False)
    return plt
    #plt.show(block="False")

'''
def CheckField(hF, hG, name,disp):
    hGArr_x, hGArr_y, hGArr_z = hG.asnp()
    hGArr_x = np.squeeze(hGArr_x)
    hGArr_y = np.squeeze(hGArr_y)
    hFArr_x, hFArr_y, hFArr_z = hF.asnp()
    hFArr_x = np.squeeze(hFArr_x)
    hFArr_y = np.squeeze(hFArr_y)
    diff_x = hFArr_x-hGArr_x
    diff_y = hFArr_y-hGArr_y
    if disp:
        title = name
        plt.figure(title)

        plt.subplot(2,3,1)
        plt.imshow(np.squeeze(hFArr_x))
        plt.colorbar();
        plt.title('host x')
        plt.draw()
        plt.subplot(2,3,2)
        plt.imshow(np.squeeze(hGArr_x))
        plt.colorbar();
        plt.title('device x')
        plt.draw()
        plt.subplot(2,3,3)
        plt.imshow(np.squeeze(diff_x))
        plt.colorbar();
        plt.title('diff x')
        plt.draw()

        plt.subplot(2,3,4)
        plt.imshow(np.squeeze(hFArr_y))
        plt.colorbar();
        plt.title('host y')
        plt.draw()
        plt.subplot(2,3,5)
        plt.imshow(np.squeeze(hGArr_y))
        plt.colorbar();
        plt.title('device y')
        plt.draw()
        plt.subplot(2,3,6)
        plt.imshow(np.squeeze(diff_y))
        plt.colorbar();
        plt.title('diff y')
        plt.draw()
        plt.show()
    diffMax = max(np.max(np.abs(diff_x)), np.max(np.abs(diff_y)))
    diffAv = (np.sum(np.abs(diff_x)) + np.sum(np.abs(diff_y))) \
        / (2*np.prod(diff_x.shape))
    return (diffAv, diffMax)
'''

################################################################################################
# Code for extension to splines
################################################################################################
def eval_rhs_adj_m_ext(out_v, scratch_v, adj_m, v, m, f, adj_f, adj_u,
                       adj_I, I, adj_P, P, adj_h, h, diff_op):   
    ca.CoAdInf(out_v, f, adj_f)
    ca.Gradient(scratch_v, I)
    ca.MulMulC_I(scratch_v, adj_I, 1.0)
    ca.Add_I(out_v, scratch_v)
    ca.Gradient(scratch_v, adj_P)
    ca.MulMulC_I(scratch_v, P, 1.0)
    ca.Sub_I(out_v, scratch_v)    
    ca.CoAdInf(scratch_v, adj_m, m)
    ca.Sub_I(out_v, scratch_v)
    ca.JacobianXY(scratch_v, h, adj_h)
    ca.Add_I(out_v, scratch_v)
    
    diff_op.applyInverseOperator(out_v)
    ca.AdInf(scratch_v, adj_m, v)
    ca.Sub_I(out_v, scratch_v)
    ca.AdInf(scratch_v, f, adj_u)
    ca.Add_I(out_v, scratch_v)

    return out_v

def eval_rhs_adj_h_ext(out_v, adj_h, v):
    ca.DivergenceTensor(out_v, adj_h, v)

def integrate_backward_rk4_direct_ext(out_gradient, cf, tdisc_spline, diff_op, rk4_scratch=None):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)

    # scratch variables
    #rk4_adj_h = ca.ManagedField3D(grid, mem_type)
    rk4_adj_m = ca.ManagedField3D(grid, mem_type)
    rk4_adj_f = ca.ManagedField3D(grid, mem_type)
    rk4_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    rk4_adj_I_part = ca.ManagedImage3D(grid, mem_type)
    #k_adj_h = ca.ManagedField3D(grid, mem_type)
    k_adj_m = ca.ManagedField3D(grid, mem_type)
    k_adj_f = ca.ManagedField3D(grid, mem_type)
    k_adj_P_part = ca.ManagedImage3D(grid, mem_type)
    k_adj_I_part = ca.ManagedImage3D(grid, mem_type)
    adj_h_part = ca.ManagedField3D(grid, mem_type)    

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_h, 0.0)
    ca.SetMem(adj_h_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_P, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_P_part, 0.0)

    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I_part, 0.0)

    if tdisc_spline[-1].has_data:
        # image match
        if cf.study.regressImages:
            ca.Sub(tdisc_spline[-1].adj_I, tdisc_spline[-1].I, tdisc_spline[-1].J)
            ca.DivC_I(tdisc_spline[-1].adj_I, -cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # negative gradient at the measurement
            cavmcommon.SplatSafe(tdisc_spline[-1].adj_I_part, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_I)
        
        # def match
        if cf.study.regressDefFields:
            ca.Sub(tdisc_spline[-1].adj_h, tdisc_spline[-1].ginv, tdisc_spline[-1].def_data)
            ca.DivC_I(tdisc_spline[-1].adj_h, -cf.splinesconfig.stdNoiseDef*cf.splinesconfig.stdNoiseDef)  # negative gradient at the measurement
            cavmcommon.SplatVSafe(adj_h_part, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_h)

    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1

    controls_counter = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            #eval_rhs_adj_h_ext(k_adj_h, tdisc_spline[i+1].adj_h, tdisc_spline[i+1].v)
            #ca.MulC_I(k_adj_h, dt)  # K1_adj_h*dt computed
            #ca.Copy(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m_ext(k_adj_m, scratch_v2,
                               tdisc_spline[i+1].adj_m,
                               tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                               tdisc_spline[i+1].f,
                               tdisc_spline[i+1].adj_f,
                               tdisc_spline[i+1].adj_u,
                               tdisc_spline[i+1].adj_I,
                               tdisc_spline[i+1].I,
                               tdisc_spline[i+1].adj_P,
                               tdisc_spline[i+1].P,
                               tdisc_spline[i+1].adj_h,
                               tdisc_spline[i+1].ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # K1_adj_m*dt computed
            ca.Copy(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].m, tdisc_spline[i+1].v, tdisc_spline[i+1].adj_f)
            #CheckNaN(k_adj_f, i)
            #CheckNaN(tdisc_spline[i+1].adj_v)
            #CheckNaN(tdisc_spline[i+1].adj_u, i)
            #CheckNaN(tdisc_spline[i+1].adj_m)
            #CheckNaN(tdisc_spline[i+1].adj_f)
            #CheckNaN(k_adj_f)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
            #                     tdisc_spline[i+1].m, tdisc_spline[i+1].g)
            ca.MulC_I(k_adj_f, dt)  # K1_adj_f_part*dt computed
            ca.Copy(rk4_adj_f, k_adj_f)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i+1].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i+1].I, tdisc_spline[i+1].adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K1_adj_P_part*dt computed
            ca.Copy(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i+1].P, tdisc_spline[i+1].adj_u,
                                tdisc_spline[i+1].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K1_adj_I_part*dt computed
            ca.Copy(rk4_adj_I_part, k_adj_I_part)

            # compute and store primal g, ginv, v, m, f, I at t+1/2 for computing k2 and k3
            if i > 0:
                ca.SubMulC(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[i].g, 0.5)
                ca.Add(rk4_scratch.g, tdisc_spline[i].g, scratch_v1)
                ca.UpdateInverse(rk4_scratch.ginv, scratch_v2, tdisc_spline[i].ginv, scratch_v1, cf.optim.NIterForInverse)
            else:  # add 0.5 times identity
                ca.HtoV(rk4_scratch.g, tdisc_spline[i+1].g)
                ca.MulC_I(rk4_scratch.g, 0.5)
                # iteratively update inverse as, g_{1,0} = Id - w\circ g_{1,0}
                ca.SetToIdentity(rk4_scratch.ginv)
                for k in range(cf.optim.NIterForInverse):
                    ca.ApplyH(scratch_v2, rk4_scratch.g, rk4_scratch.ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                    ca.HtoV(rk4_scratch.ginv, scratch_v2)
                    ca.MulC_I(rk4_scratch.ginv, -1.0)
                ca.VtoH_I(rk4_scratch.g)
            ca.AddMulC(rk4_scratch.m, tdisc_spline[i+1].m, tdisc_spline[i].m, 0.5)
            #ca.Add_I(scratch_v1, tdisc_spline[0].m)
            #ca.CoAd(rk4_scratch.m, rk4_scratch.ginv, scratch_v1)
            #CheckNaN(rk4_scratch.m)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            ca.AddMulC(rk4_scratch.f, tdisc_spline[i+1].f, tdisc_spline[i].f, 0.5)
            #ca.Add_I(scratch_v1, tdisc_spline[0].f)
            #ca.Ad(rk4_scratch.f, rk4_scratch.g, scratch_v1)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[all_control_indices[controls_counter]].P)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
                cavmcommon.SplatSafe(rk4_scratch.P, scratch_v1, tdisc_spline[0].P)

            # for K2
            # get adj_h at t+1/2#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.MulC_I(k_adj_h, 0.5)  # K1_adj_h*dt/2
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, rk4_scratch.g, adj_h_part)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K1_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K1_adj_f_part*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K1_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)
            # get adj_P at t+1/2#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.MulC_I(k_adj_P_part, 0.5)  # K1_adj_P_part*dt/2
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, rk4_scratch.ginv)

            # now compute all K2's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, rk4_scratch.v)
            #ca.MulC_I(k_adj_h, dt)  # K2_adj_h*dt computed
            #ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m_ext(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                               rk4_scratch.v, rk4_scratch.m,
                               rk4_scratch.f, rk4_scratch.adj_f,
                               rk4_scratch.adj_u, rk4_scratch.adj_I,
                               rk4_scratch.I, rk4_scratch.adj_P,
                               rk4_scratch.P, rk4_scratch.adj_h,
                               rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k2_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            #CheckNaN(rk4_scratch.adj_f)
            #CheckNaN(rk4_scratch.v)
            #CheckNaN(rk4_scratch.adj_u)
            #CheckNaN(rk4_scratch.adj_v)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f, dt)  # K2_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K2_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K2_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K3
            # get adj_h at t+1/2#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.MulC_I(k_adj_h, 0.5)  # K1_adj_h*dt/2
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, rk4_scratch.g, adj_h_part)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K2_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K2_adj_f_part*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, rk4_scratch.ginv, rk4_scratch.adj_f_part)

            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.MulC_I(k_adj_I_part, 0.5)  # K2_adj_I_part*dt/2
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)
            # get adj_P at t+1/2#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.MulC_I(k_adj_P_part, 0.5)  # K1_adj_P_part*dt/2
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, rk4_scratch.ginv)

            # now compute all K3's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, rk4_scratch.v)
            #ca.MulC_I(k_adj_h, dt)  # K3_adj_h*dt computed
            #ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m_ext(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           rk4_scratch.I, rk4_scratch.adj_P,
                           rk4_scratch.P, rk4_scratch.adj_h,
                           rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k3_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f, i)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    rk4_scratch.m, rk4_scratch.g)
            ca.MulC_I(k_adj_f, dt)  # K3_adj_f_part*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
            #CheckNaN(rk4_scratch.adj_u)
            #CheckNaN(rk4_scratch.I)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, rk4_scratch.I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K3_adj_P_part*dt computed
            ca.Add_MulC_I(rk4_adj_P_part, k_adj_P_part, 2.0)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, rk4_scratch.P, rk4_scratch.adj_u,
                                rk4_scratch.ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K3_adj_I_part*dt computed
            ca.Add_MulC_I(rk4_adj_I_part, k_adj_I_part, 2.0)

            # for K4
            # get adj_h at t+1#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, tdisc_spline[i].g, adj_h_part)
            # get adj_m and adj_v at t+1#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            #ca.CoAd(rk4_scratch.adj_f, tdisc_spline[i].ginv, rk4_scratch.adj_f_part)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_I at t+1#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            ca.Add_I(rk4_scratch.adj_I_part, k_adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, tdisc_spline[i].g, rk4_scratch.adj_I_part)
            # get adj_P at t+1#
            ca.Copy(rk4_scratch.adj_P_part, tdisc_spline[i+1].adj_P_part)
            ca.Sub_I(rk4_scratch.adj_P_part, k_adj_P_part)
            ca.ApplyH(rk4_scratch.adj_P, rk4_scratch.adj_P_part, tdisc_spline[i].ginv)

            # now compute all K4's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, tdisc_spline[i].v)
            #ca.MulC_I(k_adj_h, dt)  # K4_adj_h*dt computed
            #ca.Add_I(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m_ext(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           tdisc_spline[i].v, tdisc_spline[i].m,
                           tdisc_spline[i].f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_I,
                           tdisc_spline[i].I, rk4_scratch.adj_P,
                           tdisc_spline[i].P, rk4_scratch.adj_h,
                           tdisc_spline[i].ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k4_adj_m*dt computed
            ca.Add_I(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           tdisc_spline[i].m, tdisc_spline[i].v, rk4_scratch.adj_f)
            #CheckNaN(k_adj_f, i)
            #eval_rhs_adj_f_part(k_adj_f_part, scratch_v2, rk4_scratch.adj_v, rk4_scratch.adj_u,
            #                    tdisc_spline[i].m, tdisc_spline[i].g)
            ca.MulC_I(k_adj_f, dt)  # K4_adj_f_part*dt computed
            ca.Add_I(rk4_adj_f, k_adj_f)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].g)
            eval_rhs_adj_P_part(k_adj_P_part, scratch_I2, scratch_v2, tdisc_spline[i].I, rk4_scratch.adj_u,
                                scratch_v1)
            #CheckNaN(k_adj_P_part)
            ca.MulC_I(k_adj_P_part, dt)  # K4_adj_P_part*dt computed
            ca.Add_I(rk4_adj_P_part, k_adj_P_part)
            eval_rhs_adj_I_part(k_adj_I_part, scratch_I2, scratch_v2, tdisc_spline[i].P, rk4_scratch.adj_u,
                                tdisc_spline[i].ginv)
            ca.MulC_I(k_adj_I_part, dt)  # K4_adj_I_part*dt computed
            ca.Add_I(rk4_adj_I_part, k_adj_I_part)

            # for final update
            #ca.MulC_I(rk4_adj_h, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_m, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_f, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_P_part, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_I_part, 1.0/float(6.0))
            #CheckNaN(rk4_adj_P_part)
            #CheckNaN(rk4_adj_f, i)
            # now take the final update step
            #ca.Sub(tdisc_spline[i].adj_h, tdisc_spline[i+1].adj_h, rk4_adj_h)  # updated adj_h
            cavmcommon.SplatVSafe(tdisc_spline[i].adj_h, tdisc_spline[i].g, adj_h_part)  # updated adj_h
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, rk4_adj_m)  # updated adj_m
            ca.Add(tdisc_spline[i].adj_f, tdisc_spline[i+1].adj_f, rk4_adj_f)  # updated adj_f_part
            #ca.CoAd(tdisc_spline[i].adj_f, tdisc_spline[i].ginv, tdisc_spline[i].adj_f_part)  # updated adj_f
            ca.Sub(tdisc_spline[i].adj_P_part, tdisc_spline[i+1].adj_P_part, rk4_adj_P_part)  # updated adj_P_part
            #CheckNaN(tdisc_spline[i].adj_P_part)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[all_control_indices[controls_counter]].g, tdisc_spline[i].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i].ginv)
            #CheckNaN(tdisc_spline[all_control_indices[controls_counter]].g)
            #CheckNaN(tdisc_spline[i].ginv)
            #CheckNaN(scratch_v1)
            ca.ApplyH(tdisc_spline[i].adj_P, tdisc_spline[i].adj_P_part, scratch_v1)  # updated adj_P
            ca.Add(tdisc_spline[i].adj_I_part, tdisc_spline[i+1].adj_I_part, rk4_adj_I_part)  # upated adj_I_part
            cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)  # updated adj_I
            #CheckNaN(tdisc_spline[i].adj_P)
            if tdisc_spline[i].has_data:
                if cf.study.regressImages:
                    ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                    ca.DivC_I(scratch_I1, cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # gradient at the measurement
                    cavmcommon.SplatSafe(scratch_I2, tdisc_spline[i].ginv, scratch_I1)
                    ca.Sub_I(tdisc_spline[i].adj_I_part, scratch_I2)
                    cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)
                if cf.study.regressDefFields:
                    ca.Sub(scratch_v2, tdisc_spline[i].ginv, tdisc_spline[i].def_data)
                    ca.DivC_I(scratch_v2, cf.splinesconfig.stdNoiseDef*cf.splinesconfig.stdNoiseDef)  # gradient at the measurement
                    cavmcommon.SplatVSafe(scratch_v1, tdisc_spline[i].ginv, scratch_v2)
                    ca.Sub_I(adj_h_part, scratch_v1)
                    cavmcommon.SplatVSafe(tdisc_spline[i].adj_h, tdisc_spline[i].g, adj_h_part)

            if tdisc_spline[i].is_control:
                #CheckNaN(tdisc_spline[i].adj_P)
                if cf.optim.regularize_Ps:        
                    ca.Gradient(scratch_v1, tdisc_spline[i].I)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, 1.0)                
                    diff_op.applyInverseOperator(scratch_v1)
                    ca.MulMulC_I(scratch_v1, tdisc_spline[i].P, -cf.optim.r_Ps)                
                    ca.Divergence(out_gradient.grad_controls[controls_counter], scratch_v1)
                    ca.Sub_I(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P)  # copy Ps
                else:    
                    ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_P, -1.0)  # copy -ve into out_gradient

                ca.SetMem(tdisc_spline[i].adj_P, 0.0)  # reset adj_P
                ca.SetMem(tdisc_spline[i].adj_P_part, 0.0)  # reset adj_P_part
                controls_counter -= 1
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0

    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    if cf.optim.regularize_u0:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].f, cf.optim.r_u0)  # copy m0
        ca.Sub_I(out_gradient.grad_u, tdisc_spline[0].adj_u)  # copy m0
    else:
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient
    if cf.optim.regularize_P0:        
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, 1.0)                
        diff_op.applyInverseOperator(scratch_v1)
        ca.MulMulC_I(scratch_v1, tdisc_spline[0].P, -cf.optim.r_P0)                
        ca.Divergence(out_gradient.grad_P, scratch_v1)
        ca.Sub_I(out_gradient.grad_P, tdisc_spline[0].adj_P)  # copy m0
    else:    
        ca.MulC(out_gradient.grad_P, tdisc_spline[0].adj_P, -1.0)  # copy -ve into out_gradient
    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    return out_gradient



################################################################################################
# Code for geodesic matching with deformations
################################################################################################
def eval_rhs_adj_m_ext_geomatch(out_v, scratch_v, adj_m, v, m, adj_I,
                                I, adj_h, h, diff_op):   
    ca.Gradient(out_v, I)
    ca.MulMulC_I(out_v, adj_I, 1.0)
    ca.CoAdInf(scratch_v, adj_m, m)
    ca.Sub_I(out_v, scratch_v)
    ca.JacobianXY(scratch_v, h, adj_h)
    ca.Add_I(out_v, scratch_v)    
    diff_op.applyInverseOperator(out_v)
    ca.AdInf(scratch_v, adj_m, v)
    ca.Sub_I(out_v, scratch_v)

    return out_v

def integrate_backward_rk4_direct_ext_geomatch(out_gradient, cf, tdisc_spline, diff_op, rk4_scratch=None):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)
    # scratch variables
    #rk4_adj_h = ca.ManagedField3D(grid, mem_type)
    rk4_adj_m = ca.ManagedField3D(grid, mem_type)
    #k_adj_h = ca.ManagedField3D(grid, mem_type)
    k_adj_m = ca.ManagedField3D(grid, mem_type)    
    adj_h_part = ca.ManagedField3D(grid, mem_type)    

    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_h, 0.0)
    ca.SetMem(adj_h_part, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_I_part, 0.0)

    if tdisc_spline[-1].has_data:
        # image match
        if cf.study.regressImages:
            ca.Sub(tdisc_spline[-1].adj_I, tdisc_spline[-1].I, tdisc_spline[-1].J)
            ca.DivC_I(tdisc_spline[-1].adj_I, -cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)  # negative gradient at the measurement
            cavmcommon.SplatSafe(tdisc_spline[-1].adj_I_part, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_I)
        
        # def match
        if cf.study.regressDefFields:
            ca.Sub(tdisc_spline[-1].adj_h, tdisc_spline[-1].ginv, tdisc_spline[-1].def_data)
            ca.DivC_I(tdisc_spline[-1].adj_h, -cf.splinesconfig.stdNoiseDef*cf.splinesconfig.stdNoiseDef)  # negative gradient at the measurement
            cavmcommon.SplatVSafe(adj_h_part, tdisc_spline[-1].ginv, tdisc_spline[-1].adj_h)
        

    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1

    controls_counter = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        #if cf.optim.integMethod == "RK4":
        if True:
            # first compute all K1's
            #eval_rhs_adj_h_ext(k_adj_h, tdisc_spline[i+1].adj_h, tdisc_spline[i+1].v)
            #ca.MulC_I(k_adj_h, dt)  # K1_adj_h*dt computed
            #ca.Copy(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m_ext_geomatch(k_adj_m, scratch_v2,
                                        tdisc_spline[i+1].adj_m,
                                        tdisc_spline[i+1].v,
                                        tdisc_spline[i+1].m,
                                        tdisc_spline[i+1].adj_I,
                                        tdisc_spline[i+1].I,
                                        tdisc_spline[i+1].adj_h,
                                        tdisc_spline[i+1].ginv,
                                        diff_op)
            ca.MulC_I(k_adj_m, dt)  # K1_adj_m*dt computed
            ca.Copy(rk4_adj_m, k_adj_m)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, tdisc_spline[i+1].g)

            # compute and store primal g, ginv, v, m, f, I at t+1/2 for computing k2 and k3
            if i > 0:
                ca.SubMulC(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[i].g, 0.5)
                ca.Add(rk4_scratch.g, tdisc_spline[i].g, scratch_v1)
                ca.UpdateInverse(rk4_scratch.ginv, scratch_v2, tdisc_spline[i].ginv, scratch_v1, cf.optim.NIterForInverse)
            else:  # add 0.5 times identity
                ca.HtoV(rk4_scratch.g, tdisc_spline[i+1].g)
                ca.MulC_I(rk4_scratch.g, 0.5)
                # iteratively update inverse as, g_{1,0} = Id - w\circ g_{1,0}
                ca.SetToIdentity(rk4_scratch.ginv)
                for k in range(cf.optim.NIterForInverse):
                    ca.ApplyH(scratch_v2, rk4_scratch.g, rk4_scratch.ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                    ca.HtoV(rk4_scratch.ginv, scratch_v2)
                    ca.MulC_I(rk4_scratch.ginv, -1.0)
                ca.VtoH_I(rk4_scratch.g)
            ca.AddMulC(rk4_scratch.m, tdisc_spline[i+1].m, tdisc_spline[i].m, 0.5)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)

            # for K2
            # get adj_h at t+1/2#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.MulC_I(k_adj_h, 0.5)  # K1_adj_h*dt/2
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, rk4_scratch.g, adj_h_part)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K1_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)

            # now compute all K2's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, rk4_scratch.v)
            #ca.MulC_I(k_adj_h, dt)  # K2_adj_h*dt computed
            #ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m_ext_geomatch(k_adj_m, scratch_v2,
                                        rk4_scratch.adj_m,
                                        rk4_scratch.v, rk4_scratch.m,
                                        rk4_scratch.adj_I,
                                        rk4_scratch.I,
                                        rk4_scratch.adj_h,
                                        rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k2_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)

            # for K3
            # get adj_h at t+1/2#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.MulC_I(k_adj_h, 0.5)  # K1_adj_h*dt/2
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, rk4_scratch.g, adj_h_part)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K2_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)

            # get adj_I at t+1/2#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, rk4_scratch.g, rk4_scratch.adj_I_part)

            # now compute all K3's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, rk4_scratch.v)
            #ca.MulC_I(k_adj_h, dt)  # K3_adj_h*dt computed
            #ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m_ext_geomatch(k_adj_m, scratch_v2,
                                        rk4_scratch.adj_m,
                                        rk4_scratch.v, rk4_scratch.m,
                                        rk4_scratch.adj_I,
                                        rk4_scratch.I,
                                        rk4_scratch.adj_h,
                                        rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k3_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)

            # for K4
            # get adj_h at t+1#
            #ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            #ca.Sub_I(rk4_scratch.adj_h, k_adj_h)
            cavmcommon.SplatVSafe(rk4_scratch.adj_h, tdisc_spline[i].g, adj_h_part)
            # get adj_m and adj_v at t+1#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_I at t+1#
            ca.Copy(rk4_scratch.adj_I_part, tdisc_spline[i+1].adj_I_part)
            cavmcommon.SplatSafe(rk4_scratch.adj_I, tdisc_spline[i].g, rk4_scratch.adj_I_part)

            # now compute all K4's
            #eval_rhs_adj_h_ext(k_adj_h, rk4_scratch.adj_h, tdisc_spline[i].v)
            #ca.MulC_I(k_adj_h, dt)  # K4_adj_h*dt computed
            #ca.Add_I(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m_ext_geomatch(k_adj_m, scratch_v2,
                                        rk4_scratch.adj_m,
                                        tdisc_spline[i].v,
                                        tdisc_spline[i].m,
                                        rk4_scratch.adj_I,
                                        tdisc_spline[i].I,
                                        rk4_scratch.adj_h,
                                        tdisc_spline[i].ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k4_adj_m*dt computed
            ca.Add_I(rk4_adj_m, k_adj_m)

            # for final update
            #ca.MulC_I(rk4_adj_h, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_m, 1.0/float(6.0))

            # now take the final update step
            #ca.Sub(tdisc_spline[i].adj_h, tdisc_spline[i+1].adj_h, rk4_adj_h)  # updated adj_h
            cavmcommon.SplatVSafe(tdisc_spline[i].adj_h, tdisc_spline[i].g, adj_h_part)  # updated adj_h
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, rk4_adj_m)  # updated adj_m       
            ca.Copy(tdisc_spline[i].adj_I_part, tdisc_spline[i+1].adj_I_part)  # updated adj_I_part
            cavmcommon.SplatSafe(tdisc_spline[i].adj_I, tdisc_spline[i].g, tdisc_spline[i].adj_I_part)  # updated adj_I

        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0

    if cf.optim.regularize_m0:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
        ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
    else:
        ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient

    ca.MulC(out_gradient.grad_I, tdisc_spline[0].adj_I, -1.0)  # copy -ve into out_gradient

    return out_gradient
