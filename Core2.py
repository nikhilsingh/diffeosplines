#!/usr/bin/python2
# pyca modules
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
import Libraries.CAvmCommon as cavmcommon
import logging
import numpy as np
import matplotlib.pyplot as plt
import os
import math

from scipy.optimize import approx_fprime
from scipy.optimize import check_grad
from ad import gh, adnumber

class SplineState:
    def __init__(self, grid, mem_type, t, is_control=False, has_data=False, J=None, Jid=None, def_data=None):
        self.grid = grid
        self.mem_type = mem_type
        self.t = t
        self.is_control = is_control
        self.J = None
        self.Jid = None
        self.has_data = False
        self.def_data = None

        # states at time t
        self.ginv = ca.ManagedField3D(grid, mem_type) # denoted as h in notes
        self.g = ca.ManagedField3D(grid, mem_type)
        self.m = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.m, 0.0)
        self.u = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.u, 0.0)
        self.p = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.p, 0.0)
        self.I = ca.ManagedImage3D(grid, mem_type)
        ca.SetMem(self.I, 0.0)

        # adjoint states at time t
        self.adj_h = ca.ManagedField3D(grid, mem_type) # adjoint for ginv
        ca.SetMem(self.adj_h, 0.0)
        self.adj_m = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_m, 0.0)
        self.adj_u = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_u, 0.0)
        self.adj_p = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_p, 0.0)

        # if we have measurement data here
        if J is not None:
            #self.J = ca.Image3D(grid, mem_type)
            #ca.SetMem(self.J, 0.0)
            self.J = J
            self.Jid = Jid
            self.has_data = True
            self.def_data = def_data
            # unique id (name) for writing output
            if Jid is not None: # this should always evaluate to true if Jt is not None
                 self.Jid = Jid
        else:
            self.J = None
            self.Jid = None
            self.has_data = False
            self.def_data = None

        # other state vars for convenience
        self.v = ca.ManagedField3D(grid, mem_type)  # dual of m, v\in \mathfrak{g}
        ca.SetMem(self.v, 0.0)
        self.f = ca.ManagedField3D(grid, mem_type)  # dual of u, f\in \mathfrak{g}
        ca.SetMem(self.f, 0.0)
        self.adj_v = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_v, 0.0)
        self.adj_f = ca.ManagedField3D(grid, mem_type)
        ca.SetMem(self.adj_f, 0.0)


class SplineGradient:
    def __init__(self, grid, mem_type, num_controls=0):
        self.grid = grid
        self.mem_type = mem_type
        self.grad_m = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_m, 0.0)
        self.grad_u = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_u, 0.0)
        self.grad_p = ca.Field3D(grid, mem_type)
        ca.SetMem(self.grad_p, 0.0)
        self.num_controls = num_controls
        self.grad_controls = [ca.Field3D(grid, mem_type) for i in range(num_controls)]
        for i in range(num_controls):
            ca.SetMem(self.grad_controls[i], 0.0)

def serialize_into_grad_x(out_grad_x, spline_gradient, buffer_v=None):
    mem_type = spline_gradient.mem_type
    grid = spline_gradient.grid
    num_controls = spline_gradient.num_controls

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('Serialization begin with start counter: %d/%d', counter, len(out_grad_x))
        np_grad_m_x, np_grad_m_y, np_grad_m_z = spline_gradient.grad_m.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_z)
        counter += 1

        np_grad_u_x, np_grad_u_y, np_grad_u_z = spline_gradient.grad_u.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_z)
        counter += 1

        np_grad_p_x, np_grad_p_y, np_grad_p_z = spline_gradient.grad_p.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_z)
        counter += 1

        for i in range(num_controls):
            np_grad_pt_x, np_grad_pt_y, np_grad_pt_z = spline_gradient.grad_controls[i].asnp()
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_x)
            counter += 1
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_y)
            counter += 1
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_z)
            counter += 1

        logging.debug('Serialization done with end counter: %d/%d', counter*grid.nVox(), len(out_grad_x))

    else:
        if buffer_v is None:
            buffer_v = ca.ManagedField3D(grid, ca.MEM_HOST)

        counter = 0
        logging.debug('Serialization begin with start counter: %d/%d', counter, len(out_grad_x))
        ca.Copy(buffer_v, spline_gradient.grad_m)
        np_grad_m_x, np_grad_m_y, np_grad_m_z = buffer_v.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_m_z)
        counter += 1

        ca.Copy(buffer_v, spline_gradient.grad_u)
        np_grad_u_x, np_grad_u_y, np_grad_u_z = buffer_v.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_u_z)
        counter += 1

        ca.Copy(buffer_v, spline_gradient.grad_p)
        np_grad_p_x, np_grad_p_y, np_grad_p_z = buffer_v.asnp()
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_x)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_y)
        counter += 1
        out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_p_z)
        counter += 1

        for i in range(num_controls):
            ca.Copy(buffer_v, spline_gradient.grad_controls[i])
            np_grad_pt_x, np_grad_pt_y, np_grad_pt_z = buffer_v.asnp()
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_x)
            counter += 1
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_y)
            counter += 1
            out_grad_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_grad_pt_z)
            counter += 1

        logging.debug('Serialization done with end counter: %d/%d', counter*grid.nVox(), len(out_grad_x))

        
def unserialize_into_spline_gradient(out_spline_gradient, grad_x):
    mem_type = out_spline_gradient.mem_type
    grid = out_spline_gradient.grid

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(grad_x))
        # get references to numpy arrays for pyca types
        np_m_x, np_m_y, np_m_z = out_spline_gradient.grad_m.asnp()
        np_m_x[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_y[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_z[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1

        np_u_x, np_u_y, np_u_z = out_spline_gradient.grad_u.asnp()
        np_u_x[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_y[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_z[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1

        np_p_x, np_p_y, np_p_z = out_spline_gradient.grad_p.asnp()
        np_p_x[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_y[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_z[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        for i in range(out_spline_gradient.num_controls):
            np_pt_x, np_pt_y, np_pt_z = out_spline_gradient.grad_controls[i].asnp()
            
            np_pt_x[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                    [grid.size().x, grid.size().y, grid.size().z], order='F')
            counter += 1
            np_pt_y[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                    [grid.size().x, grid.size().y, grid.size().z], order='F')
            counter += 1
            np_pt_z[:] = np.reshape(grad_x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                    [grid.size().x, grid.size().y, grid.size().z], order='F')
            counter += 1            
        logging.debug('UnSerialization done with end counter: %d/%d', counter*grid.nVox(), len(grad_x))

    else:
        raise Exception("Unserialization into spline_gradient is not implemented for GPU!")

def unserialize_into_tdisc_spline(out_tdisc_spline, x, buffer_v=None):
    mem_type = out_tdisc_spline[0].mem_type
    grid = out_tdisc_spline[0].grid

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(x))
        # get references to numpy arrays for pyca types
        np_m_x, np_m_y, np_m_z = out_tdisc_spline[0].m.asnp()
        np_m_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1

        np_u_x, np_u_y, np_u_z = out_tdisc_spline[0].u.asnp()
        np_u_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1

        np_p_x, np_p_y, np_p_z = out_tdisc_spline[0].p.asnp()
        np_p_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        controls_counter = 0
        for i in range(len(out_tdisc_spline)):
            if out_tdisc_spline[i].is_control:
                np_pt_x, np_pt_y, np_pt_z = out_tdisc_spline[i].p.asnp()

                np_pt_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                       [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                np_pt_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                       [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                np_pt_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                       [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                controls_counter += 1

        logging.debug('UnSerialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(),
                      len(x), controls_counter)
    else:
        if buffer_v is None:
            buffer_v = ca.ManagedField3D(grid, ca.MEM_HOST)
        counter = 0
        logging.debug('UnSerialization started with start counter: %d/%d', counter, len(x))

        # get references to numpy arrays for pyca types
        np_m_x, np_m_y, np_m_z = buffer_v.asnp()
        np_m_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_m_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        ca.Copy(out_tdisc_spline[0].m, buffer_v)

        np_u_x, np_u_y, np_u_z = buffer_v.asnp()
        np_u_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_u_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        ca.Copy(out_tdisc_spline[0].u, buffer_v)

        np_p_x, np_p_y, np_p_z = buffer_v.asnp()
        np_p_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        np_p_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                               [grid.size().x, grid.size().y, grid.size().z], order='F')
        counter += 1
        ca.Copy(out_tdisc_spline[0].p, buffer_v)

        controls_counter = 0
        for i in range(len(out_tdisc_spline)):
            if out_tdisc_spline[i].is_control:
                np_pt_x, np_pt_y, np_pt_z = buffer_v.asnp()
                np_pt_x[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                        [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                np_pt_y[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                        [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                np_pt_z[:] = np.reshape(x[counter*grid.nVox():(counter+1)*grid.nVox()],
                                        [grid.size().x, grid.size().y, grid.size().z], order='F')
                counter += 1
                ca.Copy(out_tdisc_spline[i].p, buffer_v)
                controls_counter += 1

        logging.debug('UnSerialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(),
                      len(x), controls_counter)

def serialize_into_x(out_x, tdisc_spline, buffer_v=None):
    mem_type = tdisc_spline[0].mem_type
    grid = tdisc_spline[0].grid

    if mem_type == ca.MEM_HOST:
        counter = 0
        logging.debug('Serialization begin with start counter: %d/%d', counter, len(out_x))
        np_m_x, np_m_y, np_m_z = tdisc_spline[0].m.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_z)
        counter += 1

        np_u_x, np_u_y, np_u_z = tdisc_spline[0].u.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_z)
        counter += 1

        np_p_x, np_p_y, np_p_z = tdisc_spline[0].p.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_z)
        counter += 1
        
        controls_counter = 0

        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].is_control:
                np_pt_x, np_pt_y, np_pt_z = tdisc_spline[i].p.asnp()
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_x)
                counter += 1
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_y)
                counter += 1
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_z)
                counter += 1
                controls_counter += 1
        logging.debug('Serialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(), len(out_x), controls_counter)

    else:
        if buffer_v is None:
            buffer_v = ca.ManagedField3D(grid, ca.MEM_HOST)

        counter = 0
        logging.debug('Serialization begin with start counter: %d/%d', counter, len(out_x))
        ca.Copy(buffer_v, tdisc_spline[0].m)
        np_m_x, np_m_y, np_m_z = buffer_v.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_m_z)
        counter += 1

        ca.Copy(buffer_v, tdisc_spline[0].u)
        np_u_x, np_u_y, np_u_z = buffer_v.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_u_z)
        counter += 1

        ca.Copy(buffer_v, tdisc_spline[0].p)
        np_p_x, np_p_y, np_p_z = buffer_v.asnp()
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_x)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_y)
        counter += 1
        out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_p_z)
        counter += 1

        controls_counter = 0

        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].is_control:
                ca.Copy(buffer_v, tdisc_spline[i].p)
                np_pt_x, np_pt_y, np_pt_z = buffer_v.asnp()
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_x)
                counter += 1
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_y)
                counter += 1
                out_x[counter*grid.nVox():(counter+1)*grid.nVox()] = np.ravel(np_pt_z)
                counter += 1
        
        logging.debug('Serialization done with end counter: %d/%d and total controls: %d', counter*grid.nVox(), len(out_x), controls_counter)

def CheckNaN(l, iter=None):
    if type(l) is list:
        x = sum(map(ca.Sum, l))
    else:
        x = ca.Sum(l)
    if math.isinf(x):
        if iter is not None:
            print "Exception in iter: ", iter
        raise Exception("\aInf detected")
    if x != x:
        if iter is not None:
            print "Exception in iter: ", iter
        raise Exception("\aNaN detected")

def setup_time_discretization(time_descretized_array, image_data, image_ids, image_indices, control_indices, def_data=None):
    """
    Set up an list of states at time descritizations for the spline curve (list of SplineStates)
    """
    grid = image_data[0].grid()
    mem_type = image_data[0].memType()
    td = []
    for i in xrange(len(time_descretized_array)):
        if (i in image_indices) and (i in control_indices):          
            if def_data is not None:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], True, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)], def_data[image_indices.index(i)]))
            else :
                td.append(SplineState(grid, mem_type, time_descretized_array[i], True, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)]))
            
        elif i in image_indices:
            if def_data is not None:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], False, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)], def_data[image_indices.index(i)]))
            else:
                td.append(SplineState(grid, mem_type, time_descretized_array[i], False, True, image_data[image_indices.index(i)],
                                      image_ids[image_indices.index(i)]))

        elif i in control_indices:
            td.append(SplineState(grid, mem_type, time_descretized_array[i], True, False, None, None, None))
        else:
            td.append(SplineState(grid, mem_type, time_descretized_array[i], False, False, None, None, None))
    return td


def setup_timearray(num_timesteps, measurement_times_array, control_times_array, epsilon):
    num_measurements = len(measurement_times_array)
    num_controls = len(control_times_array)
    temp_t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    measurement_indices = [0]*num_measurements
    control_indices = [0]*num_controls

    # first create the time array and add timepoints for measurements
    for i in range(num_measurements):
        found_sub = False
        for j in range(len(temp_t)):
            if (measurement_times_array[i] < (temp_t[j]+epsilon)) and (measurement_times_array[i] > (temp_t[j]-epsilon)):
                found_sub = True
                break

        if not found_sub:  # need to insert a timepoint for this subject
            t.append(measurement_times_array[i])
            t.sort()

    temp_t = t
    # next add timepoints for controls
    for i in range(num_controls):
        found_control = False
        for j in range(len(temp_t)):
            if (control_times_array[i] < (temp_t[j]+epsilon)) and (control_times_array[i] > (temp_t[j]-epsilon)):
                found_control = True
                break

        if not found_control:  # need to insert a timepoint for this control
            t.append(control_times_array[i])
            t.sort()

    # now create the subject index array
    for i in range(num_measurements):
        for j in range(len(t)):
            if (measurement_times_array[i] < (t[j]+epsilon)) and (measurement_times_array[i] > (t[j]-epsilon)):
                measurement_indices[i] = j
                break

    # now create the control index array
    for i in range(num_controls):
        for j in range(len(t)):
            if (control_times_array[i] < (t[j]+epsilon)) and (control_times_array[i] > (t[j]-epsilon)):
                control_indices[i] = j
                break

    return t, measurement_indices, control_indices


def setup_diff_oper(alpha, beta, gamma, grid, mem_type):
    # differential operator (size same as a Field3D)
    diff_oper = None
    if mem_type == ca.MEM_HOST:
        diff_oper = ca.FluidKernelFFTCPU()
    else:
        diff_oper = ca.FluidKernelFFTGPU()
    diff_oper.setAlpha(alpha)
    diff_oper.setBeta(beta)
    diff_oper.setGamma(gamma)
    diff_oper.setGrid(grid)

    return diff_oper


def eval_rhs_g(out_v, v, g):
    """
    Evaluate RHS for forward integration of \dot{g}=v\circ g
    """
    ca.ApplyH(out_v, v, g, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)

    return out_v


def eval_rhs_m(out_m, scratch_v, v, m, u):
    ca.Copy(out_m, u)
    ca.CoAdInf(scratch_v, v, m)
    ca.Sub_I(out_m, scratch_v)
    return out_m


def eval_rhs_f(out_f, scratch_v, p, h, f, m, v, diff_op):
    ca.JacobianXtY(scratch_v, h, p)
    ca.CoAdInf(out_f, f, m)
    ca.Sub_I(scratch_v, out_f)
    diff_op.applyInverseOperator(scratch_v)
    ca.AdInf(out_f, v, f)
    ca.Add_I(out_f, scratch_v)

    return out_f


def eval_rhs_adj_m(out_v, scratch_v, adj_m, v, m, f, adj_f, adj_u,
                   adj_p, p, adj_h, h, diff_op):   
    ca.CoAdInf(out_v, f, adj_f)

    ca.CoAdInf(scratch_v, adj_m, m)
    ca.Sub_I(out_v, scratch_v)

    ca.JacobianXtY(scratch_v, h, adj_h)
    ca.Add_I(out_v, scratch_v)

    ca.JacobianXtY(scratch_v, adj_p, p)
    ca.Sub_I(out_v, scratch_v)

    diff_op.applyInverseOperator(out_v)

    ca.AdInf(scratch_v, adj_m, v)
    ca.Sub_I(out_v, scratch_v)

    ca.AdInf(scratch_v, f, adj_u)
    ca.Add_I(out_v, scratch_v)

    return out_v

def eval_rhs_adj_h(out_v, scratch_v, p, adj_u, adj_h, v):
    ca.DivergenceTensor(scratch_v, adj_h, v)
    ca.DivergenceTensor(out_v, p, adj_u)
    ca.Sub_I(out_v, scratch_v)

def eval_rhs_adj_f(out_m, scratch_v, u, adj_v, adj_u, m, v, adj_f, cf):
    ca.CoAdInf(out_m, adj_u, m)
    ca.Add_I(out_m, adj_v)
    ca.CoAdInf(scratch_v, v, adj_f)
    ca.Add_I(out_m, scratch_v)
    if cf.optim.regularize_ps:
        ca.Add_MulC_I(out_m, u, -cf.optim.r_u)
    ca.MulC_I(out_m, -1.0)
    return out_m

def eval_rhs_adj_p(out_v, scratch_v, h, adj_u, adj_p, v):
    ca.JacobianXY(scratch_v, h, adj_u)
    ca.JacobianXY(out_v, adj_p, v)
    ca.Add_I(out_v, scratch_v)
    ca.MulC_I(out_v, -1.0)
    return out_v

def create_bounds_x_np(num_controls, num_vox, cf, bounds=[(None,None),(None,None),(None,None),(None,None)]):
    length_x = 3*(3+num_controls)*num_vox
    bounds_x = [(None, None) for i in range(length_x)]

    counter = 0
    for i in range(counter*num_vox, (counter+3)*num_vox): bounds_x[i] = bounds[0]
    counter += 3
    for i in range(counter*num_vox, (counter+3)*num_vox): bounds_x[i] = bounds[1]
    counter += 3
    for i in range(counter*num_vox, (counter+3)*num_vox): bounds_x[i] = bounds[2]

    if num_controls > 0:
        counter += 3
        for i in range(counter*num_vox, (counter+3)*num_vox): bounds_x[i] = bounds[3]        

    return bounds_x

def create_eps_x_np(num_controls, num_vox, cf):
    length_x = 3*(3+num_controls)*num_vox
    eps_x = np.zeros(length_x, dtype='float')
    counter = 0
    eps_x[counter*num_vox:(counter+3)*num_vox] = cf.optim.stepSizem0
    counter += 3
    eps_x[counter*num_vox:(counter+3)*num_vox] = cf.optim.stepSizeu0
    counter += 3
    eps_x[counter*num_vox:(counter+3)*num_vox] = cf.optim.stepSizep0

    if num_controls > 0:
        counter += 3
        eps_x[counter*num_vox:-1] = cf.optim.stepSizepc

    return eps_x

def compute_energy_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf):
    total_energy = 0.0

    # copy from numpy x (host memory) into tdisc_spline
    unserialize_into_tdisc_spline(tdisc_spline, x)

    # calculate energy at x since now x is copied to pyca type datatype in tdisc_spline
    total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1 = compute_energy(cf, tdisc_spline, diff_oper)

    # return energy value
    return total_energy

def compute_gradient_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf):
    # copy from numpy x (host memory) into tdisc_spline
    unserialize_into_tdisc_spline(tdisc_spline, x)
    
    # calculate gradient at x since now x is copied to pyca type datatype
    compute_gradient(spline_gradient, cf, tdisc_spline, diff_oper)
    
    # convert the gradient value in spline_gradient to numpy array to return
    serialize_into_grad_x(grad_x, spline_gradient)
    
    # return the gradient as a numpy array
    return grad_x

'''
def compute_gradient_np_fdapprox(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf, delta_x=None):
    #grad_x = (f(*((xk + d,) + args)) - f0) / d[k]
    f0 = compute_energy_np(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    if delta_x == None:
        delta_x = 0.0001*x
    f1 = compute_energy_np(x+delta_x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    
    old_err_state = np.seterr(divide='raise')    
    mask = abs(delta_x)>0.0000000001
    if sum(mask)>0:
        print sum(mask)
        (f1-f0)/delta_x[mask]
    
    return grad_x
'''

def compute_gradient_np_fdapprox(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf, eps=0.000001):    
    grad_x[:] =  approx_fprime(x, compute_energy_np, eps, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    unserialize_into_spline_gradient(spline_gradient, grad_x)
    return grad_x


def compute_gradient_np_ad(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf):
    my_grad, my_hessian = gh(compute_energy_np)
    grad_x[:] = my_grad(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    unserialize_into_spline_gradient(spline_gradient, grad_x)
    return grad_x

def check_gradient(x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf):
    grad_error = check_grad(compute_energy_np, compute_gradient_np, x, tdisc_spline, grad_x, spline_gradient, diff_oper, cf)
    return grad_error

def compute_gradient(out_gradient, cf, tdisc_spline, diff_op):
    if cf.optim.integMethod == "EULER":
        raise Exception('EULER not implemented!')

    elif cf.optim.integMethod == "RK4":
        # shoot forward
        shoot_forward_rk4_direct(cf, tdisc_spline, diff_op)
        # integrate backward
        integrate_backward_rk4_direct(out_gradient, cf, tdisc_spline, diff_op)  
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)

    return out_gradient


def compute_energy(cf, tdisc_spline, diff_op):
    total_energy = 0.0
    regularizer_p0 = 0.0
    regularizer_m0 = 0.0
    regularizer_u = 0.0
    image_match = 0.0
    def_match = 0.0
    regularizer_h1 = 0.0
    
    if cf.optim.integMethod == "EULER":
        raise Exception('EULER not implemented!')
    elif cf.optim.integMethod == "RK4":
        shoot_forward_rk4_direct(cf, tdisc_spline, diff_op)
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)

    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()
    # scratch variables
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)

    if cf.study.regressImages:   
        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].has_data:
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                image_match += 0.5*ca.Sum2(scratch_I1)/(cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise)

    if cf.study.regressDefFields:        
        for i in range(len(tdisc_spline)):
            if tdisc_spline[i].has_data:
                ca.Sub(scratch_v1, tdisc_spline[i].ginv, tdisc_spline[i].def_data)
                def_match += 0.5*ca.Sum2(scratch_v1)/(cf.splinesconfig.stdNoiseDef*cf.splinesconfig.stdNoiseDef)

    # regularizers
    if cf.optim.regularize_p0:
        ca.Copy(scratch_v1, tdisc_spline[0].p)
        diff_op.applyInverseOperator(scratch_v1)
        regularizer_p0 = 0.5*cf.optim.r_p0*ca.Dot(tdisc_spline[0].p, scratch_v1)

    if cf.optim.regularize_ps:
        for j in range(len(tdisc_spline)):
            if tdisc_spline[j].is_control:
                ca.Copy(scratch_v1, tdisc_spline[j].p)
                diff_op.applyInverseOperator(scratch_v1)
                regularizer_p0 += 0.5*cf.optim.r_ps*ca.Dot(tdisc_spline[j].p, scratch_v1)
        
    if cf.optim.regularize_m0:
        regularizer_m0 = 0.5*cf.optim.r_m0*ca.Dot(tdisc_spline[0].m, tdisc_spline[0].v)

    if cf.optim.regularize_h1:        
        ca.Copy(scratch_v1, tdisc_spline[-1].ginv)
        ca.HtoV_I(scratch_v1)
        ca.ComponentDotProd(scratch_I1, scratch_v1, scratch_v1)
        ca.Sqrt(scratch_I2, scratch_I1)
        ca.GTC_I(scratch_I2, cf.optim.a_h1)
        ca.SubC_I(scratch_I1, cf.optim.a_h1**2)
        ca.Cube_I(scratch_I1)
        ca.Mul_I(scratch_I1, scratch_I2) # apply mask
        regularizer_h1 = cf.optim.r_h1*ca.Sum(scratch_I1)
        
    #this is an integral from zero to 1    
    if cf.optim.regularize_u:        
        for i in range(1, len(tdisc_spline), 1):
            dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step
            regularizer_u += 0.5*cf.optim.r_u*ca.Dot(tdisc_spline[i].u, tdisc_spline[i].f)*dt

            
    total_energy = regularizer_p0+regularizer_m0+regularizer_u+regularizer_h1+image_match+def_match
    return total_energy, image_match, def_match, regularizer_m0, regularizer_u, regularizer_p0, regularizer_h1


def shoot_forward_rk4_direct(cf, tdisc_spline, diff_op, rk4_scratch=None, start_index=0, end_index=0):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()

    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    rk4_g = ca.ManagedField3D(grid, mem_type)
    rk4_m = ca.ManagedField3D(grid, mem_type)
    rk4_f = ca.ManagedField3D(grid, mem_type)
    k_g = ca.ManagedField3D(grid, mem_type)
    k_m = ca.ManagedField3D(grid, mem_type)
    k_f = ca.ManagedField3D(grid, mem_type)

    # initial conditions
    if end_index == 0:
        end_index = len(tdisc_spline)-1

    if start_index == 0:
        # initializations
        ca.SetToIdentity(tdisc_spline[0].g)
        ca.SetToIdentity(tdisc_spline[0].ginv)
        ca.Copy(tdisc_spline[0].v, tdisc_spline[0].m)
        diff_op.applyInverseOperator(tdisc_spline[0].v)
        ca.Copy(tdisc_spline[0].f, tdisc_spline[0].u)
        diff_op.applyInverseOperator(tdisc_spline[0].f)
        index_last_control = 0

    # do integration
    for i in range(start_index+1, end_index+1, 1):
        dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step

        eval_rhs_g(k_g, tdisc_spline[i-1].v, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, dt)  # K1_g*dt computed
        ca.Copy(rk4_g, k_g)
        eval_rhs_m(k_m, scratch_v1, tdisc_spline[i-1].v, tdisc_spline[i-1].m, tdisc_spline[i-1].u)
        ca.MulC_I(k_m, dt)  # K1_m*dt computed
        ca.Copy(rk4_m, k_m)

        eval_rhs_f(k_f, scratch_v1, tdisc_spline[i-1].p, tdisc_spline[i-1].ginv, tdisc_spline[i-1].f,
                   tdisc_spline[i-1].m, tdisc_spline[i-1].v, diff_op)
        ca.MulC_I(k_f, dt)  # K1_f*dt computed
        ca.Copy(rk4_f, k_f)

        # for K2
        # get g and ginv at t+1/2#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, 0.5)  # K1_g*dt/2
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1/2#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.MulC_I(k_m, 0.5)  # K1_m*dt/2
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1/2#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.MulC_I(k_f, 0.5)  # K1_f*dt/2
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1/2#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K2's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k2_g*dt computed
        ca.Add_MulC_I(rk4_g, k_g, 2.0)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        #eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K2_m_part*dt computed
        ca.Add_MulC_I(rk4_m, k_m, 2.0)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K2_f_part*dt computed
        ca.Add_MulC_I(rk4_f, k_f, 2.0)

        # for K3
        # get g and ginv at t+1/2#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, 0.5)  # K2_g*dt/2
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1/2#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.MulC_I(k_m, 0.5)  # K2_m_part*dt/2
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1/2#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.MulC_I(k_f, 0.5)  # K2_f_part*dt/2
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1/2#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K3's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k3_g*dt computed
        ca.Add_MulC_I(rk4_g, k_g, 2.0)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K3_m_part*dt computed
        ca.Add_MulC_I(rk4_m, k_m, 2.0)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K3_f_part*dt computed
        ca.Add_MulC_I(rk4_f, k_f, 2.0)

        # for K4
        # get g and ginv at t+1#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K4's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k4_g*dt computed
        ca.Add_I(rk4_g, k_g)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K4_m_part*dt computed
        ca.Add_I(rk4_m, k_m)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K4_f_part*dt computed
        ca.Add_I(rk4_f, k_f)

        # for final update
        ca.MulC_I(rk4_g, 1.0/float(6.0))
        ca.MulC_I(rk4_m, 1.0/float(6.0))
        ca.MulC_I(rk4_f, 1.0/float(6.0))

        # now take the final update step
        ca.Add(tdisc_spline[i].g, tdisc_spline[i-1].g, rk4_g)  # updated g
        ca.UpdateInverse(tdisc_spline[i].ginv, scratch_v1, tdisc_spline[i-1].ginv, rk4_g, cf.optim.NIterForInverse)  # updated ginv
        ca.Add(tdisc_spline[i].m, tdisc_spline[i-1].m, rk4_m)  # final updated m
        ca.Add(tdisc_spline[i].f, tdisc_spline[i-1].f, rk4_f)  # final updated f
        ca.ApplyH(tdisc_spline[i].I, tdisc_spline[0].I, tdisc_spline[i].ginv)  # final updated I

        if not tdisc_spline[i].is_control:
            # input previous P[0], this g
            # get g_{last_control, this}
            ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatVSafe(tdisc_spline[i].p, scratch_v1, tdisc_spline[index_last_control].p)  # final update P
        else:
                index_last_control = i  # for subsequent iterations

        # now update other duals, v and u, required for the next integration step
        ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
        diff_op.applyInverseOperator(tdisc_spline[i].v)
        ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
        diff_op.applyOperator(tdisc_spline[i].u)

def plot_vf_2d(v, name, vmin=None, vmax=None, plot_vector=False):
    if v.memType() == ca.MEM_DEVICE:
        vh = v.copy()
        vh.toType(ca.MEM_HOST)
        vArr_x, vArr_y, vArr_z = vh.asnp()
    else:
        vArr_x, vArr_y, vArr_z = v.asnp()
    vArr_x = np.squeeze(vArr_x)
    vArr_y = np.squeeze(vArr_y)

    #title = name
    #plt.figure(title)
    if plot_vector:
        plt.subplot(2, 2, 1)
        plt.imshow(vArr_x, vmin=vmin, vmax=vmax)
        plt.colorbar()
        plt.title('component x')
        plt.draw()
        plt.subplot(2, 2, 2)
        #if vmin is not None: vmin=vmin/20
        #if vmax is not None: vmax=vmax/20
        plt.imshow(vArr_y,vmin=vmin, vmax=vmax)
        plt.colorbar()
        plt.title('component y')
        plt.draw()    
        # change axes to match image axes 
        #plt.gca().invert_yaxis()
        plt.subplot(2, 2, 3)
        plt.imshow(np.sqrt(np.square(vArr_y)+np.square(vArr_x)),vmin=vmin, vmax=vmax)
        plt.colorbar()
        plt.title('norm v')
        plt.draw()    
        plt.subplot(2, 2, 4)
        if vmin is not None: vmin=vmin/20
        if vmax is not None: vmax=vmax/20
        gridX, gridY = np.meshgrid(range(np.shape(vArr_x)[0]),range(np.shape(vArr_x)[1]))
        if vmax is not None: vmax=vmax*200
        plt.quiver(gridX, gridY, vArr_y, vArr_x, scale=vmax, color='r', linewidth=0.5, width=0.005, zorder=4)
        plt.title('vector field')
        plt.draw()
    else:
        plt.subplot(1, 2, 1)
        plt.imshow(vArr_x, vmin=vmin, vmax=vmax)
        plt.colorbar()
        plt.title('component x')
        plt.draw()
        plt.subplot(1, 2, 2)
        #if vmin is not None: vmin=vmin/20
        #if vmax is not None: vmax=vmax/20
        plt.imshow(vArr_y,vmin=vmin, vmax=vmax)
        plt.colorbar()
        plt.title('component y')
        plt.draw()    
    #plt.show(block=False)        
    return plt
    #plt.show(block=False)
    #plt.show(block="False")

def plot_im_2d(I, name):
    if I.memType() == ca.MEM_DEVICE:
        Ih = I.copy()
        Ih.toType(ca.MEM_HOST)
        IArr = Ih.asnp()
    else:
        IArr = I.asnp()
    IArr = np.squeeze(IArr)
    #title = name
    #plt.figure(title)
    plt.imshow(IArr)
    plt.colorbar()
    plt.draw()
    plt.show(block=False)
    return plt
    #plt.show(block="False")

'''
def CheckField(hF, hG, name,disp):
    hGArr_x, hGArr_y, hGArr_z = hG.asnp()
    hGArr_x = np.squeeze(hGArr_x)
    hGArr_y = np.squeeze(hGArr_y)
    hFArr_x, hFArr_y, hFArr_z = hF.asnp()
    hFArr_x = np.squeeze(hFArr_x)
    hFArr_y = np.squeeze(hFArr_y)
    diff_x = hFArr_x-hGArr_x
    diff_y = hFArr_y-hGArr_y
    if disp:
        title = name
        plt.figure(title)

        plt.subplot(2,3,1)
        plt.imshow(np.squeeze(hFArr_x))
        plt.colorbar();
        plt.title('host x')
        plt.draw()
        plt.subplot(2,3,2)
        plt.imshow(np.squeeze(hGArr_x))
        plt.colorbar();
        plt.title('device x')
        plt.draw()
        plt.subplot(2,3,3)
        plt.imshow(np.squeeze(diff_x))
        plt.colorbar();
        plt.title('diff x')
        plt.draw()

        plt.subplot(2,3,4)
        plt.imshow(np.squeeze(hFArr_y))
        plt.colorbar();
        plt.title('host y')
        plt.draw()
        plt.subplot(2,3,5)
        plt.imshow(np.squeeze(hGArr_y))
        plt.colorbar();
        plt.title('device y')
        plt.draw()
        plt.subplot(2,3,6)
        plt.imshow(np.squeeze(diff_y))
        plt.colorbar();
        plt.title('diff y')
        plt.draw()
        plt.show()
    diffMax = max(np.max(np.abs(diff_x)), np.max(np.abs(diff_y)))
    diffAv = (np.sum(np.abs(diff_x)) + np.sum(np.abs(diff_y))) \
        / (2*np.prod(diff_x.shape))
    return (diffAv, diffMax)
'''

def compute_grad_measurements(out_v, scratch_I, I, J, cf, ginv=None, scratch_v=None):
    
    ca.Sub(scratch_I, I, J)
    ca.Gradient(out_v, I)    
    ca.MulMulC_I(out_v, scratch_I, 1.0/float(cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise))
    
    '''
    # 1. \nabla I(0)
    ca.Gradient(scratch_v, I)
    # 2. deform \nabla I(0)
    #nikhil: debug
    ca.ApplyH(out_v, scratch_v, ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
    #ca.Copy(out_v, scratch_v)
    # 3. compute difference image
    ca.Sub(scratch_I, I, J)
    # 4. multiply difference image with deformed grad I(0)
    ca.MulMulC_I(out_v, scratch_I,  1.0/float(cf.splinesconfig.stdNoise*cf.splinesconfig.stdNoise))
    '''
def integrate_backward_rk4_direct(out_gradient, cf, tdisc_spline, diff_op, rk4_scratch=None):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()

    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    scratch_v2 = ca.ManagedField3D(grid, mem_type)
    scratch_I1 = ca.ManagedImage3D(grid, mem_type)
    scratch_I2 = ca.ManagedImage3D(grid, mem_type)

    # scratch variables
    rk4_adj_h = ca.ManagedField3D(grid, mem_type)
    rk4_adj_m = ca.ManagedField3D(grid, mem_type)
    rk4_adj_f = ca.ManagedField3D(grid, mem_type)
    rk4_adj_p = ca.ManagedField3D(grid, mem_type)
    k_adj_h = ca.ManagedField3D(grid, mem_type)
    k_adj_m = ca.ManagedField3D(grid, mem_type)
    k_adj_f = ca.ManagedField3D(grid, mem_type)
    k_adj_p = ca.ManagedField3D(grid, mem_type)
    
    # initial conditions
    ca.SetMem(tdisc_spline[-1].adj_h, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_m, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_v, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_u, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_f, 0.0)
    ca.SetMem(tdisc_spline[-1].adj_p, 0.0)

    if tdisc_spline[-1].has_data:
        '''
        # compute gradients at measurements
        # 1. \nabla I(0)
        ca.Gradient(scratch_v1, tdisc_spline[0].I)
        # 2. deform \nabla I(0)
        ca.ApplyH(scratch_v2, scratch_v1, tdisc_spline[-1].ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
        # 3. compute difference image
        ca.Sub(scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J)
        # 4. multiply difference image with deformed grad I(0)
        ca.MulMulC_I(scratch_v2, scratch_I1, -1.0)

        ca.Copy(tdisc_spline[-1].adj_h, scratch_v2) # negative gradient
        '''
        #compute_grad_measurements(scratch_v1, scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J, cf)
        compute_grad_measurements(scratch_v1, scratch_I1, tdisc_spline[-1].I, tdisc_spline[-1].J, cf, ginv=tdisc_spline[-1].ginv, scratch_v=scratch_v2)        
        tdisc_spline[-1].adj_h = scratch_v1*-1.0

    if cf.optim.regularize_h1:
        ca.Copy(scratch_v1, tdisc_spline[-1].ginv)
        ca.HtoV_I(scratch_v1)
        ca.ComponentDotProd(scratch_I1, scratch_v1, scratch_v1)
        ca.Sqrt(scratch_I2, scratch_I1)
        ca.GTC_I(scratch_I2, cf.optim.a_h1)
        ca.SubC_I(scratch_I1, cf.optim.a_h1**2)
        ca.Sqr_I(scratch_I1)
        ca.Mul_I(scratch_I1, scratch_I2) # apply mask
        ca.MulMulC_I(scratch_v1, scratch_I1, 6.0*cf.optim.r_h1)
        ca.Sub_I(tdisc_spline[-1].adj_h, scratch_v1)
        
    all_control_indices = np.zeros(out_gradient.num_controls, dtype='int')

    controls_counter = 0
    for i in range(len(tdisc_spline)):
        if tdisc_spline[i].is_control:
            all_control_indices[controls_counter] = i
            controls_counter += 1
            
    controls_counter = out_gradient.num_controls-1

    # backward integrate
    for i in range(len(tdisc_spline)-2, -1, -1):
        dt = tdisc_spline[i].t - tdisc_spline[i+1].t
        if True:
            # first compute all K1's
            eval_rhs_adj_h(k_adj_h, scratch_v2, tdisc_spline[i+1].p, tdisc_spline[i+1].adj_u, tdisc_spline[i+1].adj_h, tdisc_spline[i+1].v)
            ca.MulC_I(k_adj_h, dt)  # K1_adj_h*dt computed
            ca.Copy(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m(k_adj_m, scratch_v2,
                           tdisc_spline[i+1].adj_m,
                           tdisc_spline[i+1].v, tdisc_spline[i+1].m,
                           tdisc_spline[i+1].f,
                           tdisc_spline[i+1].adj_f,
                           tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].adj_p,
                           tdisc_spline[i+1].p,
                           tdisc_spline[i+1].adj_h,
                           tdisc_spline[i+1].ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # K1_adj_m*dt computed
            ca.Copy(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, tdisc_spline[i+1].u, tdisc_spline[i+1].adj_v, tdisc_spline[i+1].adj_u,
                           tdisc_spline[i+1].m, tdisc_spline[i+1].v, tdisc_spline[i+1].adj_f, cf)
            ca.MulC_I(k_adj_f, dt)  # K1_adj_f*dt computed
            ca.Copy(rk4_adj_f, k_adj_f)
            eval_rhs_adj_p(k_adj_p, scratch_v2, tdisc_spline[i+1].ginv, tdisc_spline[i+1].adj_u, tdisc_spline[i+1].adj_p, tdisc_spline[i+1].v)
            ca.MulC_I(k_adj_p, dt)  # K1_adj_p*dt computed
            ca.Copy(rk4_adj_p, k_adj_p)

            # compute and store primal g, ginv, v, m, f, u at t+1/2 for computing k2 and k3
            if i > 0:
                ca.SubMulC(scratch_v1, tdisc_spline[i+1].g, tdisc_spline[i].g, 0.5)
                ca.Add(rk4_scratch.g, tdisc_spline[i].g, scratch_v1)
                ca.UpdateInverse(rk4_scratch.ginv, scratch_v2, tdisc_spline[i].ginv, scratch_v1, cf.optim.NIterForInverse)
            else:  # add 0.5 times identity
                ca.HtoV(rk4_scratch.g, tdisc_spline[i+1].g)
                ca.MulC_I(rk4_scratch.g, 0.5)
                # iteratively update inverse as, g_{1,0} = Id - w\circ g_{1,0}
                ca.SetToIdentity(rk4_scratch.ginv)
                for k in range(cf.optim.NIterForInverse):
                    ca.ApplyH(scratch_v2, rk4_scratch.g, rk4_scratch.ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                    ca.HtoV(rk4_scratch.ginv, scratch_v2)
                    ca.MulC_I(rk4_scratch.ginv, -1.0)
                ca.VtoH_I(rk4_scratch.g)
            ca.AddMulC(rk4_scratch.m, tdisc_spline[i+1].m, tdisc_spline[i].m, 0.5)
            ca.Copy(rk4_scratch.v, rk4_scratch.m)
            diff_op.applyInverseOperator(rk4_scratch.v)
            ca.AddMulC(rk4_scratch.f, tdisc_spline[i+1].f, tdisc_spline[i].f, 0.5)
            ca.Copy(rk4_scratch.u, rk4_scratch.f)
            diff_op.applyOperator(rk4_scratch.u)

            if controls_counter >= 0:
                ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[all_control_indices[controls_counter]].ginv)
                cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[all_control_indices[controls_counter]].p)
            else:
                ca.Copy(scratch_v1, rk4_scratch.g)
                cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[0].p)

            # for K2
            # get adj_h at t+1/2#
            ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            ca.MulC_I(k_adj_h, 0.5)  # K1_adj_h*dt/2
            ca.Add_I(rk4_scratch.adj_h, k_adj_h)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K1_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K1_adj_f*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_p at t+1/2#
            ca.Copy(rk4_scratch.adj_p, tdisc_spline[i+1].adj_p)
            ca.MulC_I(k_adj_p, 0.5)  # K1_adj_p*dt/2
            ca.Add_I(rk4_scratch.adj_p, k_adj_p)
            
            # now compute all K2's
            eval_rhs_adj_h(k_adj_h, scratch_v2, rk4_scratch.p, rk4_scratch.adj_u, rk4_scratch.adj_h, rk4_scratch.v)            
            ca.MulC_I(k_adj_h, dt)  # K2_adj_h*dt computed
            ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_p,
                           rk4_scratch.p, rk4_scratch.adj_h,
                           rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k2_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.u, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f, cf)
            ca.MulC_I(k_adj_f, dt)  # K2_adj_f*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            eval_rhs_adj_p(k_adj_p, scratch_v2, rk4_scratch.ginv, rk4_scratch.adj_u, rk4_scratch.adj_p, rk4_scratch.v)    
            ca.MulC_I(k_adj_p, dt)  # K2_adj_p*dt computed
            ca.Add_MulC_I(rk4_adj_p, k_adj_p, 2.0)

            # for K3
            # get adj_h at t+1/2#
            ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            ca.MulC_I(k_adj_h, 0.5)  # K2_adj_h*dt/2
            ca.Add_I(rk4_scratch.adj_h, k_adj_h)
            # get adj_m and adj_v at t+1/2#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.MulC_I(k_adj_m, 0.5)  # K2_adj_m*dt/2
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1/2#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.MulC_I(k_adj_f, 0.5)  # K2_adj_f*dt/2
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_p at t+1/2#
            ca.Copy(rk4_scratch.adj_p, tdisc_spline[i+1].adj_p)
            ca.MulC_I(k_adj_p, 0.5)  # K2_adj_p*dt/2
            ca.Add_I(rk4_scratch.adj_p, k_adj_p)

            # now compute all K3's
            eval_rhs_adj_h(k_adj_h, scratch_v2, rk4_scratch.p, rk4_scratch.adj_u, rk4_scratch.adj_h, rk4_scratch.v)
            ca.MulC_I(k_adj_h, dt)  # K3_adj_h*dt computed
            ca.Add_MulC_I(rk4_adj_h, k_adj_h, 2.0)
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           rk4_scratch.v, rk4_scratch.m,
                           rk4_scratch.f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_p,
                           rk4_scratch.p, rk4_scratch.adj_h,
                           rk4_scratch.ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k3_adj_m*dt computed
            ca.Add_MulC_I(rk4_adj_m, k_adj_m, 2.0)
            eval_rhs_adj_f(k_adj_f, scratch_v2, rk4_scratch.u, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           rk4_scratch.m, rk4_scratch.v, rk4_scratch.adj_f, cf)
            ca.MulC_I(k_adj_f, dt)  # K3_adj_f*dt computed
            ca.Add_MulC_I(rk4_adj_f, k_adj_f, 2.0)
            eval_rhs_adj_p(k_adj_p, scratch_v2, rk4_scratch.ginv, rk4_scratch.adj_u, rk4_scratch.adj_p, rk4_scratch.v)    
            ca.MulC_I(k_adj_p, dt)  # K3_adj_p*dt computed
            ca.Add_MulC_I(rk4_adj_p, k_adj_p, 2.0)

            # for K4
            # get adj_h at t+1#
            ca.Copy(rk4_scratch.adj_h, tdisc_spline[i+1].adj_h)
            ca.Add_I(rk4_scratch.adj_h, k_adj_h)
            # get adj_m and adj_v at t+1#
            ca.Copy(rk4_scratch.adj_m, tdisc_spline[i+1].adj_m)
            ca.Add_I(rk4_scratch.adj_m, k_adj_m)
            ca.Copy(rk4_scratch.adj_v, rk4_scratch.adj_m)
            diff_op.applyOperator(rk4_scratch.adj_v)
            # get adj_f and adj_u at t+1#
            ca.Copy(rk4_scratch.adj_f, tdisc_spline[i+1].adj_f)
            ca.Add_I(rk4_scratch.adj_f, k_adj_f)
            ca.Copy(rk4_scratch.adj_u, rk4_scratch.adj_f)
            diff_op.applyInverseOperator(rk4_scratch.adj_u)
            # get adj_p at t+1#
            ca.Copy(rk4_scratch.adj_p, tdisc_spline[i+1].adj_p)
            ca.Add_I(rk4_scratch.adj_p, k_adj_p)
            
            # now compute all K4's
            eval_rhs_adj_h(k_adj_h, scratch_v2, tdisc_spline[i].p, rk4_scratch.adj_u, rk4_scratch.adj_h, tdisc_spline[i].v)
            ca.MulC_I(k_adj_h, dt)  # K4_adj_h*dt computed
            ca.Add_I(rk4_adj_h, k_adj_h)
            eval_rhs_adj_m(k_adj_m, scratch_v2, rk4_scratch.adj_m,
                           tdisc_spline[i].v, tdisc_spline[i].m,
                           tdisc_spline[i].f, rk4_scratch.adj_f,
                           rk4_scratch.adj_u, rk4_scratch.adj_p,
                           tdisc_spline[i].p, rk4_scratch.adj_h,
                           tdisc_spline[i].ginv, diff_op)
            ca.MulC_I(k_adj_m, dt)  # k4_adj_m*dt computed
            ca.Add_I(rk4_adj_m, k_adj_m)
            eval_rhs_adj_f(k_adj_f, scratch_v2, tdisc_spline[i].u, rk4_scratch.adj_v, rk4_scratch.adj_u,
                           tdisc_spline[i].m, tdisc_spline[i].v, rk4_scratch.adj_f, cf)
            ca.MulC_I(k_adj_f, dt)  # K4_adj_f*dt computed
            ca.Add_I(rk4_adj_f, k_adj_f)
            eval_rhs_adj_p(k_adj_p, scratch_v2, tdisc_spline[i].ginv, rk4_scratch.adj_u, rk4_scratch.adj_p, tdisc_spline[i].v)    
            ca.MulC_I(k_adj_p, dt)  # K4_adj_p*dt computed
            ca.Add_I(rk4_adj_p, k_adj_p)

            # for final update
            ca.MulC_I(rk4_adj_h, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_m, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_f, 1.0/float(6.0))
            ca.MulC_I(rk4_adj_p, 1.0/float(6.0))

            # now take the final update step
            ca.Add(tdisc_spline[i].adj_h, tdisc_spline[i+1].adj_h, rk4_adj_h)  # updated adj_h
            ca.Add(tdisc_spline[i].adj_m, tdisc_spline[i+1].adj_m, rk4_adj_m)  # updated adj_m
            ca.Add(tdisc_spline[i].adj_f, tdisc_spline[i+1].adj_f, rk4_adj_f)  # updated adj_f
            ca.Add(tdisc_spline[i].adj_p, tdisc_spline[i+1].adj_p, rk4_adj_p)  # updated adj_p

            if tdisc_spline[i].has_data:
                '''
                # compute gradients at measurements
                # 1. \nabla I(0)
                ca.Gradient(scratch_v1, tdisc_spline[0].I)
                # 2. deform \nabla I(0)
                ca.ApplyH(scratch_v2, scratch_v1, tdisc_spline[i].ginv, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
                # 3. compute difference image
                ca.Sub(scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J)
                # 4. multiply difference image with deformed grad I(0)
                ca.MulMulC_I(scratch_v2, scratch_I1, -1.0)

                ca.Add_I(tdisc_spline[i].adj_h, scratch_v2) # negative gradient
                '''
                #compute_grad_measurements(scratch_v1, scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J, cf)
                compute_grad_measurements(scratch_v1, scratch_I1, tdisc_spline[i].I, tdisc_spline[i].J, cf, ginv=tdisc_spline[i].ginv, scratch_v=scratch_v2)
                ca.Add_MulC_I(tdisc_spline[i].adj_h, scratch_v1, -1.0)

            if tdisc_spline[i].is_control:
                if cf.optim.update_ps:
                    if cf.optim.regularize_ps:
                        ca.Copy(scratch_v1, tdisc_spline[i].p)
                        diff_op.applyInverseOperator(scratch_v1)
                        ca.MulC(out_gradient.grad_controls[controls_counter], scratch_v1, cf.optim.r_ps)  # copy p's
                        ca.Sub_I(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_p)  # copy p's
                    else:
                        #nikhil: debug
                        #ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_p, -6*1.0)  # copy -ve into out_gradient
                        ca.MulC(out_gradient.grad_controls[controls_counter], tdisc_spline[i].adj_p, -1.0)  # copy -ve into out_gradient

                ca.SetMem(tdisc_spline[i].adj_p, 0.0)  # reset adj_p
                controls_counter -= 1
        else:
            raise Exception('Unknown integration method: ' + cf.optim.integMethod)
        # now update other duals, adj_v and adj_u, required for the next integration step
        ca.Copy(tdisc_spline[i].adj_v, tdisc_spline[i].adj_m)
        diff_op.applyOperator(tdisc_spline[i].adj_v)
        ca.Copy(tdisc_spline[i].adj_u, tdisc_spline[i].adj_f)
        diff_op.applyInverseOperator(tdisc_spline[i].adj_u)

    # remember gradient w.r.t P's at control locations should already be updated in the above loop
    # update gradients w.r.t remaining initial conditions at t=0
    if cf.optim.update_m0:
        if cf.optim.regularize_m0:
            ca.MulC(out_gradient.grad_m, tdisc_spline[0].v, cf.optim.r_m0)  # copy m0
            ca.Sub_I(out_gradient.grad_m, tdisc_spline[0].adj_m)  # copy m0
        else:
            ca.MulC(out_gradient.grad_m, tdisc_spline[0].adj_m, -1.0)  # copy -ve into out_gradient


    # note that u is regularized along the full curve during integration
    if cf.optim.update_u0:
        #nikhil: debug
        #ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -2*1.0)  # copy -ve into out_gradient
        ca.MulC(out_gradient.grad_u, tdisc_spline[0].adj_u, -1.0)  # copy -ve into out_gradient

    if cf.optim.update_p0:
        if cf.optim.regularize_p0:
            ca.Copy(scratch_v1, tdisc_spline[0].p)
            diff_op.applyInverseOperator(scratch_v1)
            ca.MulC(out_gradient.grad_p, scratch_v1, cf.optim.r_p0)  # copy p's
            ca.Sub_I(out_gradient.grad_p, tdisc_spline[0].adj_p)  # copy p's
        else:
            #nikhil: debug
            #ca.MulC(out_gradient.grad_p, tdisc_spline[0].adj_p, -6*1.0)  # copy -ve into out_gradient
            ca.MulC(out_gradient.grad_p, tdisc_spline[0].adj_p, -1.0)  # copy -ve into out_gradient

    # nikhil: debug
    #diff_op.applyOperator(out_gradient.grad_m)
    #diff_op.applyOperator(out_gradient.grad_u)
    #diff_op.applyOperator(out_gradient.grad_p)
    return out_gradient


def shoot_forward_rk4_direct_w_scaled_ctrl(cf, tdisc_spline, diff_op, rk4_scratch=None, start_index=0, end_index=0):
    grid = tdisc_spline[0].I.grid()
    mem_type = tdisc_spline[0].m.memType()

    # scratch variables
    if rk4_scratch is None:
        rk4_scratch = SplineState(grid, mem_type, None)
    scratch_v1 = ca.ManagedField3D(grid, mem_type)
    rk4_g = ca.ManagedField3D(grid, mem_type)
    rk4_m = ca.ManagedField3D(grid, mem_type)
    rk4_f = ca.ManagedField3D(grid, mem_type)
    k_g = ca.ManagedField3D(grid, mem_type)
    k_m = ca.ManagedField3D(grid, mem_type)
    k_f = ca.ManagedField3D(grid, mem_type)

    # initial conditions
    if end_index == 0:
        end_index = len(tdisc_spline)-1

    if start_index == 0:
        # initializations
        ca.SetToIdentity(tdisc_spline[0].g)
        ca.SetToIdentity(tdisc_spline[0].ginv)
        ca.Copy(tdisc_spline[0].v, tdisc_spline[0].m)
        diff_op.applyInverseOperator(tdisc_spline[0].v)
        ca.Copy(tdisc_spline[0].f, tdisc_spline[0].u)
        diff_op.applyInverseOperator(tdisc_spline[0].f)
        index_last_control = 0

    # do integration
    for i in range(start_index+1, end_index+1, 1):
        dt = tdisc_spline[i].t-tdisc_spline[i-1].t  # time step

        eval_rhs_g(k_g, tdisc_spline[i-1].v, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, dt)  # K1_g*dt computed
        ca.Copy(rk4_g, k_g)
        eval_rhs_m(k_m, scratch_v1, tdisc_spline[i-1].v, tdisc_spline[i-1].m, tdisc_spline[i-1].u)
        ca.MulC_I(k_m, dt)  # K1_m*dt computed
        ca.Copy(rk4_m, k_m)

        eval_rhs_f(k_f, scratch_v1, tdisc_spline[i-1].p, tdisc_spline[i-1].ginv, tdisc_spline[i-1].f,
                   tdisc_spline[i-1].m, tdisc_spline[i-1].v, diff_op)
        ca.MulC_I(k_f, dt)  # K1_f*dt computed
        ca.Copy(rk4_f, k_f)

        # for K2
        # get g and ginv at t+1/2#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, 0.5)  # K1_g*dt/2
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1/2#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.MulC_I(k_m, 0.5)  # K1_m*dt/2
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1/2#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.MulC_I(k_f, 0.5)  # K1_f*dt/2
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1/2#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K2's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k2_g*dt computed
        ca.Add_MulC_I(rk4_g, k_g, 2.0)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        #eval_rhs_m_part(k_m_part, rk4_scratch.g, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K2_m_part*dt computed
        ca.Add_MulC_I(rk4_m, k_m, 2.0)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K2_f_part*dt computed
        ca.Add_MulC_I(rk4_f, k_f, 2.0)

        # for K3
        # get g and ginv at t+1/2#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.MulC_I(k_g, 0.5)  # K2_g*dt/2
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1/2#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.MulC_I(k_m, 0.5)  # K2_m_part*dt/2
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1/2#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.MulC_I(k_f, 0.5)  # K2_f_part*dt/2
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1/2#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K3's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k3_g*dt computed
        ca.Add_MulC_I(rk4_g, k_g, 2.0)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K3_m_part*dt computed
        ca.Add_MulC_I(rk4_m, k_m, 2.0)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K3_f_part*dt computed
        ca.Add_MulC_I(rk4_f, k_f, 2.0)

        # for K4
        # get g and ginv at t+1#
        ca.Copy(rk4_scratch.g, tdisc_spline[i-1].g)
        ca.Add_I(rk4_scratch.g, k_g)
        ca.UpdateInverse(rk4_scratch.ginv, scratch_v1, tdisc_spline[i-1].ginv, k_g, cf.optim.NIterForInverse)
        # get m and v at t+1#
        ca.Copy(rk4_scratch.m, tdisc_spline[i-1].m)
        ca.Add_I(rk4_scratch.m, k_m)
        ca.Copy(rk4_scratch.v, rk4_scratch.m)
        diff_op.applyInverseOperator(rk4_scratch.v)
        # get u and f at t+1#
        ca.Copy(rk4_scratch.f, tdisc_spline[i-1].f)
        ca.Add_I(rk4_scratch.f, k_f)
        ca.Copy(rk4_scratch.u, rk4_scratch.f)
        diff_op.applyOperator(rk4_scratch.u)
        # get p and I at t+1#
        ca.ApplyH(rk4_scratch.I, tdisc_spline[0].I, rk4_scratch.ginv)
        ca.ComposeHH(scratch_v1, rk4_scratch.g, tdisc_spline[index_last_control].ginv)
        cavmcommon.SplatVSafe(rk4_scratch.p, scratch_v1, tdisc_spline[index_last_control].p)

        # now compute all K4's
        eval_rhs_g(k_g, rk4_scratch.v, rk4_scratch.g)
        ca.MulC_I(k_g, dt)  # k4_g*dt computed
        ca.Add_I(rk4_g, k_g)
        eval_rhs_m(k_m, scratch_v1, rk4_scratch.v, rk4_scratch.m, rk4_scratch.u)
        ca.MulC_I(k_m, dt)  # K4_m_part*dt computed
        ca.Add_I(rk4_m, k_m)
        eval_rhs_f(k_f, scratch_v1, rk4_scratch.p, rk4_scratch.ginv, rk4_scratch.f,
                   rk4_scratch.m, rk4_scratch.v, diff_op)
        ca.MulC_I(k_f, dt)  # K4_f_part*dt computed
        ca.Add_I(rk4_f, k_f)

        # for final update
        ca.MulC_I(rk4_g, 1.0/float(6.0))
        ca.MulC_I(rk4_m, 1.0/float(6.0))
        ca.MulC_I(rk4_f, 1.0/float(6.0))

        # now take the final update step
        ca.Add(tdisc_spline[i].g, tdisc_spline[i-1].g, rk4_g)  # updated g
        ca.UpdateInverse(tdisc_spline[i].ginv, scratch_v1, tdisc_spline[i-1].ginv, rk4_g, cf.optim.NIterForInverse)  # updated ginv
        ca.Add(tdisc_spline[i].m, tdisc_spline[i-1].m, rk4_m)  # final updated m
        ca.Add(tdisc_spline[i].f, tdisc_spline[i-1].f, rk4_f)  # final updated f
        ca.ApplyH(tdisc_spline[i].I, tdisc_spline[0].I, tdisc_spline[i].ginv)  # final updated I

        if not tdisc_spline[i].is_control:
            # input previous P[0], this g
            # get g_{last_control, this}
            ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatVSafe(tdisc_spline[i].p, scratch_v1, tdisc_spline[index_last_control].p)  # final update P
        else:
            ca.ComposeHH(scratch_v1, tdisc_spline[i].g, tdisc_spline[index_last_control].ginv)
            cavmcommon.SplatVSafe(tdisc_spline[i].p, scratch_v1, tdisc_spline[index_last_control].p)  # final update P
            ca.MulC_I(tdisc_spline[i].p,-1.3)
            index_last_control = i  # for subsequent iterations
            
        # now update other duals, v and u, required for the next integration step
        ca.Copy(tdisc_spline[i].v, tdisc_spline[i].m)
        diff_op.applyInverseOperator(tdisc_spline[i].v)
        ca.Copy(tdisc_spline[i].u, tdisc_spline[i].f)
        diff_op.applyOperator(tdisc_spline[i].u)
