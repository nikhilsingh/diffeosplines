This is a python based CPU/GPU implementation of regression with
shooting splines for diffeomorphisms. It is implemented for splines
regression with both 2D and 3D image sequences.

This repository also contains the supplementary material that include
the derivations of variations of the energy functionals. It also
provides additional results such as the visualizations for the
estimated initial conditions that could not be included in the main
article due to space constraints.

Please cite one of the following papers if you use it for your research:

[1] Nikhil Singh, François-Xavier Vialard, and Marc Niethammer,
Splines for Diffeomorphisms. (Among top 3 finalists for journal best paper award) 
in J. Medical Image Analysis (MedIA), 2015. 

[2] Nikhil Singh and Marc Niethammer, "Splines for Diffeomorphic Image
Regression", MICCAI 2014, Boston
