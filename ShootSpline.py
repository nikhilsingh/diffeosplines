#!/usr/bin/python2

# from vectormomenta config module. must be made general package by itself
from Configs import Config, Compute

# pyca modules
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
import Libraries.CAvmCommon as cavmcommon

# diffeo_splines modules
import Core
import Optim
import SplinesConfig

# others
import numpy as np
import matplotlib
#matplotlib.use('GTKAgg')
import matplotlib.pyplot as plt
import os
import errno

import logging
import sys
import copy
import math
import time

# optimization
from scipy.optimize import minimize
from scipy.optimize import fmin_l_bfgs_b

StudySpec = {
    'I0':
    Config.Param(default='Path/to/I0', required=True,
    comment="Path to initial image."),
    'm0':
    Config.Param(default='Path/to/m0', required=True,
    comment="Path to initial momenta."),
    'u0':
    Config.Param(default='Path/to/u0', required=True,
    comment="Path to initial control."),
    'P0':
    Config.Param(default='Path/to/P0', required=True,
    comment="Path to initial jerk."),
    'Pc':
    Config.Param(default=['Path/to/P0'], required=False,
    comment="List of paths to jerk at control locations in the same order as controlLocations."),
    'scale':
    Config.Param(default=1.0, required=False,
    comment="Scale all the initial conditions by this amount before shooting.")
    }

SplinesConfigSpec = {
    'compute': Compute.ComputeConfigSpec,
    'study': StudySpec,
    'splinesconfig': SplinesConfig.ConfigSpec,
    'optim': Optim.OptimConfigSpec,
    'io': {
        'plotEvery':
        Config.Param(default=10,
                     comment="Update plots every N iterations"),
        'plotSlice':
        Config.Param(default=None,
                     comment="Slice to plot.  Defaults to mid axial"),
        'quiverEvery':
        Config.Param(default=1,
                     comment="How much to downsample for quiver plots"),
        'gridEvery':
        Config.Param(default=1,
                     comment="How much to downsample for deformation grid plots"),
        'outputPrefix':
        Config.Param(default="./",
                     comment="Where to put output.  Don't forget trailing "
                     + "slash"),
        'writeAll':
        Config.Param(default=False,
                     comment="Write out all states at all time descretizations?"),
        'saveFrames':
        Config.Param(default=True,
                     comment="Save frames for every timestep for creating video later?")},
    '_resource': 'SplinesConfigSpec'}

def setup_timearray(num_timesteps, control_times_array, epsilon):
    num_controls = len(control_times_array)
    temp_t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    t = [x*1./num_timesteps for x in range(num_timesteps+1)]
    control_indices = [0]*num_controls

    temp_t = t
    # add timepoints for controls
    for i in range(num_controls):
        found_control = False
        for j in range(len(temp_t)):
            if (control_times_array[i] < (temp_t[j]+epsilon)) and (control_times_array[i] > (temp_t[j]-epsilon)):
                found_control = True
                break

        if not found_control:  # need to insert a timepoint for this control
            t.append(control_times_array[i])
            t.sort()

    # now create the control index array
    for i in range(num_controls):
        for j in range(len(t)):
            if (control_times_array[i] < (t[j]+epsilon)) and (control_times_array[i] > (t[j]-epsilon)):
                control_indices[i] = j
                break

    return t, control_indices


def setup_time_discretization(time_descretized_array, control_indices, grid, mem_type):
    """
    Set up an list of states at time descritizations for the spline curve (list of SplineStates)
    """
    td = []
    for i in xrange(len(time_descretized_array)):
        if i in control_indices:
            td.append(Core.SplineState(grid, mem_type, time_descretized_array[i], True, False, None, None))
        else:
            td.append(Core.SplineState(grid, mem_type, time_descretized_array[i], False, False, None, None))
    return td


def shoot_spline(cf):
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    if cf.compute.useCUDA and cf.compute.gpuID is not None:
        ca.SetCUDADevice(cf.compute.gpuID)

    # prepare output directory
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))

    # Output loaded config
    if cf.io.outputPrefix is not None:
        cfstr = Config.ConfigToYAML(SplinesConfigSpec, cf)
        with open(cf.io.outputPrefix + "parsedconfig.yaml", "w") as f:
            f.write(cfstr)

    mem_type = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST

    # Read input
    logging.info('Reading program input...')
    I0 = common.LoadITKImage(cf.study.I0, mem_type)
    m0 = common.LoadITKField(cf.study.m0, mem_type)
    u0 = common.LoadITKField(cf.study.u0, mem_type)
    P0 = common.LoadITKImage(cf.study.P0, mem_type)

    Pc = [common.LoadITKImage(f, mem_type) if isinstance(f, str) else f for f in cf.study.Pc]
    control_locations = cf.splinesconfig.controlLocations
    logging.info('...done.')

    # start up the memory manager for scratch variables
    logging.info('Initializing memory manager...')
    grid = I0.grid()
    ca.ThreadMemoryManager.init(grid, mem_type, 0)
    logging.info('...done.')

    logging.info('Setting up time descretization and allocating memory...')
    (time_descretized_array, control_indices) = setup_timearray(cf.optim.nTimeSteps, control_locations, 0.001)
    tdisc_spline = setup_time_discretization(time_descretized_array, control_indices, grid, mem_type)
    ca.Copy(tdisc_spline[0].I, I0)
    ca.MulC(tdisc_spline[0].m, m0, float(cf.study.scale))
    ca.MulC(tdisc_spline[0].u, u0, float(cf.study.scale)**2.0)
    ca.MulC(tdisc_spline[0].P, P0, float(cf.study.scale)**3.0)

    for i in range(len(control_indices)):
        ca.Copy(tdisc_spline[control_indices[i]].P, Pc[i])
        #nikhil: debug
        #ca.MulC_I(tdisc_spline[control_indices[i]].P,-1.0)
    logging.info('...done.')

    # create differential operator
    logging.info('Creating differential operator...')
    diff_oper = Core.setup_diff_oper(cf.splinesconfig.diffOpParams[0], cf.splinesconfig.diffOpParams[1],
                                     cf.splinesconfig.diffOpParams[2], grid, mem_type)
    logging.info('...done.')

    # Run spline shooting
    logging.info('Running spline shooting...')
    run_shoot_spline(cf, tdisc_spline, diff_oper)
    logging.info('...done.')

    # Write output
    logging.info('Saving results...')
    write_output(cf, tdisc_spline, diff_oper)
    logging.info('...done.')


def run_shoot_spline(cf, tdisc_spline, diff_oper):
    if cf.optim.integMethod == "EULER":
        Core.shoot_forward(cf, tdisc_spline, diff_oper)
    elif cf.optim.integMethod == "RK4":
        Core.shoot_forward_rk4_direct(cf, tdisc_spline, diff_oper)
    else:
        raise Exception('Unknown integration method: ' + cf.optim.integMethod)


def write_output(cf, tdisc_spline, diff_oper):

    common.SaveITKField(tdisc_spline[0].m, cf.io.outputPrefix + "m00.mhd")
    common.SaveITKField(tdisc_spline[0].u, cf.io.outputPrefix + "u00.mhd")
    common.SaveITKImage(tdisc_spline[0].P, cf.io.outputPrefix + "P00.mhd")
    common.SaveITKImage(tdisc_spline[0].I, cf.io.outputPrefix + "I00.mhd")

    if cf.io.writeAll:
        for i in range(1, len(tdisc_spline)):
            common.SaveITKField(tdisc_spline[i].m, cf.io.outputPrefix + "m"+str(i).zfill(2)+".mhd")
            common.SaveITKField(tdisc_spline[i].u, cf.io.outputPrefix + "u"+str(i).zfill(2)+".mhd")
            common.SaveITKImage(tdisc_spline[i].P, cf.io.outputPrefix + "P"+str(i).zfill(2)+".mhd")
            common.SaveITKImage(tdisc_spline[i].I, cf.io.outputPrefix + "I"+str(i).zfill(2)+".mhd")

    # write out frames
    if cf.io.saveFrames:
        save_frames(cf, tdisc_spline)


def save_frames(cf, tdisc_spline):
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/')
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix)+'/frames/extras/')

    image_idx = 0

    for i in range(len(tdisc_spline)):
        fig = plt.figure('images', frameon=False)
        plt.clf()
        display.DispImage(tdisc_spline[i].I, '', newFig=False, cmap='gray', sliceIdx=cf.io.plotSlice)
        plt.draw()
        outfilename = cf.io.outputPrefix+'/frames/I'+str(image_idx).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

        fig = plt.figure('invdef',frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].ginv, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        outfilename = cf.io.outputPrefix+'/frames/invdef'+str(image_idx).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)

        fig = plt.figure('def', frameon=False)
        plt.clf()
        display.GridPlot(tdisc_spline[i].g, every=cf.io.gridEvery, color='k', sliceIdx=cf.io.plotSlice, isVF=False, plotBase=False)
        #fig.patch.set_alpha(0)
        #fig.patch.set_visible(False)
        a = fig.gca()
        #a.set_frame_on(False)
        a.set_xticks([]); a.set_yticks([])
        plt.axis('tight')
        plt.axis('image')
        plt.axis('off')
        plt.draw()
        outfilename = cf.io.outputPrefix+'/frames/def'+str(image_idx).zfill(5)+'.png'
        fig.set_size_inches(4, 4)
        plt.savefig(outfilename, bbox_inches='tight', pad_inches=0,dpi=100)

        # plot extras
        save_extra_frames(cf, tdisc_spline, i, image_idx)

        image_idx += 1


def save_extra_frames(cf, tdisc_spline, i, image_idx):
    fig = plt.figure('momenta', frameon=False)
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].m, '')
    outfilename = cf.io.outputPrefix+'/frames/extras/m'+str(image_idx).zfill(5)+'.png'
    fig.set_size_inches(4, 4)
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('control', frameon=False)
    plt.clf()
    Core.plot_vf_2d(tdisc_spline[i].u, '')
    outfilename = cf.io.outputPrefix+'/frames/extras/u'+str(image_idx).zfill(5)+'.png'
    fig.set_size_inches(4, 4)
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)

    fig = plt.figure('jerk', frameon=False)
    plt.clf()
    Core.plot_im_2d(tdisc_spline[i].P, '')
    outfilename = cf.io.outputPrefix+'/frames/extras/P'+str(image_idx).zfill(5)+'.png'
    fig.set_size_inches(4, 4)
    plt.savefig(outfilename, bbox_inches='tight', pad_inches=0, dpi=100)


if __name__ == '__main__':
    #common.DebugHere()
    try:
        usercfg = Config.Load(spec=SplinesConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        # Don't freak out, this should have printed a sample config to STDOUT
        sys.exit(1)

    shoot_spline(usercfg)
